﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using ClientDataBase;
using DataTransferClientLibrary;
using DataTransferModel.DataTransfer;
using ModelsTablesDBLib;

namespace ClientTestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var config = ReadConfig();
            var client = new DataTransferClient(config.DataTransferHost, config.DataTransferPort);
            Console.WriteLine($"Client started\r\n" +
                              $"Current data transfer ip {config.DataTransferHost}:{config.DataTransferPort}\r\n" +
                              $"Current database ip {config.DatabaseHost}:{config.DatabasePort}");
            var db = new ClientDB("test data transfer client", $"{config.DatabaseHost}:{config.DatabasePort}");
            db.OnConnect += (sender, eventArgs) => Console.WriteLine("db connected");
            db.OnDisconnect += (sender, eventArgs) => Console.WriteLine("db disconnected");
            client.OnMessageReceived += (sender, s) => Console.WriteLine($"sender : {s.SenderId}, receiver : {s.ReceiverId}, message : {s.Text}");
            client.OnModeReceived += (sender, mode) => Console.WriteLine($"Mode received {mode}");
            client.OnSlaveRdfResultReceived += (sender, signalz) => { Console.WriteLine($"signals to : {signalz.StationId}, band : {signalz.BandNumber}, n : {signalz.Signals.Length}");};
            client.OnMasterRdfResultReceived += (sender, signalz) => { Console.WriteLine($"results to : {signalz.StationId}, band : {signalz.BandNumber}, n : {signalz.Signals.Length}"); }; 
            byte modeToBeSet = 0;
            while (true)
            {
                try
                {
                    var line = Console.ReadLine();
                    if (line.Equals("connect") || line.Equals("c"))
                    {
                        client.Connect("TestApp");
                        Console.WriteLine($"Connected, our id is :{client.Id}");
                    }

                    if (line.Equals("disconnect") || line.Equals("d"))
                    {
                        client.Disconnect();
                        Console.WriteLine($"Disconnected, our id was :{client.Id}");
                    }

                    if (line.Equals("test3"))
                    {
                        var jam = db.Tables[NameTable.TableSuppressFWS];
                        var frsjam = new TableSuppressFWS()
                        {
                            Id = 1,
                            IdMission = 1,
                            FreqKHz = 35_000
                        };
                        jam.Clear();
                        jam.AddRange(new System.Collections.Generic.List<TableSuppressFWS>() { frsjam});
                    }
                    if (line.Equals("test4"))
                    {
                        var jam = db.Tables[NameTable.TableSuppressFWS];
                        var frsjam = new TableSuppressFWS()
                        {
                            Id = 1,
                            IdMission = 1,
                            FreqKHz = 35_000,
                            NumberASP = 1
                        };
                        jam.Clear();
                        jam.Add(frsjam);
                    }
                    if (line.Equals("clear"))
                    {
                        var jam = db.Tables[NameTable.TableSuppressFWS];
                        jam.Clear();
                    }

                    if (line.Equals("test"))
                    {
                        var fhss = db.Tables[NameTable.TableReconFHSS];
                        var source = db.Tables[NameTable.TableSourceFHSS];
                        var ex = db.Tables[NameTable.TableFHSSReconExcluded];
                        fhss.Clear();
                        source.Clear();
                        fhss.Add(new TableReconFHSS()
                        {
                            Id = 1,
                            IdMission = 1,
                            FreqMinKHz = 35_000,
                            FreqMaxKHz = 40_000
                        });
                        source.Add(new TableSourceFHSS()
                        {
                            Coordinates = new Coord(),
                            Id = 1,
                            IdFHSS = 1,
                            ListJamDirect = new ObservableCollection<TableJamDirect>()
                            {
                                new TableJamDirect()
                                {
                                    ID = 1,
                                    JamDirect = new JamDirect()
                                    {
                                        Bearing = 15,
                                        DistanceKM = 15,
                                        IsOwn = true,
                                        Level = -50,
                                        NumberASP = 1,
                                        Std = 0.5f
                                    }
                                },
                                new TableJamDirect()
                                {
                                    ID = 2,
                                    JamDirect = new JamDirect()
                                    {
                                        Bearing = 225,
                                        DistanceKM = 523532,
                                        IsOwn = true,
                                        Level = -50,
                                        NumberASP = 1,
                                        Std = 0.5f
                                    }
                                }
                            }
                        });
                        //ex.Add(new TableFHSSExcludedFreq()
                        //{
                        //    Deviation = 15,
                        //    FreqKHz = 36_000,
                        //    Id = 1,
                        //    IdFHSS = 1
                        //});
                    }
                    if (line.Equals("test2"))
                    {
                        var fhss = db.Tables[NameTable.TableReconFHSS];
                        var source = db.Tables[NameTable.TableSourceFHSS];
                        var ex = db.Tables[NameTable.TableFHSSReconExcluded];
                        fhss.Clear();
                        source.Clear();
                        fhss.Add(new TableReconFHSS()
                        {
                            Id = 1,
                            IdMission = 1,
                            FreqMinKHz = 35_000,
                            FreqMaxKHz = 40_000
                        });
                        source.Add(new TableSourceFHSS()
                        {
                            Coordinates = new Coord(),
                            Id = 1,
                            IdFHSS = 1,
                            ListJamDirect = new ObservableCollection<TableJamDirect>()
                            {
                                new TableJamDirect()
                                {
                                    ID = 1,
                                    JamDirect = new JamDirect()
                                    {
                                        Bearing = 15,
                                        DistanceKM = 15,
                                        IsOwn = true,
                                        Level = -50,
                                        NumberASP = 1,
                                        Std = 0.5f
                                    }
                                }
                            }
                                
                        });
                        source.Add(new TableSourceFHSS()
                        {
                            Coordinates = new Coord(),
                            Id = 2,
                            IdFHSS = 1,
                            ListJamDirect = new ObservableCollection<TableJamDirect>()
                            {
                                new TableJamDirect()
                                {
                                    ID = 2,
                                    JamDirect = new JamDirect()
                                    {
                                        Bearing = 225,
                                        DistanceKM = 523532,
                                        IsOwn = true,
                                        Level = -50,
                                        NumberASP = 1,
                                        Std = 0.5f
                                    }
                                }
                            }
                        });
                        source.Add(new TableSourceFHSS()
                        {
                            Coordinates = new Coord(),
                            Id = 3,
                            IdFHSS = 1,
                            ListJamDirect = new ObservableCollection<TableJamDirect>()
                            {
                                new TableJamDirect()
                                {
                                    ID = 3,
                                    JamDirect = new JamDirect()
                                    {
                                        Bearing = 22222,
                                        DistanceKM = 111111,
                                        IsOwn = false,
                                        Level = -30,
                                        NumberASP = 2,
                                        Std = 0.5f
                                    }
                                }
                            }
                        });
                        source.Add(new TableSourceFHSS()
                        {
                            Coordinates = new Coord(),
                            Id = 4,
                            IdFHSS = 1,
                            ListJamDirect = new ObservableCollection<TableJamDirect>()
                            {
                                new TableJamDirect()
                                {
                                    ID = 4,
                                    JamDirect = new JamDirect()
                                    {
                                        Bearing = 1515,
                                        DistanceKM = 545245235,
                                        IsOwn = false,
                                        Level = -30,
                                        NumberASP = 2,
                                        Std = 0.5f
                                    }
                                }
                            }
                        });
                    }

                    if (line.Equals("send"))
                    {
                        Console.WriteLine("Input sender id, receiver id and message : ");
                        var message = Console.ReadLine().Split(' ');
                        client.SendMessage(Int32.Parse(message[0]), Int32.Parse(message[1]), message[2]);
                    }

                    if (line.Equals("db"))
                    {
                        db.Connect();
                    }

                    if (line.Equals("db d"))
                    {
                        db.Disconnect();
                    }

                    if (line.Equals("ip"))
                    {
                        Console.WriteLine("Input new data transfer ip and port : ");
                        var newLine = Console.ReadLine();
                        var ipAndPort = newLine.Split(' ');
                        client = new DataTransferClient(ipAndPort[0], Int32.Parse(ipAndPort[1]));
                    }

                    if (line.Equals("mode") || line.Equals("m"))
                    {
                        client.SendMode(modeToBeSet);
                        modeToBeSet++;
                        if (modeToBeSet == 5)
                            modeToBeSet = 0;
                    }

                    if (line.Equals("t"))
                    {
                        Console.WriteLine("input station id");
                        var station = Int32.Parse(Console.ReadLine());
                        client.SendSlaveRdfResult(new RdfCycleResult()
                        {
                            StationId = station,
                            BandNumber = modeToBeSet,
                            Signals = new Signal[]
                            {
                                new Signal(), new Signal() 
                            }
                        });

                        modeToBeSet++;
                        if (modeToBeSet == 99)
                            modeToBeSet = 0;
                    }

                    if (line.Equals("t2"))
                    {
                        Console.WriteLine("input station id");
                        var station = Int32.Parse(Console.ReadLine());
                        client.SendMasterRdfResult(new RdfCycleResult()
                        {
                            StationId = station,
                            BandNumber = modeToBeSet,
                            Signals = new Signal[]
                            {
                                new Signal(), new Signal(), new Signal()
                            }
                        });

                        modeToBeSet++;
                        if (modeToBeSet == 99)
                            modeToBeSet = 0;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error occured : " + e.ToString());
                }
            }
        }

        private static ClientConfig ReadConfig()
        {
            if (!File.Exists("config.txt"))
            {
                return new ClientConfig();
            }

            var config = File.ReadAllText("config.txt").Split(',');

            return new ClientConfig(config);
        }
    }

    class ClientConfig  
    {
        public string DataTransferHost { get; set; }
        public int DataTransferPort { get; set; }
        public string DatabaseHost { get; set; }
        public int DatabasePort { get; set; }

        public ClientConfig()
        {
            DataTransferHost = "127.0.0.1";
            DataTransferPort = 10009;
            DatabaseHost = "127.0.0.1";
            DatabasePort = 8302;
        }

        public ClientConfig(string[] file)
        {
            DataTransferHost = file[0];
            DataTransferPort = Int32.Parse(file[1]);
            DatabaseHost = file[2];
            DatabasePort = Int32.Parse(file[3]);
        }
    }
}
