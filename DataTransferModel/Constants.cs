﻿namespace DataTransferModel
{
    public static class Constants
    {
        public static int DeadlineSec = 1;
        public static int DeadlineMs = DeadlineSec * 1000;

        public const int FailedPingDelayMs = 2000;
        public const int SucceededPingDelayMs = 30_000;

        //Com port executive df settings
        public const int RequestGapKhz = 500;
        public const int PhaseAveragingCount = 3;
        public const int DirectionAveragingCount = 3;
    }
}
