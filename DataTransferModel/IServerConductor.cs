﻿using System;

namespace DataTransferModel
{
    public interface IServerConductor
    {
        event EventHandler<string> OnTransmissionError;

        /// <summary>
        /// Initializes connections between conducted parts
        /// </summary>
        void Initialize();

        /// <summary>
        /// Removes connections between conducted parts
        /// </summary>
        void ShutDown();
    }
}
