﻿using System;

namespace DataTransferModel.Trasmission
{
    public interface ITransmitter : ITransmissionClient, ITransmissionServer
    {
        TransmissionType Type { get; }

        bool IsMaster { get; }

        int OwnStationId { get; }
        int ConnectedStationId { get; }

        bool ShowRdfLog { get; set; }

        event EventHandler<string> OnSend;
        event EventHandler<string> OnReceive;
    }
}
