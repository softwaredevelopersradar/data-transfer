﻿using DataTransferModel.DataTransfer;
using ModelsTablesDBLib;
using System;

namespace DataTransferModel.Trasmission
{
    public interface IReceiver : IRequestedForceConnection
    {
        event EventHandler<Message> OnSendMessageRequest;
        event EventHandler<ExecutiveDfRequestArgs> OnSendExecutiveDfRequest;
        event EventHandler<ExecutiveDfResponseArgs> OnSendExecutiveDfResponse;
        event EventHandler<byte> OnSendModeRequest;
        event EventHandler<int> OnSetBandReceived;
        event EventHandler<DateTime> OnLocalTimeReceived;

        event EventHandler<RdfCycleResult> OnMasterRdfResultReceived;
        event EventHandler<RdfCycleResult> OnSlaveRdfResultReceived;
        event EventHandler<FhssCycleResult> OnFhssCycleResultReceived;

        event EventHandler<AspTable> AspTableReceived;
        event EventHandler<TableEventArgs<TableSectorsRangesRecon>> TableSectorsRangesReconReceived;
        event EventHandler<TableEventArgs<TableSectorsRangesSuppr>> TableSectorsRangesSupprReceived;
        event EventHandler<TableEventArgs<TableReconFWS>> TableFrsReconReceived;
        event EventHandler<TableEventArgs<TableReconFHSS>> TableFhssReconReceived;
        event EventHandler<TableEventArgs<TableSuppressFWS>> TableFrsJamReceived;
        event EventHandler<TableEventArgs<TableSuppressFHSS>> TableFhssJamReceived;
        event EventHandler<TableEventArgs<TableFHSSExcludedFreq>> TableFhssExcludedFreqReceived;
        event EventHandler<TableEventArgs<TableFreqImportant>> TableFreqImportantReceived;
        event EventHandler<TableEventArgs<TableFreqForbidden>> TableFreqForbiddenReceived;
        event EventHandler<TableEventArgs<TableFreqKnown>> TableFreqKnownReceived;
    }
}
