﻿namespace DataTransferModel.Trasmission
{
    public interface IConnectable
    {
        /// <summary>
        /// Tries to connect to the server, or to host the server
        /// </summary>
        void Initialize();
        void ShutDown();
    }
}
