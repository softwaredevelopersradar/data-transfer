﻿using System;

namespace DataTransferModel.Trasmission
{
    public interface IRequestedForceConnection
    {
        event EventHandler<PeerInfo> OnForceConnectionRequest;//todo : rename to OnConnect :?
    }
}
