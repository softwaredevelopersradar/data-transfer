﻿namespace DataTransferModel.Trasmission
{
    public interface ITransmitterFactory
    {
        /// <summary>
        /// Create com transmitter or grpc transmitter with client not connected to anything
        /// </summary>
        ITransmitter CreateTransmitter(int ownStationId, int connectedStationId, bool isMaster);
        /// <summary>
        /// Create grpc transmitter
        /// </summary>
        ITransmitter CreateTransmitter(int ownStationId, int connectedStationId, string linkedStationHost, int linkedStationPort, bool isMaster);
    }
}
