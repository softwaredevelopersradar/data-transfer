﻿namespace DataTransferModel.Trasmission
{
    public interface ITransmissionServer : IReceiver, IConnectable
    {
        bool IsWorking { get; }

        string ServerAddress { get; }
        int ServerPort { get; }
    }
}
