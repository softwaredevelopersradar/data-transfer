﻿namespace DataTransferModel.Trasmission
{
    public struct TableEventArgs<T>
    {
        public int SenderStationId { get; private set; }
        public T[] Table { get; private set; }
        public TableEventArgs(int senderStationId, T[] table)
        {
            SenderStationId = senderStationId;
            Table = table;
        }
    }
}
