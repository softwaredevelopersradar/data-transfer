﻿using System;

namespace DataTransferModel.Trasmission
{
    public interface ITransmissionClient : ISender, IConnectable
    {
        bool IsConnected { get; }
        string ClientAddress { get; }
        int ClientPort { get; }

        /// <summary>
        /// When we do not know where to connect and got the ping from the server, so we can connect to it
        /// </summary>
        void ForceConnection(string host, int port);

        /// <summary>
        /// Clears current connection (needs to be done, before establishing a new one)
        /// </summary>
        void AbortConnection(bool isMaster);

        event EventHandler<bool> ConnectionStateChanged;
    }
}
