﻿using DataTransferModel.DataTransfer;
using ModelsTablesDBLib;
using System;

namespace DataTransferModel.Trasmission
{
    public interface ISender
    {
        /// <summary>
        /// Args is just an argument to pass, used here to translate server port to ensure backward connection via grpc
        /// </summary>
        bool Ping(int ownStationId, int ownPort);

        bool SendMessage(Message message);
        bool SendMode(byte mode);
        bool SendLocalTime(DateTime localTime);

        bool SendExecutiveDfRequest(float startFrequencyKhz, float endFrequencyKhz, int phaseAveragingCount, int directionAveragingCount);
        bool SendExecutiveDfResponse(float result, int stationId);

        bool SendSlaveRdfResult(RdfCycleResult result);
        bool SendMasterRdfResult(RdfCycleResult result);

        bool SendFhssCycleResult(FhssCycleResult cycleResult);
        bool SetBandNumber(int bandNumber);

        //database updates

        bool SendAspTable(AspTable table);
        bool SendTableSectorsRangesRecon(TableEventArgs<TableSectorsRangesRecon> table);
        bool SendTableSectorsRangesSuppr(TableEventArgs<TableSectorsRangesSuppr> table);
        bool SendTableFrsRecon(TableEventArgs<TableReconFWS> table);
        bool SendTableFhssRecon(TableEventArgs<TableReconFHSS> table);
        bool SendTableFrsJam(TableEventArgs<TableSuppressFWS> table);
        bool SendTableFhssJam(TableEventArgs<TableSuppressFHSS> table);
        bool SendTableFhssExcludedFreq(TableEventArgs<TableFHSSExcludedFreq> table);
        bool SendTableFreqImportant(TableEventArgs<TableFreqImportant> table);
        bool SendTableFreqKnown(TableEventArgs<TableFreqKnown> table);
        bool SendTableFreqForbidden(TableEventArgs<TableFreqForbidden> table);
    }
}
