﻿namespace DataTransferModel.Trasmission
{
    public enum TransmissionType : byte
    {
        GrpcTransmitter,
        ComTransmitter,
        None = 255
    }
}
