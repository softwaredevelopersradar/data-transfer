﻿namespace DataTransferModel.Trasmission
{
    public struct PeerInfo
    {
        public int StationId { get; private set; }
        public string IpAddress { get; private set; }
        public int Port { get; private set; }

        public PeerInfo(int stationId, string ipAddress, int port) 
        {
            StationId = stationId;
            IpAddress = ipAddress;
            Port = port;
        }
    }
}
