﻿namespace DataTransferModel.Database
{
    public enum ConnectionType : byte
    {
        Ethernet = 0,
        Mobile3G = 1,
        ComPort = 2,
        None = 255
    }
}
