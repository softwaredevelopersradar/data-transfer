﻿using ModelsTablesDBLib;

namespace DataTransferModel.Database
{
    public static class Utilities
    {
        public static ConnectionType ParseToDataTransferType(this TypeConnection typeConnection) 
        {
            switch (typeConnection) 
            {
                case TypeConnection.Tainet: return ConnectionType.ComPort;
                case TypeConnection.RRS: return ConnectionType.Ethernet;
                case TypeConnection.Type3G: return ConnectionType.Mobile3G;
                default: return ConnectionType.None;
            }
        }
    }
}
