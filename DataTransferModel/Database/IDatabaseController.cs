﻿using DataTransferModel.DataTransfer;
using ModelsTablesDBLib;
using System;
using System.Collections.Generic;

namespace DataTransferModel.Database
{
    public interface IDatabaseController
    {
        bool IsConnected { get; }
        IReadOnlyList<Station> LinkedStations { get; }
        Station OwnStation { get; }
        StationRole Role { get; }

        event EventHandler<string> OnDatabaseLog;
        event EventHandler<string> OnDatabaseError;
        event EventHandler<bool> OnDatabaseConnectionStateChanged;
        event EventHandler<IReadOnlyList<int>> OnLinkedStationsConnectionSettingsChanged;
        event EventHandler<bool> OnOwnStationValidData;
        
        event EventHandler<AspTable> AspTableReceived;
        event EventHandler<TableSectorsRangesRecon[]> TableSectorsRangesReconReceived;
        event EventHandler<TableSectorsRangesSuppr[]> TableSectorsRangesSupprReceived;
        event EventHandler<TableReconFWS[]> TableFrsReconReceived;
        event EventHandler<TableReconFHSS[]> TableFhssReconReceived;
        event EventHandler<TableSuppressFWS[]> TableFrsJamReceived;
        event EventHandler<TableSuppressFHSS[]> TableFhssJamReceived;
        event EventHandler<TableFHSSExcludedFreq[]> TableFhssExcludedFreqReceived;
        event EventHandler<TableFreqImportant[]> TableFreqImportantReceived;
        event EventHandler<TableFreqForbidden[]> TableFreqForbiddenReceived;
        event EventHandler<TableFreqKnown[]> TableFreqKnownReceived;

        void Connect();
        void Disconnect();
        void LoadTablesForClient();

        void UpdateFrsJamTable(TableSuppressFWS[] table);
        void ActuallyUpdateFrsJamTable(int stationNumber, TableSuppressFWS[] table);
        void UpdateFhssJamTable(TableSuppressFHSS[] table);
        void ActuallyUpdateFhssJamTable(int stationNumber, TableSuppressFHSS[] table);
        void UpdateSectorsRangesRecon(TableSectorsRangesRecon[] table);
        void UpdateSectorsRangesSuppr(TableSectorsRangesSuppr[] table);
        void UpdateFrsReconTable(TableReconFWS[] table);
        void ActuallyUpdateFrsReconTable(int stationNumber, TableReconFWS[] table);
        void UpdateFhssReconTable(TableReconFHSS[] table);
        void ActuallyUpdateFhssReconTable(int stationNumber, TableReconFHSS[] table);
        void UpdateForbiddenFrequenciesTable(TableFreqForbidden[] table);
        void UpdateImportantFrequenciesTable(TableFreqImportant[] table);
        void UpdateKnownFrequenciesTable(TableFreqKnown[] table);
        void UpdateAspTable(AspTable table);
        void UpdateAspConnectionState(int stationId, bool isConnected);
    }
}
