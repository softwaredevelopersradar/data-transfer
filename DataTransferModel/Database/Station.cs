﻿namespace DataTransferModel.Database
{
    public class Station
    {
        public int Id { get; private set; }
        public bool IsOwn { get; private set; }
        public string IpAddress { get; private set; }
        public int Port { get; private set; }

        public ConnectionType ConnectionType { get; private set; }

        public Station(int id, bool isOwn, string ipAddress, int port, ConnectionType connectionType = ConnectionType.None)
        {
            Id = id;
            IsOwn = isOwn;
            IpAddress = ipAddress;
            Port = port;
            ConnectionType = connectionType;
        }
    }
}
