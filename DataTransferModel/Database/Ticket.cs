﻿using System;
using ModelsTablesDBLib;

namespace DataTransferModel.Database
{
    public struct Ticket
    {
        public NameTable Table { get; }
        public DateTime CreationTime { get; }

        public Ticket(NameTable table)
        {
            Table = table;
            CreationTime = DateTime.Now;
        }

        public bool IsExpired() => DateTime.Now.Subtract(CreationTime).TotalSeconds > 5; //todo : to constants

        public bool IsDefault() => Table == NameTable.TableMission &&
                                   CreationTime == new DateTime();
    }
}
