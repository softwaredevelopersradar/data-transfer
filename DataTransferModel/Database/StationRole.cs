﻿namespace DataTransferModel.Database
{
    public enum StationRole : byte
    {
        Autonomic = 0,
        Master = 1,
        Slave = 2
    }
}
