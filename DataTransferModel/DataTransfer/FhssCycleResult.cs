﻿using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace DataTransferModel.DataTransfer
{
    /// <summary>
    /// To transmit fhss intelligence results
    /// </summary>
    [DataContract]
    public struct FhssCycleResult
    {
        [DataMember]
        public int StationId { get; }

        [DataMember]
        public FhssNetwork[] Networks { get; }

        public FhssCycleResult(int stationId, FhssNetwork[] networks) 
        {
            StationId = stationId;
            Networks = networks;
        }
        public FhssCycleResult(int stationId, IEnumerable<FhssNetwork> networks)
        {
            StationId = stationId;
            Networks = networks.ToArray();
        }
    }
}
