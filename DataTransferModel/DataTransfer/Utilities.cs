﻿using System;
using System.Runtime.Serialization;
using ModelsTablesDBLib;

namespace DataTransferModel.DataTransfer
{
    public enum DisconnectedState
    {
        Disconnected,
        Removed
    }

    public enum ErrorType
    {
        ConnectionError,
        DisconnectionError,
        InvalidEndPointError,
        SendMessageError
    }

    public struct ErrorArgs
    {
        public ErrorType Type { get; private set; }
        public string Message { get; private set; }

        public ErrorArgs(ErrorType type, string message)
        {
            Type = type;
            Message = message;
        }

        public ErrorArgs(ErrorType type, object exception)
        {
            Type = type;
            Message = exception.ToString();
        }
    }

    public struct Message
    {
        public int SenderId { get; private set; }
        public int ReceiverId { get; private set; }
        public string Text { get; private set; }

        public Message(int senderId, int receiverId, string text)
        {
            SenderId = senderId;
            ReceiverId = receiverId;
            Text = text;
        }

        /// <summary>
        /// Updates sender id if it is equal to -1 (small hack for convinience)
        /// </summary>
        public void UpdateSenderId(int senderId) 
        {
            if (SenderId == -1)
                SenderId = senderId;
        }
    }

    public struct ExecutiveDfRequestArgs
    {
        public int StationId { get; }
        public float StartFrequencyKhz { get; }
        public float EndFrequencyKhz { get; }
        public int PhaseAveragingCount { get; }
        public int DirectionAveragingCount { get; }

        public ExecutiveDfRequestArgs(int stationId, float startFrequencyKhz, float endFrequencyKhz, int phaseAveragingCount, int directionAveragingCount)
        {
            StationId = stationId;
            StartFrequencyKhz = startFrequencyKhz;
            EndFrequencyKhz = endFrequencyKhz;
            PhaseAveragingCount = phaseAveragingCount;
            DirectionAveragingCount = directionAveragingCount;
        }
    }

    public struct ExecutiveDfResponseArgs
    {
        public float FrequencyKhz { get; }
        public int StationId { get; }
        public float Direction { get; }

        public ExecutiveDfResponseArgs(float? frequencyKhz, float direction, int stationId)
        {
            FrequencyKhz = frequencyKhz ?? -1;
            Direction = direction;
            StationId = stationId;
        }
    }
    
    [DataContract]
    public struct AspTable
    {
        [DataMember] public byte Mode;

        [DataMember] public int StationId;

        [DataMember] public Coord Coordinates;

        [DataMember] public CompassesValues Compasses;

        public AspTable(byte mode, int stationId, Coord coordinates, CompassesValues compasses)
        {
            Mode = mode;
            StationId = stationId;
            Coordinates = coordinates;
            Compasses = compasses;
        }

        public override bool Equals(object obj)
        {
            try
            {
                var table = (AspTable) obj;

                return this.Mode == table.Mode &&
                       this.StationId == table.StationId &&
                       this.Coordinates.Altitude == table.Coordinates.Altitude &&
                       this.Coordinates.Latitude == table.Coordinates.Latitude &&
                       this.Coordinates.Longitude == table.Coordinates.Longitude &&
                       this.Compasses.Lpa13 == table.Compasses.Lpa13 &&
                       this.Compasses.Lpa24 == table.Compasses.Lpa24 &&
                       this.Compasses.Lpa510 == table.Compasses.Lpa510 &&
                       this.Compasses.Rrs1 == table.Compasses.Rrs1 &&
                       this.Compasses.Rrs2 == table.Compasses.Rrs2 &&
                       this.Compasses.BPSS == table.Compasses.BPSS &&
                       this.Compasses.LPA57 == table.Compasses.LPA57 &&
                       this.Compasses.LPA59 == table.Compasses.LPA59 &&
                       this.Compasses.LPA10 == table.Compasses.LPA10;
            }
            catch
            {
                return false;
            }
        }
    }

    [DataContract]
    public struct RdfCycleResult
    {
        [DataMember]
        public int StationId;

        //todo: it is not used, since we gather in packeges
        [DataMember]
        public int BandNumber;

        [DataMember]
        public Signal[] Signals;
    }

    [DataContract]
    public struct Signal
    {
        [DataMember]
        public float FrequencyKhz;
        
        [DataMember]
        public float CentralFrequencyKhz;

        [DataMember]
        public float BandwidthKhz;

        [DataMember]
        public float Direction;

        [DataMember]
        public float Amplitude;

        [DataMember]
        public float StandardDeviation;

        [DataMember]
        public float Altitude;

        [DataMember]
        public float Longitude;

        [DataMember]
        public float Latitude;

        [DataMember]
        public float DiscardedDirectionsPart;
        
        [DataMember]
        public float PhaseDeviation;

        [DataMember]
        public float RelativeSubScanCount;

        [DataMember]
        public TimeSpan BroadcastTimeSpan;
    }
}
