﻿using System.ServiceModel;
using System;

namespace DataTransferModel.DataTransfer
{
    public interface IDataTransferCallback
    {
        [OperationContract(IsOneWay = true)]
        void Abort();

        [OperationContract(IsOneWay = true)]
        void MessageCallback(int senderId, int receiverId, string message);
        
        [OperationContract(IsOneWay = true)]
        void ModeCallback(byte mode);

        [OperationContract(IsOneWay = true)]
        void LocalTimeCallback(DateTime localTime);

        [OperationContract(IsOneWay = true)]
        void ExecutiveDfRequestCallback(int stationId, float startFrequencyKhz, float endFrequencyKhz, int phaseAveragingCount, int directionAveragingCount);

        [OperationContract(IsOneWay = true)]
        void ExecutiveDfResponseCallback(int stationId, float result);
        
        [OperationContract(IsOneWay = true)]
        void SetBandCallback(int bandNumber);

        [OperationContract(IsOneWay = true)]
        void MasterRdfResultCallback(RdfCycleResult result);

        [OperationContract(IsOneWay = true)]
        void SlaveRdfResultCallback(RdfCycleResult result);

        [OperationContract(IsOneWay = true)]
        void FhssCycleResultCallback(FhssCycleResult result);
    }
}
