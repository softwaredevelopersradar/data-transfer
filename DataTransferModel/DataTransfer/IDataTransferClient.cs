﻿using System;

namespace DataTransferModel.DataTransfer
{
    public interface IDataTransferClient : IDataTransferBase
    {
        int Id { get; }
        bool IsConnected { get; }
        string EndpointAddress { get; }

        void Connect(string name);
        void Disconnect();
        void SendMessage(int senderId, int receiverId, string message);
        void SendMessage(int receiverId, string message);
        void SendMode(byte mode);
        void SendLocalTime(DateTime localTime);
        void SendExecutiveDfRequest(int stationId, float startFrequencyKhz, float endFrequencyKhz, int phaseAveragingCount,
            int directionAveragingCount);
        void SendExecutiveDfResponse(int stationId, float result);

        event EventHandler OnConnect;
        event EventHandler<DisconnectedState> OnDisconnect;
        event EventHandler<ErrorArgs> OnError;

                
        void SendSlaveRdfResult(RdfCycleResult result);
        void SendMasterRdfResult(RdfCycleResult result);
        void SendBandNumber(int bandNumber);

        void SendFhssCycleResult(FhssCycleResult result);
    }
}
