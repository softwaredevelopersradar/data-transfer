﻿namespace DataTransferModel.DataTransfer
{

    /// <summary>
    /// Struct to store station's compasses values, used only for transmission
    /// </summary>
    public struct CompassesValues
    {
        public short Lpa13 { get; }
        public short Lpa24 { get; }
        public short Lpa510 { get; }
        public short Rrs1 { get; }
        public short Rrs2 { get; }
        public short BPSS { get; set; }
        public short LPA57 { get; set; }
        public short LPA59 { get; set; }
        public short LPA10 { get; set; }

        public CompassesValues(short lpa13, short lpa24, short lpa510, short rrs1, short rrs2)
        {
            Lpa13 = lpa13;
            Lpa24 = lpa24;
            Lpa510 = lpa510;
            Rrs1 = rrs1;
            Rrs2 = rrs2;
            BPSS = -1;
            LPA57 = -1;
            LPA59 = -1;
            LPA10 = -1;
        }

        public CompassesValues(short lpa13, short lpa24, short lpa510, short rrs1, short rrs2, short bpss, short lpa57, short lpa59, short lpa10)
        {
            Lpa13 = lpa13;
            Lpa24 = lpa24;
            Lpa510 = lpa510;
            Rrs1 = rrs1;
            Rrs2 = rrs2;
            BPSS = bpss;
            LPA57 = lpa57;
            LPA59 = lpa59;
            LPA10 = lpa10;
        }
    }
}
