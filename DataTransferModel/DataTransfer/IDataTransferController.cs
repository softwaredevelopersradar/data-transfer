﻿using System;

namespace DataTransferModel.DataTransfer
{
    public interface IDataTransferController : IDataTransferBase
    {
        bool ShowRdfLog { get; set; }

        event EventHandler<string> OnLog;
        event EventHandler<string> OnError;

        void MessageReceived(int senderId, int receiverId, string message);
        void ModeReceived(byte mode);
        void LocalTimeReceived(DateTime localTime);
        void ExecutiveDfRequestReceived(ExecutiveDfRequestArgs requestArgs);
        void ExecutiveDfResponseReceived(int stationId, float result);

        void SlaveRdfResultReceived(RdfCycleResult result);
        void MasterRdfResultReceived(RdfCycleResult result);
        void BandReceived(int bandNumber);

        void FhssCycleResultReceived(FhssCycleResult result); 

        void Abort();
    }
}