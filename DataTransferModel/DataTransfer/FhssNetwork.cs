﻿using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace DataTransferModel.DataTransfer
{
    [DataContract]
    public struct FhssNetwork
    {
        [DataMember]
        public float FrequencyMinKhz { get; }
        [DataMember]
        public float FrequencyMaxKhz { get; }
        [DataMember]
        public FhssUserInfo[] UserInfo { get; }

        [DataMember]
        public float Altitude { get; }
        [DataMember]
        public float Latitude { get; }
        [DataMember]
        public float Longitude { get; }

        public FhssNetwork(float frequencyMihKhz, float frequencyMaxKhz, IEnumerable<FhssUserInfo> userInfo,
            float altitude, float latitude, float longitude) 
        {
            FrequencyMinKhz = frequencyMihKhz;
            FrequencyMaxKhz = frequencyMaxKhz;
            UserInfo = userInfo.ToArray();

            Altitude = altitude;
            Latitude = latitude;
            Longitude = longitude;
        }
    }
}
