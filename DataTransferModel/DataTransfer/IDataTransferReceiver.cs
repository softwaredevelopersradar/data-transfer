﻿using System;

namespace DataTransferModel.DataTransfer
{
    public interface IDataTransferBase
    {
        event EventHandler<Message> OnMessageReceived;
        event EventHandler<byte> OnModeReceived;
        event EventHandler<int> OnSetBandReceived;
        event EventHandler<DateTime> OnLocalTimeReceived;

        event EventHandler<ExecutiveDfRequestArgs> OnExecutiveDfRequestReceived;
        event EventHandler<ExecutiveDfResponseArgs> OnExecutiveDfResponseReceived;
        event EventHandler<RdfCycleResult> OnSlaveRdfResultReceived;
        event EventHandler<RdfCycleResult> OnMasterRdfResultReceived;
        event EventHandler<FhssCycleResult> OnFhssCycleResultReceived;
    }
}
