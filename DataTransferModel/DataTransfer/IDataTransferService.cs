﻿using System.ServiceModel;
using System;

namespace DataTransferModel.DataTransfer
{
    [ServiceContract(CallbackContract = typeof(IDataTransferCallback))]
    public interface IDataTransferService
    {
        [OperationContract]
        int Connect(string name);

        [OperationContract]
        void Disconnect(int clientId);

        [OperationContract(IsOneWay = true)]
        void SendMessage(int senderId, int receiverId, string message);
        
        [OperationContract(IsOneWay = true)]
        void SendMode(byte mode);

        [OperationContract(IsOneWay = true)]
        void SendLocalTime(DateTime localTime);

        [OperationContract(IsOneWay = true)]
        void SendExecutiveDfRequest(int stationId, float startFrequencyKhz, float endFrequencyKhz, int phaseAveragingCount, int directionAveragingCount);

        [OperationContract(IsOneWay = true)]
        void SendExecutiveDfResponse(int stationId, float result);
        
        [OperationContract(IsOneWay = true)]
        void SendSlaveRdfResult(RdfCycleResult result);

        [OperationContract(IsOneWay = true)]
        void SendMasterRdfResult(RdfCycleResult result);

        [OperationContract(IsOneWay = true)]
        void SetBandNumber(int bandNumber);

        [OperationContract(IsOneWay = true)]
        void SendFhssCycleResult(FhssCycleResult result);
    }
}
