﻿using System.ServiceModel;

namespace DataTransferControllerLibrary
{
    public class ServiceUser
    {
        public string Name { get; private set; }

        public OperationContext Context { get; private set; }

        public ServiceUser(string name, OperationContext context)
        {
            Name = name;
            Context = context;
        }
    }
}
