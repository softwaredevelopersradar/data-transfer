﻿using System.Runtime.Serialization;

namespace DataTransferModel.DataTransfer
{
    /// <summary>
    /// Information about possible user of fhss network
    /// </summary>
    [DataContract]
    public struct FhssUserInfo
    {
        [DataMember]
        public float Amplitude { get; }
        [DataMember]
        public float Direction { get; }
        [DataMember]
        public float StandardDeviation { get; }

        public FhssUserInfo(float amplitude, float direction, float standardDeviation)
        {
            Amplitude = amplitude;
            Direction = direction;
            StandardDeviation = standardDeviation;
        }
    }
}
