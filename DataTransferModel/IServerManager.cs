﻿using System;

namespace DataTransferModel
{
    public interface IServerManager
    {
        event EventHandler<string> OnLocalEvent;
        event EventHandler<string> OnGlobalEvent;
        event EventHandler<string> OnError;
        event EventHandler<bool> OnDatabaseConnectionStateChangedEvent;

        /// <summary>
        /// Aborts all connections for further shut down
        /// </summary>
        void ShutDown();

        void SetRdfLogVisibility(bool flag);
    }
}
