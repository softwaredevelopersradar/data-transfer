﻿namespace DataTransferModel
{
    public enum EthernetConnectionType : byte 
    {
        Fast,
        Slow
    }
}
