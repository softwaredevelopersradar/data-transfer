﻿using DataTransferModel;
using System.ComponentModel;
using System.Windows.Controls.WpfPropertyGrid;

namespace OD
{
    [CategoryOrder("Connection settings", 1)]
    //[CategoryOrder("Интервал опроса, c", 2)]
    public class LocalSettings : INotifyPropertyChanged
    {
        public LocalSettings()
        {
            PU_CType = PU_ConnectionType.APD;

            Tainet = new COM();
            MobileNetwork = new IPnPort();
            RRS = new IPnPort();
            FiberOptics = new IPnPort();
            Database = new IPnPort();
            DataTransfer = new IPnPort();
            APD = new COM();
        }

        public string GetIP()
        {
            return "127.0.0.1";
        }

        [Category("Control post")]
        [DisplayName("Connection type")]
        public PU_ConnectionType PU_CType { get; set; }

        private int _timeoutSec;
        [Category("Secondary settings")]
        [DisplayName("Timeout, seconds")]
        public int TimeoutSec { get { return _timeoutSec; } 
            set 
            {
                if (value <= 0) return;

                Constants.DeadlineSec = value;
                _timeoutSec = value;
            } 
        }

        [Category("Secondary settings")]
        [DisplayName("Ethernet connection type")]
        public EthernetConnectionType ConnectionType { get; set; }

        [Category("Connection settings")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [PropertyOrder(1)]
        [DisplayName("Tainet")]
        public COM Tainet { get; set; }

        [Category("Connection settings")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [PropertyOrder(2)]
        [DisplayName("3G/4G")]
        public IPnPort MobileNetwork { get; set; }

        [Category("Connection settings")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [PropertyOrder(3)]
        [DisplayName("RRC")]
        public IPnPort RRS { get; set; }

        [Category("Connection settings")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [PropertyOrder(4)]
        [DisplayName("Optical fiber")]
        public IPnPort FiberOptics { get; set; }

        [Category("Connection settings")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [PropertyOrder(5)]
        [DisplayName("APD")]
        public COM APD { get; set; }

        [Category("Database settings")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [DisplayName("Address")]
        public IPnPort Database { get; set; }

        [Category("Data transfer")]
        [PropertyOrder(2)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [DisplayName("Address")]
        public IPnPort DataTransfer { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
    }

    public class IPnPort : INotifyPropertyChanged
    {
        private string _IP;
        private int _Port;

        [NotifyParentProperty(true)]
        public string IP
        {
            get { return _IP; }
            set
            {
                if (_IP == value) return;
                _IP = value;
                OnPropertyChanged("IP");
            }
        }

        [NotifyParentProperty(true)]
        public int Port
        {
            get { return _Port; }
            set
            {
                if (_Port == value) return;
                _Port = value;
                OnPropertyChanged("Port");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public IPnPort()
        {
            IP = "127.0.0.1";
            Port = 10009;
        }

        /// <summary>
        /// ctor for datatransfer host
        /// </summary>
        public IPnPort(string ip)
        {
            IP = ip;
            Port = 10009;
        }

        public override string ToString()
        {
            return IP + " : " + Port;
        }
    }

    public class COM : INotifyPropertyChanged
    {
        private string _COMPort;
        private SpeedRate _Rate;

        [NotifyParentProperty(true)]
        public string COMPort
        {
            get { return _COMPort; }
            set
            {
                if (_COMPort == value) return;
                _COMPort = value;
                OnPropertyChanged("COMPort");
            }
        }

        [NotifyParentProperty(true)]
        public SpeedRate Rate
        {
            get { return _Rate; }
            set
            {
                if (_Rate == value) return;
                _Rate = value;
                OnPropertyChanged("Rate");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public COM()
        {
            _COMPort = "COM1";
            _Rate = SpeedRate.Speed_9600;
        }

        public override string ToString()
        {
            return _COMPort + " / " + (int)_Rate;
        }
    }

    public enum SpeedRate
    {
        [Description("2400")]
        Speed_2400 = 2400,
        [Description("4800")]
        Speed_4800 = 4800,
        [Description("9600")]
        Speed_9600 = 9600,
        [Description("19200")]
        Speed_19200 = 19200,
        [Description("38400")]
        Speed_38400 = 38400,
        [Description("57600")]
        Speed_57600 = 57600,
        [Description("115200")]
        Speed_115200 = 115200
    }

    public enum OD_ConnectionType
    {
        [Description("3G/4G")]
        MobileConnection,
        [Description("RRC")]
        RRS,
        [Description("Optical fiber")]
        FiberOptics,
        [Description("Tainet")]
        Tainet
    }

    public enum PU_ConnectionType
    {
        [Description("APD")]
        APD
    }
}
