﻿using DataTransferControllerLibrary;
using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.ServiceModel;
using System.Threading;
using System.Windows;
using ServerControllerLibrary;
using DataTransferModel;
using TransmissionLibrary.GrpcTransmission;
using TransmissionLibrary.ComTransmission;
using DataTransferModel.Trasmission;
using TransmissionLibrary.SlowUdpTransmission;

namespace OD
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private LocalSettings localSettings;
        private Yaml yaml = new Yaml();
        private Server ODServer = new Server();

        private readonly ServiceHost _dataTransferServiceHost;
        private readonly IServerManager _serverManager;
        private readonly DataTransferController _dataTransferController;

        private readonly ITransmitterFactory _ethernetTransmitterFactory;
        private readonly SlowTcpTransmitterFactory _slowEthernetTransmitterFactory;
        private readonly ITransmitterFactory _mobile3gTransmitterFactory;
        private readonly ITransmitterFactory _comTransmitterFactory;

        public MainWindow()
        {
            InitializeComponent();
            localSettings = yaml.YamlLoad<LocalSettings>("LocalSettings.yaml");
            MyPG.SelectedObject = localSettings;

            _ethernetTransmitterFactory = new GrpcTransmitterFactory(localSettings.RRS.IP, localSettings.RRS.Port, localSettings.ConnectionType);
            _mobile3gTransmitterFactory = new GrpcTransmitterFactory(localSettings.MobileNetwork.IP, localSettings.MobileNetwork.Port);
            //_comTransmitterFactory = new ComTransmitterFactory(localSettings.Tainet.COMPort);
            _comTransmitterFactory = new ComTransmitterFactory(localSettings.Tainet.COMPort, (int)localSettings.Tainet.Rate);

            CheckIfTheAppIsAlreadyRunning();
            _dataTransferController = new DataTransferController();
            _dataTransferServiceHost = InitializeServiceHost();
            SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());

            SlowTcpTransmitterFactory.SetDelay(localSettings.TimeoutSec);
            
            try
            {
                _dataTransferServiceHost.Open();
                _serverManager = new ServerManager(_dataTransferController, localSettings.Database.IP, localSettings.Database.Port,
                    ethernetTransmitterFactory: _ethernetTransmitterFactory,
                    mobile3gTransmitterFactory: _mobile3gTransmitterFactory,
                    comTransmitterFactory: _comTransmitterFactory,
                    localSettings.ConnectionType);

                SubscribeServerControllerToEvents(_serverManager);
            }
            catch(Exception e)
            {
                AddToLocalLog(e.ToString());
                MessageBox.Show("Error during hosting wcf service\r\n" +
                                "Check host ip address and port and try again\r\n" +
                                "Also make sure the programm is running under administrator");
            }
        }

        private ServiceHost InitializeServiceHost()
        {
            var address_HTTP = $"Http://{localSettings.DataTransfer.IP}:{localSettings.DataTransfer.Port - 1}";
            var address_TCP = $"net.tcp://{localSettings.DataTransfer.IP}:{localSettings.DataTransfer.Port}";

            Uri[] address_base = { new Uri(address_HTTP), new Uri(address_TCP) };

            ServiceHost serviceHost = new ServiceHost(_dataTransferController, address_base);
            serviceHost.CloseTimeout = TimeSpan.MaxValue;

            NetTcpBinding netTcpBinding = new NetTcpBinding();
            netTcpBinding.ReliableSession.Enabled = true;
            netTcpBinding.ReliableSession.InactivityTimeout =
                new TimeSpan(TimeSpan.MaxValue.Hours,
                    TimeSpan.MaxValue.Minutes, TimeSpan.MaxValue.Seconds);
            netTcpBinding.ReceiveTimeout = TimeSpan.MaxValue;
            netTcpBinding.CloseTimeout = TimeSpan.MaxValue;
            netTcpBinding.SendTimeout = TimeSpan.FromSeconds(5);
            netTcpBinding.Security.Mode = SecurityMode.None;
            serviceHost.AddServiceEndpoint(typeof(DataTransferModel.DataTransfer.IDataTransferService), netTcpBinding, "");
            return serviceHost;
        }

        private void CheckIfTheAppIsAlreadyRunning()
        {
            bool exists;
            // получаем GIUD приложения
            string guid = Marshal.GetTypeLibGuidForAssembly(Assembly.GetExecutingAssembly()).ToString();
            var mutexObj = new Mutex(true, guid, out exists);
            if (!exists)
            {
                LocalTextBlock.Text = "The application is already running";
                Thread.Sleep(3000);
                this.Close();
            }
        }

        private void SubscribeServerControllerToEvents(IServerManager serverManager)
        {
            serverManager.OnLocalEvent += (sender, s) => { AddToLocalLog(s); };
            serverManager.OnGlobalEvent += (sender, s) => { AddToGlobalLog(s); };
            serverManager.OnError += (sender, s) => { AddToLocalLog($"Error :\r\n{s}"); };
            serverManager.OnDatabaseConnectionStateChangedEvent += (sender, state) =>
            {
                Dispatcher.Invoke(() =>
                {
                    if (state)
                    {
                        DBControlConnection.ShowConnect();
                    }
                    else
                    {
                        DBControlConnection.ShowDisconnect();
                    }
                });
            };
        }

        private int _localLogCounter = 0;
        private int _globalLogCounter = 0;
        private const int LogClearageThreshold = 20;

        private void AddToLocalLog(string text)
        {
            Dispatcher?.Invoke(() =>
            {
                _localLogCounter++;
                if (_localLogCounter == LogClearageThreshold)
                {
                    LocalTextBlock.Text = "";
                    _localLogCounter = 0;
                }
                LocalTextBlock.Text += $"{DateTime.Now.ToLongTimeString()} : {text}\r\n";
            });
        }

        private void AddToGlobalLog(string text)
        {
            Dispatcher?.Invoke(() => 
            {
                _globalLogCounter++;
                if (_globalLogCounter == LogClearageThreshold)
                {
                    GlobalTextBlock.Text = "";
                    _globalLogCounter = 0;
                }
                GlobalTextBlock.Text += $"{DateTime.Now.ToLongTimeString()} : {text}\r\n";
            });
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            yaml.YamlSave<LocalSettings>((LocalSettings)MyPG.SelectedObject, "LocalSettings.yaml");
            _serverManager.ShutDown();
            System.Windows.Threading.Dispatcher.ExitAllFrames();
        }

        private void APDConnection_Click(object sender, RoutedEventArgs e)
        {

        }

        private void RdfLogCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            _serverManager.SetRdfLogVisibility(RdfLogCheckBox.IsChecked ?? false);
        }

        private void RdfLogCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            _serverManager.SetRdfLogVisibility(RdfLogCheckBox.IsChecked ?? false);
        }
    }
}
