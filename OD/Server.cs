﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using ODProtocol;
using llcss;
using Nito.AsyncEx;
using System.Collections.Concurrent;

namespace OD
{
    public class Server
    {
        private const int port = 10003;
        private const int port2 = 10004;
        private TcpListener listener;

        private List<ClientObject> ListOfClientObjects = new List<ClientObject>();

        public delegate void ResponseNeeded(int Response, int extraCode);
        public event ResponseNeeded ResponseIsNeeded;

        Dictionary<int, int> Responses = new Dictionary<int, int>();

        public async Task ServerStart(string IP)
        {
            try
            {
                //Пробуем стартовать сервер
                listener = new TcpListener(IPAddress.Parse(IP), port);
                listener.Start();
                Console.WriteLine("Ожидание подключений...");
            }
            catch
            {
                listener = new TcpListener(IPAddress.Parse("127.0.0.1"), port);
                listener.Start();
                Console.WriteLine("Ожидание подключений...");
            }
            while (true)
            {
                TcpClient client = await listener.AcceptTcpClientAsync();
                ClientObject clientObject = new ClientObject(client);
                ListOfClientObjects.Add(clientObject);

                clientObject.ClientAdressResponse += clientObject_ClientAdressResponse;

                Console.WriteLine("Новый клиент");

                //создаем новый поток для обслуживания нового клиента
                Task.Run(() => clientObject.Read());
                var answer = await ServerSendAddressIdentificationRequest();
            }
        }

        public async Task ServerStart2(string IP)
        {
            try
            {
                //Пробуем стартовать сервер
                listener = new TcpListener(IPAddress.Parse(IP), port2);
                listener.Start();
                Console.WriteLine("Ожидание подключений...");
            }
            catch
            {
                listener = new TcpListener(IPAddress.Parse("127.0.0.1"), port2);
                listener.Start();
                Console.WriteLine("Ожидание подключений...");
            }
            while (true)
            {
                TcpClient client = await listener.AcceptTcpClientAsync();
                ClientObject clientObject = new ClientObject(client);
                ListOfClientObjects.Add(clientObject);

                clientObject.ClientAdressResponse += clientObject_ClientAdressResponse;

                Console.WriteLine("Новый клиент");

                //создаем новый поток для обслуживания нового клиента
                Task.Run(() => clientObject.Read());
                var answer = await ServerSendAddressIdentificationRequest();
            }
        }

        private void clientObject_ClientAdressResponse(int Adress, int Response, int extraCode)
        {
            if (ResponseIsNeeded != null)
            {
                Responses.Add(Response, Adress);
                ResponseIsNeeded(Response, extraCode);
            }
        }

        //Шифр 0
        public async Task<List<DefaultResponse>> ServerSendAddressIdentificationRequest()
        {
            List<DefaultResponse> ListOfDefaultResponses = new List<DefaultResponse>();
            for (int i = 0; i < ListOfClientObjects.Count; i++)
            {
                var result = await ListOfClientObjects[i].SendAddressIdentificationRequest(Convert.ToByte(i + 1));
                ListOfDefaultResponses.Add(result);
            }
            return ListOfDefaultResponses;
        }

        //Шифр 1
        public async Task<List<DefaultResponse>> ServerWSendStationLocation(byte ErrorCode, StationCoordinates[] stationCoordinates)
        {
            List<DefaultResponse> ListOfDefaultResponses = new List<DefaultResponse>();
            for (int i = 0; i < ListOfClientObjects.Count; i++)
            {
                var result = await ListOfClientObjects[i].SendStationLocation(ErrorCode, stationCoordinates);
                ListOfDefaultResponses.Add(result);
            }
            return ListOfDefaultResponses;
        }

        //Шифр 2
        public async Task<List<DefaultResponse>> ServerWSendStationStatus(byte ErrorCode, StationStatus[] stationsStatus)
        {
            List<DefaultResponse> ListOfDefaultResponses = new List<DefaultResponse>();
            for (int i = 0; i < ListOfClientObjects.Count; i++)
            {
                var result = await ListOfClientObjects[i].SendStationStatus(ErrorCode, stationsStatus);
                ListOfDefaultResponses.Add(result);
            }
            return ListOfDefaultResponses;
        }

        //Шифр 3
        public async Task<List<DefaultResponse>> ServerWSendModeS(byte ErrorCode, ModeS[] modeS)
        {
            List<DefaultResponse> ListOfDefaultResponses = new List<DefaultResponse>();
            for (int i = 0; i < ListOfClientObjects.Count; i++)
            {
                var result = await ListOfClientObjects[i].SendModeS(ErrorCode, modeS);
                ListOfDefaultResponses.Add(result);
            }
            return ListOfDefaultResponses;
        }

        //Шифр 4
        public async Task<List<DefaultResponse>> ServerWSendMLAT(byte ErrorCode, MLAT[] MLATs)
        {
            List<DefaultResponse> ListOfDefaultResponses = new List<DefaultResponse>();
            for (int i = 0; i < ListOfClientObjects.Count; i++)
            {
                var result = await ListOfClientObjects[i].SendMLAT(ErrorCode, MLATs);
                ListOfDefaultResponses.Add(result);
            }
            return ListOfDefaultResponses;
        }


        //Шифр 1
        public async Task<DefaultResponse> ServerSendStationLocation(byte ErrorCode, StationCoordinates[] stationCoordinates)
        {
            int i = Responses[1] - 1;
            Responses.Remove(1);
            var result = await ListOfClientObjects[i].SendStationLocation(ErrorCode, stationCoordinates);
            return result;
        }

        //Шифр 2
        public async Task<DefaultResponse> ServerSendStationStatus(byte ErrorCode, StationStatus[] stationsStatus)
        {
            int i = Responses[2] - 1;
            Responses.Remove(2);
            var result = await ListOfClientObjects[i].SendStationStatus(ErrorCode, stationsStatus);
            return result;
        }

        //Шифр 3
        public async Task<DefaultResponse> ServerSendModeS(byte ErrorCode, ModeS[] modeS)
        {
            int i = Responses[3] - 1;
            Responses.Remove(3);
            var result = await ListOfClientObjects[i].SendModeS(ErrorCode, modeS);
            return result;
        }

        //Шифр 4
        public async Task<DefaultResponse> ServerSendMLAT(byte ErrorCode, MLAT[] MLATs)
        {
            int i = Responses[4] - 1;
            Responses.Remove(4);
            var result = await ListOfClientObjects[i].SendMLAT(ErrorCode, MLATs);
            return result;
        }


    }

    public class ClientObject
    {
        public TcpClient client;

        private readonly AsyncLock asyncLock;

        private readonly ConcurrentDictionary<int, ConcurrentQueue<TaskCompletionSource<IBinarySerializable>>> concurrentDictionary;

        public delegate void IsConnectedEventHandler(bool isConnected);
        public event IsConnectedEventHandler ClientIsConnected;

        public delegate void OnReadEventHandler(bool isRead);
        public event OnReadEventHandler ClientIsRead;

        public delegate void IsWriteEventHandler(bool isWrite);
        public event IsWriteEventHandler ClientIsWrite;

        public delegate void AdressAndResponse(int Adress, int Response, int extraCode);
        public event AdressAndResponse ClientAdressResponse;

        public int ClientAdress = 0;

        public ClientObject(TcpClient tcpClient)
        {
            client = tcpClient;
            asyncLock = new AsyncLock();
            concurrentDictionary = new ConcurrentDictionary<int, ConcurrentQueue<TaskCompletionSource<IBinarySerializable>>>();
        }


        public async Task Read()
        {
            while (client.Connected)
            {
                var headerBuffer = new byte[MessageHeader.BinarySize];
                var header = new MessageHeader();
                var count = 0;

                try
                {
                    count = await client.GetStream().ReadAsync(headerBuffer, 0, MessageHeader.BinarySize).ConfigureAwait(false);
                    if (ClientIsRead != null)
                        ClientIsRead(true);
                }
                catch (Exception)
                {
                    //Console.WriteLine("Сервер отвалился");
                    if (ClientIsRead != null)
                        ClientIsRead(false);
                    if (ClientIsConnected != null)
                        ClientIsConnected(false);
                    //break;
                }
                if (count != MessageHeader.BinarySize)
                {
                    //fatal error
                    //break;
                    if (ClientIsRead != null)
                        ClientIsRead(false);
                    return;
                }

                MessageHeader.TryParse(headerBuffer, out header);

                switch (header.Code)
                {
                    case 0:
                        Console.WriteLine(headerBuffer);
                        break;
                    case 1:
                        var response1 = await ReadResponse<DefaultResponse>(header, headerBuffer).ConfigureAwait(false);
                        //if (ClientAdressResponse != null) ClientAdressResponse(header.SenderAddress, header.Code, 0);
                        if (ClientAdressResponse != null) ClientAdressResponse(ClientAdress, header.Code, 0);
                        break;
                    case 2:
                        var response2 = await ReadResponse<StationStatusRequest>(header, headerBuffer).ConfigureAwait(false);
                        //if (ClientAdressResponse != null) ClientAdressResponse(header.SenderAddress, header.Code, response2.StationNumber);
                        if (ClientAdressResponse != null) ClientAdressResponse(ClientAdress, header.Code, response2.StationNumber);
                        break;
                    case 3:
                        var response3 = await ReadResponse<DefaultResponse>(header, headerBuffer).ConfigureAwait(false);
                        //if (ClientAdressResponse != null) ClientAdressResponse(header.SenderAddress, header.Code, 0);
                        if (ClientAdressResponse != null) ClientAdressResponse(ClientAdress, header.Code, 0);
                        break;
                    case 4:
                        var response4 = await ReadResponse<DefaultResponse>(header, headerBuffer).ConfigureAwait(false);
                        //if (ClientAdressResponse != null) ClientAdressResponse(header.SenderAddress, header.Code, 0);
                        if (ClientAdressResponse != null) ClientAdressResponse(ClientAdress, header.Code, 0);
                        break;
                    default:
                        //вывод полезной информации
                        break;
                }
            }
        }


        private async Task<T> ReadResponse<T>(MessageHeader header, byte[] headerBuffer) where T : class, IBinarySerializable, new()
        {
            var count = 0;
            var buffer = new byte[MessageHeader.BinarySize + header.InformationLength];
            headerBuffer.CopyTo(buffer, 0);

            if (header.InformationLength != 0)
            {
                var difference = header.InformationLength;
                var shiftpos = 0;

                while (difference != 0)
                {
                    count = await client.GetStream().ReadAsync(buffer, MessageHeader.BinarySize + shiftpos, difference).ConfigureAwait(false);
                    if (ClientIsRead != null)
                        ClientIsRead(true);
                    shiftpos += count;
                    difference = difference - count;
                }

            }

            try
            {
                var result = new T();
                result.Decode(buffer, 0);
                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }

        static MessageHeader GetMessageHeader(int code, int length)
        {
            return new MessageHeader(0, 1, (byte)code, 0, length);
        }

        private async Task SendDefault(MessageHeader header)
        {
            using (await asyncLock.LockAsync().ConfigureAwait(false))
            {
                try
                {
                    await client.GetStream().WriteAsync(header.GetBytes(), 0, MessageHeader.BinarySize).ConfigureAwait(false);
                    if (ClientIsWrite != null)
                        ClientIsWrite(true);
                }
                catch (Exception)
                {
                    if (ClientIsWrite != null)
                        ClientIsWrite(false);
                }
            }
        }

        private async Task SendRequest(MessageHeader header, byte[] message)
        {
            using (await asyncLock.LockAsync().ConfigureAwait(false))
            {
                int count = message.Count();
                try
                {
                    await client.GetStream().WriteAsync(message, 0, count).ConfigureAwait(false);
                    if (ClientIsWrite != null)
                        ClientIsWrite(true);
                }
                catch (Exception)
                {
                    if (ClientIsWrite != null)
                        ClientIsWrite(false);
                }
            }
        }

        private async Task SendResponse<T>(T Response) where T : class, IBinarySerializable, new()
        {
            using (await asyncLock.LockAsync().ConfigureAwait(false))
            {
                try
                {
                    await client.GetStream().WriteAsync(Response.GetBytes(), 0, Response.StructureBinarySize).ConfigureAwait(false);
                    if (ClientIsWrite != null)
                        ClientIsWrite(true);
                }
                catch (Exception)
                {
                    if (ClientIsWrite != null)
                        ClientIsWrite(false);
                }
            }
        }

        //Шифр 0 
        public async Task<DefaultResponse> SendAddressIdentificationRequest(byte ReceiverAddress)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                ClientAdress = ReceiverAddress;

                MessageHeader header = GetMessageHeader(code: 0, length: 0);
                header.ReceiverAddress = ReceiverAddress;
                byte[] message = DefaultRequest.ToBinary(header);

                //await SendRequest(header, message).ConfigureAwait(false);
                DefaultResponse answer = new DefaultResponse(header);
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 0, 0, 0);
                DefaultResponse answer = new DefaultResponse(header);
                return answer;
            }
        }

        //Шифр 1 
        public async Task<DefaultResponse> SendStationLocation(byte ErrorCode, StationCoordinates[] stationCoordinates)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(1, 1 + StationCoordinates.BinarySize * stationCoordinates.Count());
                byte[] message = StationLocationResponse.ToBinary(header, ErrorCode, stationCoordinates);

                await SendRequest(header, message).ConfigureAwait(false);
                DefaultResponse answer = new DefaultResponse(header);
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 1, 0, 0);
                DefaultResponse answer = new DefaultResponse(header);
                return answer;
            }
        }

        //Шифр 2
        public async Task<DefaultResponse> SendStationStatus(byte ErrorCode, StationStatus[] stationsStatus)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(2, 1 + StationStatus.BinarySize * stationsStatus.Count());
                byte[] message = StationStatusResponse.ToBinary(header, ErrorCode, stationsStatus);

                await SendRequest(header, message).ConfigureAwait(false);
                DefaultResponse answer = new DefaultResponse(header);
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 2, 0, 0);
                DefaultResponse answer = new DefaultResponse(header);
                return answer;
            }
        }

        //Шифр 3 
        public async Task<DefaultResponse> SendModeS(byte ErrorCode, ModeS[] modeS)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(3, 1 + ModeS.BinarySize * modeS.Length);
                byte[] message = ModeSResponse.ToBinary(header, ErrorCode, modeS);

                await SendRequest(header, message).ConfigureAwait(false);
                DefaultResponse answer = new DefaultResponse(header);
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 3, 0, 0);
                DefaultResponse answer = new DefaultResponse(header);
                return answer;
            }
        }

        //Шифр 4 
        public async Task<DefaultResponse> SendMLAT(byte ErrorCode, MLAT[] MLATs)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(4, 1 + MLAT.BinarySize * MLATs.Length);
                byte[] message = MLATResponce.ToBinary(header, ErrorCode, MLATs);

                await SendRequest(header, message).ConfigureAwait(false);
                DefaultResponse answer = new DefaultResponse(header);
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 4, 0, 0);
                DefaultResponse answer = new DefaultResponse(header);
                return answer;
            }
        }



    }
}
