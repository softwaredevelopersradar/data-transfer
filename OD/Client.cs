﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using ODProtocol;
using llcss;
using Nito.AsyncEx;
using System.Collections.Concurrent;
using System.Threading;

namespace OD
{
    public class Client
    {
        private TcpClient client;
        private string IP;
        private const int port = 10003;

        private byte ReceiverAddress;

        private AsyncLock asyncLock;

        private ConcurrentDictionary<int, ConcurrentQueue<TaskCompletionSource<IBinarySerializable>>> concurrentDictionary;

        public delegate void IsConnectedEventHandler(bool isConnected);
        public event IsConnectedEventHandler IsConnected;

        public delegate void OnReadEventHandler(bool isRead);
        public event OnReadEventHandler ClientIsRead;

        public delegate void IsWriteEventHandler(bool isWrite);
        public event IsWriteEventHandler ClientIsWrite;

        bool disconnect = false;

        public delegate void Code1Event(StationLocationResponse stationLocationResponse);
        public event Code1Event Code1;

        public delegate void Code2Event(StationStatusResponse stationStatusResponse);
        public event Code2Event Code2;

        public delegate void Code3Event(ModeSResponse modeSResponse);
        public event Code3Event Code3;

        public delegate void Code4Event(MLATResponce MLATsResponce);
        public event Code4Event Code4;

        public async Task ConnectToServer(string IP)
        {
            disconnect = false;
            client = new TcpClient();

            asyncLock = new AsyncLock();
            concurrentDictionary = new ConcurrentDictionary<int, ConcurrentQueue<TaskCompletionSource<IBinarySerializable>>>();

            while (!client.Connected)
            {
                try
                {
                    await client.ConnectAsync(IPAddress.Parse(IP), port).ConfigureAwait(false);

                    this.IP = IP;
                    //this.port = port;
                }
                catch (Exception)
                {

                }
                if (!client.Connected)
                    await Task.Delay(500);
            }

            Console.WriteLine("Connect");

            Task.Run(() => Read());

            if (IsConnected != null)
                IsConnected(true);
        }

        public async Task ConnectToServer2(string IP, CancellationToken token)
        {
            disconnect = false;
            client = new TcpClient();

            asyncLock = new AsyncLock();
            concurrentDictionary = new ConcurrentDictionary<int, ConcurrentQueue<TaskCompletionSource<IBinarySerializable>>>();

            while (!client.Connected)
            {
                if (token.IsCancellationRequested)
                    return;
                try
                {
                    await client.ConnectAsync(IPAddress.Parse(IP), port).ConfigureAwait(false);

                    this.IP = IP;
                    //this.port = port;
                }
                catch (Exception)
                {

                }
                if (!client.Connected)
                    await Task.Delay(500);
            }

            Console.WriteLine("Connect");

            Task.Run(() => Read());

            if (IsConnected != null)
                IsConnected(true);
        }


        public void DisconnectFromServer()
        {
            disconnect = true;
            if (client != null)
                client.Close();
            if (IsConnected != null)
                IsConnected(false);
        }


        private async Task Read()
        {
            while (client.Connected)
            {
                var headerBuffer = new byte[MessageHeader.BinarySize];
                var header = new MessageHeader();
                var count = 0;

                try
                {
                    count = await client.GetStream().ReadAsync(headerBuffer, 0, MessageHeader.BinarySize).ConfigureAwait(false);
                    if (ClientIsRead != null)
                        ClientIsRead(true);
                }
                catch (Exception)
                {
                    //Console.WriteLine("Сервер отвалился");
                    if (ClientIsRead != null)
                        ClientIsRead(false);
                    if (IsConnected != null)
                        IsConnected(false);
                    if (disconnect == false)
                        Task.Run(() => ConnectToServer(IP));
                    //break;
                }
                if (count != MessageHeader.BinarySize)
                {
                    //fatal error
                    //break;
                    if (ClientIsRead != null)
                        ClientIsRead(false);
                    return;
                }

                MessageHeader.TryParse(headerBuffer, out header);

                switch (header.Code)
                {
                    case 0:
                        Console.WriteLine(headerBuffer);
                        ReceiverAddress = header.ReceiverAddress;
                        break;
                    case 1:
                        var response1 = await ReadResponse<StationLocationResponse>(header, headerBuffer).ConfigureAwait(false);
                        if (Code1 != null) Code1(response1);
                        break;
                    case 2:
                        var response2 = await ReadResponse<StationStatusResponse>(header, headerBuffer).ConfigureAwait(false);
                        if (Code2 != null) Code2(response2);
                        break;
                    case 3:
                        var response3 = await ReadResponse<ModeSResponse>(header, headerBuffer).ConfigureAwait(false);
                        if (Code3 != null) Code3(response3);
                        break;
                    case 4:
                        var response4 = await ReadResponse<MLATResponce>(header, headerBuffer).ConfigureAwait(false);
                        if (Code4 != null) Code4(response4);
                        break;
                    default:
                        //вывод полезной информации
                        break;

                }
            }
        }

        private async Task<T> ReadResponse<T>(MessageHeader header, byte[] headerBuffer) where T : class, IBinarySerializable, new()
        {
            var count = 0;
            var buffer = new byte[MessageHeader.BinarySize + header.InformationLength];
            headerBuffer.CopyTo(buffer, 0);

            if (header.InformationLength != 0)
            {
                var difference = header.InformationLength;
                var shiftpos = 0;

                while (difference != 0)
                {
                    count = await client.GetStream().ReadAsync(buffer, MessageHeader.BinarySize + shiftpos, difference).ConfigureAwait(false);
                    if (ClientIsRead != null)
                        ClientIsRead(true);
                    shiftpos += count;
                    difference = difference - count;
                }

            }

            try
            {
                var result = new T();
                result.Decode(buffer, 0);
                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }

        static MessageHeader GetMessageHeader(int code, int length)
        {
            return new MessageHeader(0, 1, (byte)code, 0, length);
        }

        private async Task SendRequest(MessageHeader header, byte[] message)
        {
            using (await asyncLock.LockAsync().ConfigureAwait(false))
            {
                int count = message.Count();
                try
                {
                    await client.GetStream().WriteAsync(message, 0, count).ConfigureAwait(false);
                    if (ClientIsWrite != null)
                        ClientIsWrite(true);
                }
                catch (Exception)
                {
                    if (ClientIsWrite != null)
                        ClientIsWrite(false);
                }
            }
        }

        private async Task SendDefault(MessageHeader header)
        {
            using (await asyncLock.LockAsync().ConfigureAwait(false))
            {
                try
                {
                    await client.GetStream().WriteAsync(header.GetBytes(), 0, MessageHeader.BinarySize).ConfigureAwait(false);
                    if (ClientIsWrite != null)
                        ClientIsWrite(true);
                }
                catch (Exception)
                {
                    if (ClientIsWrite != null)
                        ClientIsWrite(false);
                }
            }
        }

        private async Task SendResponse<T>(T Response) where T : class, IBinarySerializable, new()
        {
            using (await asyncLock.LockAsync().ConfigureAwait(false))
            {
                try
                {
                    await client.GetStream().WriteAsync(Response.GetBytes(), 0, Response.StructureBinarySize).ConfigureAwait(false);
                    if (ClientIsWrite != null)
                        ClientIsWrite(true);
                }
                catch (Exception)
                {
                    if (ClientIsWrite != null)
                        ClientIsWrite(false);
                }
            }
        }


        //Шифр 1
        public async Task<DefaultResponse> ClientStationLocationRequest()
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(1, 0);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = DefaultRequest.ToBinary(header);

                await SendRequest(header, message).ConfigureAwait(false);

                DefaultResponse answer = new DefaultResponse(header);
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 1, 0, 0);
                DefaultResponse answer = new DefaultResponse(header);
                return answer;
            }
        }

        //Шифр 2
        public async Task<DefaultResponse> ClientStationStatusRequest(byte stationNumber)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(2, 1);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = StationStatusRequest.ToBinary(header, stationNumber);

                await SendRequest(header, message).ConfigureAwait(false);

                DefaultResponse answer = new DefaultResponse(header);
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 2, 0, 0);
                DefaultResponse answer = new DefaultResponse(header);
                return answer;
            }
        }

        //Шифр 3
        public async Task<DefaultResponse> ClientModeSRequest()
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(3, 0);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = DefaultRequest.ToBinary(header);

                await SendRequest(header, message).ConfigureAwait(false);

                DefaultResponse answer = new DefaultResponse(header);
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 3, 0, 0);
                DefaultResponse answer = new DefaultResponse(header);
                return answer;
            }
        }

        //Шифр 4
        public async Task<DefaultResponse> ClientMLATRequest()
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(4, 0);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = DefaultRequest.ToBinary(header);

                await SendRequest(header, message).ConfigureAwait(false);

                DefaultResponse answer = new DefaultResponse(header);
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 4, 0, 0);
                DefaultResponse answer = new DefaultResponse(header);
                return answer;
            }
        }

    }
}
