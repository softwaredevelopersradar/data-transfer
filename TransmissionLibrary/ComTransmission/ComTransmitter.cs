﻿using System;
using System.Linq;
using System.Collections.Generic;
using RsRxchangeDll;
using RsRxchangeDll.Serialization;
using ModelsTablesDBLib;
using ModelsTablesDBLib.Interfaces;
using DataTransferModel;
using DataTransferModel.Trasmission;
using DataTransferModel.DataTransfer;
using System.Threading;

namespace TransmissionLibrary.ComTransmission
{
    /*
     * A hack is used here, for tables with .NumberAsp property.
     * Since Tanya uses only interfaces (which do not include previously mentioned property),
     * an information record is added to the start of each table supplying this information via com port
     * and not breaking Tanya's models.
     */
    internal class ComTransmitter : ITransmitter
    {
        private readonly ManualResetEvent _manualResetEvent;
        private readonly RSDevice device;
        private readonly MessageQueue _queue = new MessageQueue();

        public bool IsMaster { get; private set; }
        public int OwnStationId { get; private set; }
        public int ConnectedStationId { get; private set; }

        public bool ShowRdfLog { get; set; }

        private bool _isConnected;
        public bool IsConnected
        {
            get => _isConnected;
            private set
            {
                if (_isConnected != value)
                    ConnectionStateChanged?.Invoke(this, value);
                _isConnected = value;
            }
        }

        public bool IsWorking { get; private set; }

        public string ServerAddress { get; private set; }

        public int ServerPort { get; private set; }

        public string ClientAddress => ServerAddress;

        public int ClientPort => ServerPort;

        public TransmissionType Type => TransmissionType.ComTransmitter;

        public int ComPortRate { get; private set; }

        public event EventHandler<string> OnSend;
        public event EventHandler<string> OnReceive;
        public event EventHandler<Message> OnSendMessageRequest;
        public event EventHandler<ExecutiveDfRequestArgs> OnSendExecutiveDfRequest;
        public event EventHandler<ExecutiveDfResponseArgs> OnSendExecutiveDfResponse;
        public event EventHandler<byte> OnSendModeRequest;
        public event EventHandler<int> OnSetBandReceived;
        public event EventHandler<DateTime> OnLocalTimeReceived;
        public event EventHandler<RdfCycleResult> OnMasterRdfResultReceived;
        public event EventHandler<RdfCycleResult> OnSlaveRdfResultReceived;
        public event EventHandler<AspTable> AspTableReceived;
        public event EventHandler<TableEventArgs<TableSectorsRangesRecon>> TableSectorsRangesReconReceived;
        public event EventHandler<TableEventArgs<TableSectorsRangesSuppr>> TableSectorsRangesSupprReceived;
        public event EventHandler<TableEventArgs<TableReconFWS>> TableFrsReconReceived;
        public event EventHandler<TableEventArgs<TableReconFHSS>> TableFhssReconReceived;
        public event EventHandler<TableEventArgs<TableSuppressFWS>> TableFrsJamReceived;
        public event EventHandler<TableEventArgs<TableSuppressFHSS>> TableFhssJamReceived;
        public event EventHandler<TableEventArgs<TableFHSSExcludedFreq>> TableFhssExcludedFreqReceived;
        public event EventHandler<TableEventArgs<TableFreqImportant>> TableFreqImportantReceived;
        public event EventHandler<TableEventArgs<TableFreqForbidden>> TableFreqForbiddenReceived;
        public event EventHandler<TableEventArgs<TableFreqKnown>> TableFreqKnownReceived;
        public event EventHandler<PeerInfo> OnForceConnectionRequest;
        public event EventHandler<bool> ConnectionStateChanged;
        public event EventHandler<FhssCycleResult> OnFhssCycleResultReceived;

        public ComTransmitter(int ownStationId, int connectedStationId, string comPortString, bool isMaster)
        {
            OwnStationId = ownStationId;
            ConnectedStationId = connectedStationId;
            device = new RSDevice(new ModelSerialization());
            ServerAddress = comPortString;
            ComPortRate = 9600;
            ServerPort = 0;
            IsMaster = isMaster;

            _manualResetEvent = new ManualResetEvent(false);
        }

        public ComTransmitter(int ownStationId, int connectedStationId, string comPortString, int comPortRate, bool isMaster)
        {
            OwnStationId = ownStationId;
            ConnectedStationId = connectedStationId;
            device = new RSDevice(new ModelSerialization());
            ServerAddress = comPortString;
            ComPortRate = comPortRate;
            ServerPort = 0;
            IsMaster = isMaster;

            _manualResetEvent = new ManualResetEvent(false);
        }

        private void SubscribeToDeviceEvents() 
        {
            (device.DictCommands[Requests.TextMess] as IEventRequest<string>).OnRequest += TextMess_Request;
            (device.DictCommands[Requests.TableAsp] as IEventRequest<List<IFixASP>>).OnRequest += TableAsp_Request;
            (device.DictCommands[Requests.TableSectorRangeRecon] as IEventRequest<List<IFixSectorRanges>>).OnRequest += TransmitterOnSectorRangesRecon;
            (device.DictCommands[Requests.TableSectorRangeSuppr] as IEventRequest<List<IFixSectorRanges>>).OnRequest += Transmitter_OnSectorsRangesSuppr;
            (device.DictCommands[Requests.TableFreqImportant] as IEventRequest<List<IFixSpecFreq>>).OnRequest += Transmitter_OnImportant;
            (device.DictCommands[Requests.TableFreqKnown] as IEventRequest<List<IFixSpecFreq>>).OnRequest += Transmitter_OnKnown;
            (device.DictCommands[Requests.TableFreqForbidden] as IEventRequest<List<IFixSpecFreq>>).OnRequest += Transmitter_OnForbidden;
            (device.DictCommands[Requests.TableSuppressFWS] as IEventRequest<List<IFixSupprFWS>>).OnRequest += Transmitter_OnFrsJam;
            (device.DictCommands[Requests.TableSuppressFHSS] as IEventRequest<List<IFixSupprFHSS>>).OnRequest += Transmitter_OnFhssJam;
            (device.DictCommands[Requests.TableReconFWS] as IEventRequest<List<IFixReconFWS>>).OnRequest += TransmitterOnReconFrs;
            (device.DictCommands[Requests.Synchronize] as IEventRequest<DateTime>).OnRequest += Transmitter_OnSetLocalTime;
            (device.DictCommands[Requests.Regime] as IEventRequest<byte>).OnRequest += TransmitterOnMode;
            (device.DictCommands[Requests.QuasiDirectFind] as IEventRequest<IQuasiRequest>).OnRequest += TransmitterOnQuasiDfRequest;
            (device.DictCommands[Requests.QuasiDirectFind] as IEventConfirm).OnConfirm += Transmitter_OnQuasiDfConfirm; ;
            (device.DictCommands[Requests.Ping] as IEventRequest<byte>).OnRequest += OnPingRequest;
            (device.DictCommands[Requests.Ping] as IEventConfirm).OnConfirm += OnPingConfirm;
        }

        private void Transmitter_OnQuasiDfConfirm(object sender, IConfirm result)
        {
            var resultTransformed = result as IConfirm<IQuasiConfirm>; //todo : wtf
            OnReceive?.Invoke(this, $"Received executive df response from {ConnectedStationId}");
            OnSendExecutiveDfResponse?.Invoke(this, new ExecutiveDfResponseArgs(-1, resultTransformed?.Model.Bearing ?? -1, resultTransformed?.Model.StationId ?? -1));
        }

        private void TransmitterOnMode(object sender, byte mode)
        {
            OnReceive?.Invoke(this, $"Received mode from {ConnectedStationId}");
            OnSendModeRequest?.Invoke(this, mode);
        }

        private void Transmitter_OnSetLocalTime(object sender, DateTime request)
        {
            //todo : not existing at all atm
        }

        private void TransmitterOnReconFrs(object sender, List<IFixReconFWS> table)
        {
            OnReceive?.Invoke(this, $"Received recon frs from {ConnectedStationId}");
            var parsed = new TableEventArgs<TableReconFWS>(ConnectedStationId,
                table.Select(ParseReconFrs).ToArray());
            TableFrsReconReceived?.Invoke(this, parsed);
        }

        /// <summary>
        /// Parses received table
        /// </summary>
        private TableReconFWS ParseReconFrs(IFixReconFWS record) 
        {
            var output = new TableReconFWS()
            {
                Coordinates = record.Coordinates,
                Deviation = record.Deviation,
                FreqKHz = record.FreqKHz,
                Time = record.Time,
                Type = record.Type,
                ListJamDirect = new System.Collections.ObjectModel.ObservableCollection<TableJamDirect>(
                            record.ListJamDirect.Select(ParseTableJamDirect).ToList()),
                Sender = record.Sender,
                Modulation = record.Modulation
            };
            return output;
        }

        private TableJamDirect ParseTableJamDirect(TableJamDirect record) 
        {
            var output = new TableJamDirect()
            {
                ID = record.ID,
                JamDirect = new JamDirect()
            };

            if (record.JamDirect != null)
            {
                output.JamDirect.IsOwn = record.JamDirect.IsOwn;
                output.JamDirect.Bearing = record.JamDirect.Bearing;
                output.JamDirect.DistanceKM = record.JamDirect.DistanceKM;
                output.JamDirect.NumberASP = record.JamDirect.NumberASP;
                output.JamDirect.Std = record.JamDirect.Std;
                output.JamDirect.Level = record.JamDirect.Level;
            }
            else
            {
                output.JamDirect.IsOwn = false;
                output.JamDirect.Bearing = -1;
                output.JamDirect.DistanceKM = -1;
                output.JamDirect.NumberASP = ConnectedStationId;
                output.JamDirect.Std = -1;
                output.JamDirect.Level = -1;
            }
            return output;
        }
        
        private void TransmitterOnQuasiDfRequest(object sender, IQuasiRequest request)
        {
            var gap = Constants.RequestGapKhz;
            OnReceive?.Invoke(this, $"Received quasi df request from {ConnectedStationId}");
            OnSendExecutiveDfRequest?.Invoke(this, new ExecutiveDfRequestArgs(ConnectedStationId, 
                startFrequencyKhz: request.Frequency - gap, 
                endFrequencyKhz: request.Frequency + gap,
                phaseAveragingCount: Constants.PhaseAveragingCount,
                directionAveragingCount: Constants.DirectionAveragingCount));
        }

        private void Transmitter_OnFhssJam(object sender, List<IFixSupprFHSS> table)
        {
            OnReceive?.Invoke(this, $"Received fhss jam from {ConnectedStationId}");
            var numberAsp = table[0].Id;
            table.RemoveAt(0);
            TableFhssJamReceived?.Invoke(this, new TableEventArgs<TableSuppressFHSS>(ConnectedStationId,
                table.Select(r => new TableSuppressFHSS()
                {
                    NumberASP = numberAsp,
                    EPO = r.EPO,
                    FreqMaxKHz = r.FreqMaxKHz,
                    FreqMinKHz = r.FreqMinKHz,
                    InterferenceParam = r.InterferenceParam,
                    Letters = r.Letters,
                    StepKHz = r.StepKHz,
                    Threshold = r.Threshold
                }).ToArray()));
        }

        private void Transmitter_OnSectorsRangesSuppr(object sender, List<IFixSectorRanges> table)
        {
            OnReceive?.Invoke(this, $"Received sectors ranges suppr from {ConnectedStationId}");
            var numberAsp = table[0].Id;
            table.RemoveAt(0);
            TableSectorsRangesSupprReceived?.Invoke(this, new TableEventArgs<TableSectorsRangesSuppr>(ConnectedStationId,
                table.Select(r => new TableSectorsRangesSuppr()
                {
                    NumberASP = numberAsp,
                    AngleMin = r.AngleMin,
                    AngleMax = r.AngleMax,
                    FreqMinKHz = r.FreqMinKHz,
                    FreqMaxKHz = r.FreqMaxKHz,
                    IsCheck = true
                }).ToArray()));
        }

        private void Transmitter_OnFrsJam(object sender, List<IFixSupprFWS> table)
        {
            OnReceive?.Invoke(this, $"Received frs jam from {ConnectedStationId}");
            var numberAsp = table[0].Id;
            table.RemoveAt(0);
            TableFrsJamReceived?.Invoke(this, new TableEventArgs<TableSuppressFWS>(ConnectedStationId,
                table.Select(r => new TableSuppressFWS()
                {
                    NumberASP = numberAsp,
                    Bearing = r.Bearing,
                    Coordinates = r.Coordinates,
                    FreqKHz = r.FreqKHz,
                    InterferenceParam = r.InterferenceParam,
                    Priority = r.Priority,
                    Threshold = r.Threshold,
                    Letter = r.Letter
                }).ToArray()));
        }

        private void Transmitter_OnForbidden(object sender, List<IFixSpecFreq> table)
        {
            OnReceive?.Invoke(this, $"Received forbidden from {ConnectedStationId}");
            var numberAsp = table[0].Id;
            table.RemoveAt(0);
            TableFreqForbiddenReceived?.Invoke(this, new TableEventArgs<TableFreqForbidden>(ConnectedStationId,
                table.Select(r => new TableFreqForbidden()
                {
                    NumberASP = numberAsp,
                    FreqMinKHz = r.FreqMinKHz,
                    FreqMaxKHz = r.FreqMaxKHz
                }).ToArray()));
        }

        private void Transmitter_OnKnown(object sender, List<IFixSpecFreq> table)
        {
            OnReceive?.Invoke(this, $"Received known from {ConnectedStationId}");
            var numberAsp = table[0].Id;
            table.RemoveAt(0);
            TableFreqKnownReceived?.Invoke(this, new TableEventArgs<TableFreqKnown>(ConnectedStationId,
                table.Select(r => new TableFreqKnown()
                {
                    NumberASP = numberAsp,
                    FreqMinKHz = r.FreqMinKHz,
                    FreqMaxKHz = r.FreqMaxKHz
                }).ToArray()));
        }

        private void Transmitter_OnImportant(object sender, List<IFixSpecFreq> table)
        {
            OnReceive?.Invoke(this, $"Received important from {ConnectedStationId}");
            var numberAsp = table[0].Id;
            table.RemoveAt(0);
            TableFreqImportantReceived?.Invoke(this, new TableEventArgs<TableFreqImportant>(ConnectedStationId,
                table.Select(r => new TableFreqImportant()
                {
                    NumberASP = numberAsp,
                    FreqMinKHz = r.FreqMinKHz,
                    FreqMaxKHz = r.FreqMaxKHz
                }).ToArray()));
        }

        private void TransmitterOnSectorRangesRecon(object sender, List<IFixSectorRanges> table)
        {
            OnReceive?.Invoke(this, $"Received sectors ranges suppr from {ConnectedStationId}");
            var numberAsp = table[0].Id;
            table.RemoveAt(0);
            TableSectorsRangesReconReceived?.Invoke(this, new TableEventArgs<TableSectorsRangesRecon>(ConnectedStationId, 
                table.Select(r => new TableSectorsRangesRecon()
                {
                    NumberASP = numberAsp,
                    AngleMin = r.AngleMin, 
                    AngleMax = r.AngleMax, 
                    FreqMinKHz = r.FreqMinKHz,
                    FreqMaxKHz = r.FreqMaxKHz,
                    IsCheck = true
                }).ToArray()));
        }

        private void TableAsp_Request(object sender, List<IFixASP> requestTable)
        {
            OnReceive?.Invoke(this, $"Received asp table from {ConnectedStationId}");
            var table = requestTable.FirstOrDefault();
            var compassesValues = new CompassesValues(table.LPA13, table.LPA24, table.LPA510, table.RRS1, table.RRS2, table.BPSS, table.LPA57, table.LPA59, table.LPA10);
            AspTableReceived?.Invoke(this, new AspTable(table.Mode, table.Id, table.Coordinates, compassesValues));
        }

        private void TextMess_Request(object sender, string text)
        {
            OnReceive?.Invoke(this, $"Received text message from {ConnectedStationId}");
            OnSendMessageRequest?.Invoke(this, new Message(ConnectedStationId, OwnStationId, text));
        }

        //We can't force connect via com port
        public void ForceConnection(string host, int port)
        {}

        public void Initialize()
        {
            //device.Connect(ServerAddress );
            device.Connect(ServerAddress, ComPortRate);
            SubscribeToDeviceEvents();
        }

        public bool Ping(int ownStationId, int ownPort)
        {
            return true;
            //OnSend?.Invoke(this, $"Pinging station {ConnectedStationId}");
            //_manualResetEvent.Reset();
            //device.DictCommands[Requests.Ping].SendRequest((byte)OwnStationId, (byte)ConnectedStationId, (byte)ownPort);
            //var result = _manualResetEvent.WaitOne(Constants.DeadlineMs);
            //IsConnected = result;
            //OnSend?.Invoke(this, $"Ping result : {result}");
            //return result;
        }

        private void OnPingConfirm(object sender, ModelConfirm e)
        {
            //_manualResetEvent.Set();
        }

        private void OnPingRequest(object sender, byte e)
        {
            device.DictCommands[Requests.Ping].SendConfirm((byte)OwnStationId, (byte)ConnectedStationId, new ModelConfirm() { CodeError = 0 });
        }

        public bool SendAspTable(AspTable table)
        {
            var model = new List<IFixASP>()
                {
                    new TableASP
                        {
                            Id = table.StationId,
                            ISOwn = true, //TODO: поч true?
                            Mode = table.Mode,
                            Coordinates = table.Coordinates,
                            LPA13 = table.Compasses.Lpa13,
                            LPA24 = table.Compasses.Lpa24,
                            LPA510 = table.Compasses.Lpa510,
                            RRS1 = table.Compasses.Rrs1,
                            RRS2 = table.Compasses.Rrs2,
                            BPSS = table.Compasses.BPSS,
                            LPA57 = table.Compasses.LPA57,
                            LPA59 = table.Compasses.LPA59,
                            LPA10 = table.Compasses.LPA10
                        }
                    };
            device.DictCommands[Requests.TableAsp].SendRequest((byte)OwnStationId, (byte)ConnectedStationId, model);
            OnSend?.Invoke(this, $"Sending asp table to {ConnectedStationId}");
            return true;
        }

        public bool SendExecutiveDfRequest(float startFrequencyKhz, float endFrequencyKhz, int phaseAveragingCount, int directionAveragingCount)
        {
            OnSend?.Invoke(this, $"Sending executive df request to {ConnectedStationId}");
            device.DictCommands[Requests.QuasiDirectFind].SendRequest((byte)OwnStationId, (byte)ConnectedStationId, 
                model: new QuasiReq()
                {
                    Frequency = (int)(endFrequencyKhz + startFrequencyKhz) / 2
                });
            return true;
        }

        public bool SendExecutiveDfResponse(float result, int stationId)
        {
            OnSend?.Invoke(this, $"Sending executive df response to {ConnectedStationId}");
            device.DictCommands[Requests.QuasiDirectFind].SendConfirm((byte)OwnStationId, (byte)ConnectedStationId, 
                CodeError: new ModelConfirm<IQuasiConfirm>()
                {
                    Model = new QuasiConf() { Bearing = (short)result, StationId = stationId }
                });
            return true;
        }

        public bool SendLocalTime(DateTime localTime)
        {
            //todo: add, not existing at all right now
            return true;
        }

        public bool SendMasterRdfResult(RdfCycleResult result)
        {
            return true;
            //todo: no implementations, so yeah
        }

        public bool SendSlaveRdfResult(RdfCycleResult result)
        {
            return true;
            //todo: no implementations, so yeah
        }

        public bool SendMessage(Message message)
        {
            OnSend?.Invoke(this, $"Sending text message to {ConnectedStationId}");
            message.UpdateSenderId(OwnStationId);
            device.DictCommands[Requests.TextMess].SendRequest((byte)message.SenderId, (byte)message.ReceiverId, message.Text);
            return true;
        }

        public bool SendMode(byte mode)
        {
            OnSend?.Invoke(this, $"Sending mode to {ConnectedStationId}");
            device.DictCommands[Requests.Regime].SendRequest((byte)OwnStationId, (byte)ConnectedStationId, mode);
            return true;
        }

        public bool SendTableFhssExcludedFreq(TableEventArgs<TableFHSSExcludedFreq> args)
        {
            //todo: no implementations, so yeah
            return true;
        }

        public bool SendTableFhssJam(TableEventArgs<TableSuppressFHSS> args)
        {
            OnSend?.Invoke(this, $"Sending fhss jam to {ConnectedStationId}");
            var informationRecord = new TableSuppressFHSS()
            {
                Id = IsMaster ? ConnectedStationId : OwnStationId
            };
            var updatedTable = new List<IFixSupprFHSS>(args.Table);
            updatedTable.Insert(0, informationRecord);
            device.DictCommands[Requests.TableSuppressFHSS].SendRequest((byte)OwnStationId, (byte)ConnectedStationId, updatedTable);
            return true;
        }

        public bool SendTableFhssRecon(TableEventArgs<TableReconFHSS> args)
        {
            //todo: no implementations, so yeah
            return true;
        }

        public bool SendTableFreqForbidden(TableEventArgs<TableFreqForbidden> args)
        {
            OnSend?.Invoke(this, $"Sending forbidden to {ConnectedStationId}");
            var informationRecord = new TableFreqForbidden()
            {
                Id = IsMaster ? ConnectedStationId : OwnStationId
            };
            var updatedTable = new List<IFixSpecFreq>(args.Table);
            updatedTable.Insert(0, informationRecord);
            device.DictCommands[Requests.TableFreqForbidden].SendRequest((byte)OwnStationId, (byte)ConnectedStationId, updatedTable);
            return true;
        }

        public bool SendTableFreqImportant(TableEventArgs<TableFreqImportant> args)
        {
            OnSend?.Invoke(this, $"Sending important to {ConnectedStationId}");
            var informationRecord = new TableFreqForbidden()
            {
                Id = IsMaster ? ConnectedStationId : OwnStationId
            };
            var updatedTable = new List<IFixSpecFreq>(args.Table);
            updatedTable.Insert(0, informationRecord);
            device.DictCommands[Requests.TableFreqImportant].SendRequest((byte)OwnStationId, (byte)ConnectedStationId, updatedTable);
            return true;
        }

        public bool SendTableFreqKnown(TableEventArgs<TableFreqKnown> args)
        {
            OnSend?.Invoke(this, $"Sending known to {ConnectedStationId}");
            var informationRecord = new TableFreqForbidden()
            {
                Id = IsMaster ? ConnectedStationId : OwnStationId
            };
            var updatedTable = new List<IFixSpecFreq>(args.Table);
            updatedTable.Insert(0, informationRecord);
            device.DictCommands[Requests.TableFreqKnown].SendRequest((byte)OwnStationId, (byte)ConnectedStationId, updatedTable);
            return true;
        }

        public bool SendTableFrsJam(TableEventArgs<TableSuppressFWS> args)
        {
            OnSend?.Invoke(this, $"Sending frs jam to {ConnectedStationId}");
            var informationRecord = new TableSuppressFWS()
            {
                Id = IsMaster ? ConnectedStationId : OwnStationId
            };
            var updatedTable = new List<IFixSupprFWS>(args.Table);
            updatedTable.Insert(0, informationRecord);
            device.DictCommands[Requests.TableSuppressFWS].SendRequest((byte)OwnStationId, (byte)ConnectedStationId, updatedTable);
            return true;
        }

        public bool SendTableFrsRecon(TableEventArgs<TableReconFWS> args)
        {
            OnSend?.Invoke(this, $"Sending frs recon to {ConnectedStationId}");
            device.DictCommands[Requests.TableReconFWS].SendRequest((byte)OwnStationId, (byte)ConnectedStationId, new List<IFixReconFWS>(args.Table));
            return true;
        }

        public bool SendTableSectorsRangesRecon(TableEventArgs<TableSectorsRangesRecon> args)
        {
            OnSend?.Invoke(this, $"Sending sectors ranges recon to {ConnectedStationId}");
            var informationRecord = new TableSectorsRangesRecon()
            {
                Id = IsMaster ? ConnectedStationId : OwnStationId
            };
            var updatedTable = new List<IFixSectorRanges>(args.Table);
            updatedTable.Insert(0, informationRecord);
            device.DictCommands[Requests.TableSectorRangeRecon].SendRequest((byte)OwnStationId, (byte)ConnectedStationId, updatedTable);
            return true;
        }

        public bool SendTableSectorsRangesSuppr(TableEventArgs<TableSectorsRangesSuppr> args)
        {
            OnSend?.Invoke(this, $"Sending sectors ranges suppress to {ConnectedStationId}");
            var informationRecord = new TableSectorsRangesRecon()
            {
                Id = IsMaster ? ConnectedStationId : OwnStationId
            };
            var updatedTable = new List<IFixSectorRanges>(args.Table);
            updatedTable.Insert(0, informationRecord);
            device.DictCommands[Requests.TableSectorRangeSuppr].SendRequest((byte)OwnStationId, (byte)ConnectedStationId, updatedTable);
            return true;
        }

        public bool SetBandNumber(int bandNumber)
        {
            //todo: no implementation, so yeah
            return true;
        }

        public void ShutDown()
        {
            device.Disconnect();
        }

        public void AbortConnection(bool isMaster)
        {
            //todo : ?
        }

        public bool SendFhssCycleResult(FhssCycleResult cycleResult)
        {
            return true;
            //todo : not available, add?
        }
    }
}
