﻿using DataTransferModel.Trasmission;

namespace TransmissionLibrary.ComTransmission
{
    public class ComTransmitterFactory : ITransmitterFactory
    {
        private bool _isComPortUsed = false;
        private string _comPortString = "COM99";
        private int _comPortRate = 9600;

        public ComTransmitterFactory(string comPortString) 
        {
            Initialize(comPortString);
        }

        public ComTransmitterFactory(string comPortString, int comPortRate)
        {
            Initialize(comPortString, comPortRate);
        }

        public void Initialize(string comPortString)
        {
            _comPortString = comPortString;
        }

        public void Initialize(string comPortString, int comPortRate)
        {
            _comPortString = comPortString;
            _comPortRate = comPortRate;
        }

        public ITransmitter CreateTransmitter(int ownStationId, int connectedStationId, bool isMaster) 
        {
            if (_isComPortUsed)
                return new DummyTransmitter(); //we cannot have two transmitters using the same comport
            //var transmitter = new ComTransmitter(ownStationId, connectedStationId, _comPortString, isMaster);
            var transmitter = new ComTransmitter(ownStationId, connectedStationId, _comPortString, _comPortRate, isMaster);
            transmitter.Initialize();
            _isComPortUsed = true;
            return transmitter;
        }



        public ITransmitter CreateTransmitter(int ownStationId, int connectedStationId, string linkedStationHost, int linkedStationPort, bool isMaster) =>
            CreateTransmitter(ownStationId, connectedStationId, isMaster);
    }
}
