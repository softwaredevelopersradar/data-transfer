﻿using DataTransferModel.Database;
using DataTransferModel.Trasmission;

namespace TransmissionLibrary.SlowUdpTcpTransmission
{
    public class SlowTcpTransmitterFactory
    {
        private readonly string _localIp;
        private readonly int _localPort;
        private const int UdpPort = 8506; // fixed!

        public SlowTcpTransmitterFactory(string localIp, int localPort)
        {
            _localIp = localIp;
            _localPort = localPort;
        }
        
        public ITransmitter CreateTransmitter(bool isMaster, int ownId, Station linked)
        {
            var localUdpPort = isMaster ? UdpPort : UdpPort + 1;
            var remoteUdpPort = isMaster ? UdpPort + 1: UdpPort;
            return new SlowTcpUdpTransmitter(isMaster, ownId, linked.Id,
                _localIp, _localPort, localUdpPort,
                linked.IpAddress, linked.Port, remoteUdpPort);
        }
    }
}