﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace TransmissionLibrary.SlowUdpTcpTransmission
{
    public class Transmitter
    {
        private const int ReadWriteTimeout = 1000;
        private const int ConnectionTaskCheckDelayMs = 1000;
        private const int ReadTaskCheckDelayMs = 100;
        private const int MaxNumberOfReconnectionAttempts = 200;

        private readonly byte[] _buffer = new byte[4096];
        private readonly IPEndPoint _endPoint;
        private string _remoteIp;
        private int _remoteTcpPort;

        private TcpClient _client;
        private int _numberOfReconnectionAttempts = 0;

        private readonly SlavePingManager _slavePingManager;
        private readonly MasterPingManager _masterPingManager;

        private bool _connectTaskFlag;
        private bool _readTaskFlag;

        public event EventHandler<byte[]> MessageReceived;
        public event EventHandler<bool> ConnectionStateChanged;

        public event EventHandler<string> OnLog;
        
        public bool IsConnected => _client != null && _client.Connected;

        public Transmitter(string localIp, int localTcpPort, int localUdpPort, int remoteUdpPort)
        {
            _endPoint = new IPEndPoint(IPAddress.Parse(localIp), localTcpPort);
            _slavePingManager = new SlavePingManager(localIp, localUdpPort, remoteUdpPort);
            _slavePingManager.ReadyToEstablishTcpConnection += _slavePingManager_ReadyToEstablishTcpConnection;
            _slavePingManager.UdpConnectionLost += OnUdpConnectionLost;
            _slavePingManager.OnLog += (sender, s) => Log(s);
        }

        private void OnUdpConnectionLost(object sender, EventArgs e)
        {
            _connectTaskFlag = false;
            _readTaskFlag = false;
            DisposeTcpClient();
            ConnectionStateChanged?.Invoke(this, false);
        }

        private void _slavePingManager_ReadyToEstablishTcpConnection(object sender, byte[] e)
        {
            var ping = new PingMessage(e);
            this._remoteIp = ping.IpAddress;
            this._remoteTcpPort = ping.Port;
            _connectTaskFlag = true;
            CreateTcpClient();
            Task.Run(ConnectTask);
        }

        public Transmitter(string localIp, int localTcpPort, int localUdpPort, string remoteIp, int remoteTcpPort, int remoteUdpPort)
        {
            this._remoteIp = remoteIp;
            this._remoteTcpPort = remoteTcpPort;
            _endPoint = new IPEndPoint(IPAddress.Parse(localIp), localTcpPort);
            _masterPingManager = new MasterPingManager(localIp, localUdpPort, localTcpPort, remoteIp, remoteUdpPort);
            
            _masterPingManager.ReadyToEstablishTcpConnection += MasterPingManagerOnReadyToEstablishTcpConnection;
            _masterPingManager.UdpConnectionLost += OnUdpConnectionLost;
            _masterPingManager.OnLog += (sender, s) => Log(s);
        }

        private void MasterPingManagerOnReadyToEstablishTcpConnection(object sender, EventArgs e)
        {
            _connectTaskFlag = true;
            CreateTcpClient();
            Task.Run(ConnectTask);
        }

        public void Send(byte[] message)
        {
            if (!_client.Connected)
            {
                Log("Client is not connected, aborting sending data");
            }

            var stream = _client.GetStream();
            stream.Write(message, 0, message.Length);
        }

        private void CreateTcpClient() 
        {
            _client = new TcpClient(_endPoint)
            {
                ReceiveTimeout = ReadWriteTimeout,
                SendTimeout = ReadWriteTimeout
            };
            Log($"Created tcp client {_endPoint}");
        }

        private void DisposeTcpClient()
        {
            _client.Close();
            _client = null;
            GC.Collect();
        }

        private void Connect()
        {
            _numberOfReconnectionAttempts++;
            _client.Connect(_remoteIp, _remoteTcpPort);
            _numberOfReconnectionAttempts = 0;
            _readTaskFlag = true;
            ConnectionStateChanged?.Invoke(this, true);
            Task.Run(ReadTask);
        }

        private async Task ConnectTask()
        {
            while (_connectTaskFlag)
            {
                try
                {
                    if (_client.Connected)
                    {
                        await Task.Delay(ConnectionTaskCheckDelayMs);
                        continue;
                    }

                    if (_numberOfReconnectionAttempts == MaxNumberOfReconnectionAttempts)
                    {
                        Log($"Reconnection timeout reached : {MaxNumberOfReconnectionAttempts} mins\r\n" +
                            $"The other side is not responding or something is wrong in the configuration");
                        break;
                    }

                    Log($"Trying to connect to {_remoteIp}:{_remoteTcpPort}, attempts left {MaxNumberOfReconnectionAttempts - _numberOfReconnectionAttempts}");
                    Connect();
                    Log($"Connected  to {_remoteIp}:{_remoteTcpPort}");
                }
                catch
                {
                    await Task.Delay(ConnectionTaskCheckDelayMs);
                    Log($"Failed to connect to {_remoteIp}:{_remoteTcpPort}");
                }
            }
        }

        private async Task ReadTask() 
        {
            while (_readTaskFlag) 
            {
                try 
                {
                    if (!_client.Connected)
                    {
                        await Task.Delay(ReadTaskCheckDelayMs);
                        continue;
                    }
                    var stream = _client.GetStream();
                    if (!stream.DataAvailable)
                    {
                        stream.Read(_buffer, 0, 0); // ping read
                        continue;
                    }
                    stream.Read(_buffer, 0, _buffer.Length);
                    var copy = new byte[_buffer.Length];
                    _buffer.CopyTo(copy, 0);
                    MessageReceived?.Invoke(this, copy);
                    await Task.Delay(1); // consecutive reads are allowed to be fast
                }
                catch
                {
                    //Log($"Failed to read data as it was probably unavailable");
                }
            }
        }

        private void Log(string message)
        {
            OnLog?.Invoke(this, message);
        }
    }
}