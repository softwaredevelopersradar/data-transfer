﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace TransmissionLibrary.SlowUdpTcpTransmission
{
    public class SlavePingManager
    {
        private readonly UdpClient _listener;
        private IPEndPoint _remoteReceive;

        private IPEndPoint _remoteSend;
        private bool _isWorking = false;

        public event EventHandler<byte[]> ReadyToEstablishTcpConnection;
        public event EventHandler UdpConnectionLost;

        private bool _connectionStable = false;

        public SlavePingManager(string localIp, int localUdpPort, int remoteUdpPort)
        {
            _listener = new UdpClient(new IPEndPoint(IPAddress.Parse(localIp), localUdpPort));
            _listener.Client.ReceiveTimeout = 2000;
            _listener.Client.SendTimeout = 2000;
            _remoteReceive = new IPEndPoint(IPAddress.Any, remoteUdpPort);

            _isWorking = true;
            Task.Run(ListeningTask);
        }

        public void Stop() 
        {
            _isWorking = false;
        }

        private const int _maxPingToFail = 5;
        private int _amountOfFailedPingsToFail; 

        private async Task ListeningTask() 
        {
            while (_isWorking)
            {
                try
                {
                    Log("Waiting for ping request");
                    var data = _listener.Receive(ref _remoteReceive);
                    if (data[0] != 99)
                    {
                        throw new Exception();
                    }

                    _amountOfFailedPingsToFail = _maxPingToFail;

                    Log("Received ping request");
                    if (!_connectionStable)
                    {
                        // ping request received, tcp connection can be made
                        var ping = new PingMessage(data);
                        _remoteSend = new IPEndPoint(IPAddress.Parse(ping.IpAddress), _remoteReceive.Port);
                        ReadyToEstablishTcpConnection?.Invoke(this, data);
                        _connectionStable = true;
                    }

                    if (_remoteSend != null)
                    {
                        _listener.Send(new byte[] { 138 }, 1, _remoteSend);
                    }
                }
                catch
                {
                    Log("Ping receiving failed");
                    _amountOfFailedPingsToFail--;
                    _remoteSend = null;
                    if (_connectionStable && _amountOfFailedPingsToFail == 0)
                    {
                        UdpConnectionLost?.Invoke(this, EventArgs.Empty);
                    }

                    _connectionStable = false;
                    await Task.Delay(1);
                }
            }
        }
        
        public event EventHandler<string> OnLog;

        private void Log(string message)
        {
            OnLog?.Invoke(this, message);
        }
    }
}