﻿namespace TransmissionLibrary.SlowUdpTcpTransmission
{
    public enum Commands : byte
    {
        Ping = 99,
        Message = 101,
        Mode = 102,
        ExecutiveDfRequest = 103,
        ExecutiveDfResponse = 104,
        AspTable = 105,
        TableSectorsRangesRecon = 106,
        TableSectorsRangesSuppr = 107,
        TableFrsRecon = 108,
        TableFrsJam = 109,
        TableFhssJam = 110,
        TableFreqImportant = 111,
        TableFreqKnown = 112,
        TableFreqForbidden = 113
    }
}