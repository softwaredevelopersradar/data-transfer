﻿using System;
using Google.Protobuf;

namespace TransmissionLibrary.SlowUdpTcpTransmission
{
    public static class Encoder
    {
        public static void Send(this Transmitter transmitter, IMessage message, Commands command)
        {
            var bytes = message.Convert(command);
            transmitter.Send(bytes);
        }

        private static byte[] Convert(this IMessage message, Commands command)
        {
            var array = message.ToByteArray();
            var output = new byte[array.Length + 5];
            Array.Copy(array, 0, output, 5, array.Length);
            output[0] = (byte)command;
            var length = BitConverter.GetBytes(array.Length);
            Array.Copy(length, 0, output, 1, length.Length);
            return output;
        }
    }
}