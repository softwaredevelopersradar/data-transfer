﻿using System;
using System.Text;

namespace TransmissionLibrary.SlowUdpTcpTransmission
{
    public struct PingMessage
    {
        public string IpAddress { get; set; }
        public int Port { get; set; }

        public byte[] GetBytes() 
        {
            var stringBytes = ASCIIEncoding.ASCII.GetBytes(IpAddress);
            var portByte = BitConverter.GetBytes(Port);

            var message = new byte[1 + 4 + stringBytes.Length]; // 1 - ping cmd code, 4 - port, ip string itself
            message[0] = 138;
            Array.Copy(portByte, 0, message, 1, portByte.Length);
            Array.Copy(stringBytes, 0, message, 5, stringBytes.Length);
            return message;
        }

        public PingMessage(byte[] data) 
        {
            Port = BitConverter.ToInt32(data, 1);
            IpAddress = ASCIIEncoding.ASCII.GetString(data, 5, data.Length - 5);
        }
    }
}
