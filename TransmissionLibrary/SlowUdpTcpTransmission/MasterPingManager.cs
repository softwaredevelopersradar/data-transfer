﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace TransmissionLibrary.SlowUdpTcpTransmission
{
    public class MasterPingManager
    {
        private readonly UdpClient _sender;
        private IPEndPoint _remote;
        private bool _isWorking;
        private readonly byte[] _pingMessage;
        
        public event EventHandler ReadyToEstablishTcpConnection;
        public event EventHandler UdpConnectionLost;

        private bool _connectionStable;

        public MasterPingManager(string localIp, int localUdpPort, int localTcpPort, string remoteIp, int remoteUdpPort)
        {
            _sender = new UdpClient(new IPEndPoint(IPAddress.Parse(localIp), localUdpPort));

            _sender.Client.ReceiveTimeout = 2000;
            _sender.Client.SendTimeout = 2000;

            _remote = new IPEndPoint(IPAddress.Parse(remoteIp), remoteUdpPort);
            _sender.Connect(_remote);

            _isWorking = true;
            var pingMessage = new PingMessage() { IpAddress = localIp, Port = localTcpPort };
            _pingMessage = pingMessage.GetBytes();

            Task.Run(PingTask);
        }

        public void Stop() 
        {
            _isWorking = false;
        }
        
        private const int _maxPingToFail = 5;
        private int _amountOfFailedPingsToFail; 

        private async Task PingTask()
        {
            while (_isWorking)
            {
                try
                {
                    Log("Sending ping request");
                    _sender.Send(_pingMessage, _pingMessage.Length);
                    var response = _sender.Receive(ref _remote);
                    if (response[0] == 138)
                    {
                        if (!_connectionStable)
                        {
                            // ping request received, tcp connection can be made
                            ReadyToEstablishTcpConnection?.Invoke(this, EventArgs.Empty);
                            _connectionStable = true;
                        }
                        Log("Received ping response");
                        _amountOfFailedPingsToFail = _maxPingToFail;
                    }
                    else
                    {
                        throw new Exception();
                    }
                }
                catch
                {
                    Log("Ping request failed");
                    _amountOfFailedPingsToFail--;
                    if (_connectionStable && _amountOfFailedPingsToFail == 0)
                    {
                        UdpConnectionLost?.Invoke(this, EventArgs.Empty);
                        _connectionStable = false;
                    }
                }

                await Task.Delay(1000);
            }
        }
        
        public event EventHandler<string> OnLog;

        private void Log(string message)
        {
            OnLog?.Invoke(this, message);
        }
    }
}