﻿using System;
using DataTransferModel.Trasmission;
using TransmissionPackage;
using AspTable = DataTransferModel.DataTransfer.AspTable;
using DateTime = System.DateTime;
using ExecutiveDfRequestArgs = DataTransferModel.DataTransfer.ExecutiveDfRequestArgs;
using ExecutiveDfResponseArgs = DataTransferModel.DataTransfer.ExecutiveDfResponseArgs;
using FhssCycleResult = DataTransferModel.DataTransfer.FhssCycleResult;
using Message = DataTransferModel.DataTransfer.Message;
using RdfCycleResult = DataTransferModel.DataTransfer.RdfCycleResult;
using TableFHSSExcludedFreq = ModelsTablesDBLib.TableFHSSExcludedFreq;
using TableFreqForbidden = ModelsTablesDBLib.TableFreqForbidden;
using TableFreqImportant = ModelsTablesDBLib.TableFreqImportant;
using TableFreqKnown = ModelsTablesDBLib.TableFreqKnown;
using TableReconFHSS = ModelsTablesDBLib.TableReconFHSS;
using TableReconFWS = ModelsTablesDBLib.TableReconFWS;
using TableSectorsRangesRecon = ModelsTablesDBLib.TableSectorsRangesRecon;
using TableSectorsRangesSuppr = ModelsTablesDBLib.TableSectorsRangesSuppr;
using TableSuppressFHSS = ModelsTablesDBLib.TableSuppressFHSS;
using TableSuppressFWS = ModelsTablesDBLib.TableSuppressFWS;

namespace TransmissionLibrary.SlowUdpTcpTransmission
{
    public class SlowTcpUdpTransmitter : ITransmitter
    {
        public bool Ping(int ownStationId, int ownPort)
        {
            // is implemented internally in master/slave ping managers via udp
            // thus it is removed here, even though it's not pretty
            return true;
        }

        public bool SendMessage(Message message)
        {
            _transmitter.Send(message.ConvertToProtoType(), Commands.Message);
            return true;
        }

        public bool SendMode(byte mode)
        {
            var modeRequest = new ModeRequest() {Mode = mode};
            _transmitter.Send(modeRequest, Commands.Mode);
            return true;
        }

        public bool SendLocalTime(DateTime localTime)
        {
            // not supported
            return true;
        }

        public bool SendExecutiveDfRequest(float startFrequencyKhz, float endFrequencyKhz, int phaseAveragingCount,
            int directionAveragingCount)
        {
            var request = new TransmissionPackage.ExecutiveDfRequestArgs()
            {
                StationId = -1, //todo : remove from proto?
                StartFrequencyKhz = startFrequencyKhz,
                EndFrequencyKhz = endFrequencyKhz,
                PhaseAveragingCount = phaseAveragingCount,
                DirectionAveragingCount = directionAveragingCount
            };
            _transmitter.Send(request, Commands.ExecutiveDfRequest);
            return true;
        }

        public bool SendExecutiveDfResponse(float result, int stationId)
        {
            var response = new TransmissionPackage.ExecutiveDfResponseArgs()
                {Direction = result, FrequencyKhz = -1, StationId = stationId};
            _transmitter.Send(response, Commands.ExecutiveDfResponse);
            return true;
        }

        public bool SendSlaveRdfResult(RdfCycleResult result)
        {
            // not supported
            return true;
        }

        public bool SendMasterRdfResult(RdfCycleResult result)
        {
            // not supported
            return true;
        }

        public bool SendFhssCycleResult(FhssCycleResult cycleResult)
        {
            // not supported
            return true;
        }

        public bool SetBandNumber(int bandNumber)
        {
            // not supported
            return true;
        }

        public bool SendAspTable(AspTable table)
        {
            _transmitter.Send(table.ConvertToProtoType(), Commands.AspTable);
            return true;
        }

        public bool SendTableSectorsRangesRecon(TableEventArgs<TableSectorsRangesRecon> table)
        {
            _transmitter.Send(table.ConvertToProtoType(), Commands.TableSectorsRangesRecon);
            return true;
        }

        public bool SendTableSectorsRangesSuppr(TableEventArgs<TableSectorsRangesSuppr> table)
        {
            _transmitter.Send(table.ConvertToProtoType(), Commands.TableSectorsRangesSuppr);
            return true;
        }

        public bool SendTableFrsRecon(TableEventArgs<TableReconFWS> table)
        {
            _transmitter.Send(table.ConvertToProtoType(), Commands.TableFrsRecon);
            return true;
        }

        public bool SendTableFhssRecon(TableEventArgs<TableReconFHSS> table)
        {
            // not supported
            return true;
        }

        public bool SendTableFrsJam(TableEventArgs<TableSuppressFWS> table)
        {
            _transmitter.Send(table.ConvertToProtoType(), Commands.TableFrsJam);
            return true;
        }

        public bool SendTableFhssJam(TableEventArgs<TableSuppressFHSS> table)
        {
            _transmitter.Send(table.ConvertToProtoType(), Commands.TableFhssJam);
            return true;
        }

        public bool SendTableFhssExcludedFreq(TableEventArgs<TableFHSSExcludedFreq> table)
        {
            // not supported
            return true;
        }

        public bool SendTableFreqImportant(TableEventArgs<TableFreqImportant> table)
        {
            _transmitter.Send(table.ConvertToProtoType(), Commands.TableFreqImportant);
            return true;
        }

        public bool SendTableFreqKnown(TableEventArgs<TableFreqKnown> table)
        {
            _transmitter.Send(table.ConvertToProtoType(), Commands.TableFreqKnown);
            return true;
        }

        public bool SendTableFreqForbidden(TableEventArgs<TableFreqForbidden> table)
        {
            _transmitter.Send(table.ConvertToProtoType(), Commands.TableFreqForbidden);
            return true;
        }

        public void Initialize()
        {
            // done manually in transmitter
        }

        public void ShutDown()
        {
            // done manually in transmitter
        }

        public bool IsConnected => _transmitter.IsConnected;
        public string ClientAddress { get; }
        public int ClientPort { get; }
        public void ForceConnection(string host, int port)
        {
            // done manually in transmitter
        }

        public void AbortConnection(bool isMaster)
        {
            // done manually in transmitter
        }

        public event EventHandler<bool> ConnectionStateChanged;
        public event EventHandler<PeerInfo> OnForceConnectionRequest;
        public event EventHandler<Message> OnSendMessageRequest;
        public event EventHandler<ExecutiveDfRequestArgs> OnSendExecutiveDfRequest;
        public event EventHandler<ExecutiveDfResponseArgs> OnSendExecutiveDfResponse;
        public event EventHandler<byte> OnSendModeRequest;
        public event EventHandler<int> OnSetBandReceived;
        public event EventHandler<DateTime> OnLocalTimeReceived;
        public event EventHandler<RdfCycleResult> OnMasterRdfResultReceived;
        public event EventHandler<RdfCycleResult> OnSlaveRdfResultReceived;
        public event EventHandler<FhssCycleResult> OnFhssCycleResultReceived;
        public event EventHandler<AspTable> AspTableReceived;
        public event EventHandler<TableEventArgs<TableSectorsRangesRecon>> TableSectorsRangesReconReceived;
        public event EventHandler<TableEventArgs<TableSectorsRangesSuppr>> TableSectorsRangesSupprReceived;
        public event EventHandler<TableEventArgs<TableReconFWS>> TableFrsReconReceived;
        public event EventHandler<TableEventArgs<TableReconFHSS>> TableFhssReconReceived;
        public event EventHandler<TableEventArgs<TableSuppressFWS>> TableFrsJamReceived;
        public event EventHandler<TableEventArgs<TableSuppressFHSS>> TableFhssJamReceived;
        public event EventHandler<TableEventArgs<TableFHSSExcludedFreq>> TableFhssExcludedFreqReceived;
        public event EventHandler<TableEventArgs<TableFreqImportant>> TableFreqImportantReceived;
        public event EventHandler<TableEventArgs<TableFreqForbidden>> TableFreqForbiddenReceived;
        public event EventHandler<TableEventArgs<TableFreqKnown>> TableFreqKnownReceived;
        public bool IsWorking { get; }
        public string ServerAddress { get; }
        public int ServerPort { get; }
        public TransmissionType Type { get; }
        public bool IsMaster { get; }
        public int OwnStationId { get; }
        public int ConnectedStationId { get; }
        public bool ShowRdfLog { get; set; }
        public event EventHandler<string> OnSend;
        public event EventHandler<string> OnReceive;

        private readonly Transmitter _transmitter;

        public SlowTcpUdpTransmitter(bool isMaster, int ownStationId, int connectedStationId, 
            string localIp, int localTcpPort, int localUdpPort,
            string remoteIp, int remoteTcpPort, int remoteUdpPort)
        {
            IsMaster = isMaster;
            OwnStationId = ownStationId;
            ConnectedStationId = connectedStationId;
            Type = TransmissionType.GrpcTransmitter;
            ServerAddress = localIp;
            ServerPort = localTcpPort;
            ClientAddress = remoteIp;
            ClientPort = remoteTcpPort;
            
            _transmitter = isMaster 
                ? new Transmitter(localIp, localTcpPort, localUdpPort, remoteIp, remoteTcpPort, remoteUdpPort)
                : new Transmitter(localIp, localTcpPort, localUdpPort, remoteUdpPort);
            
            _transmitter.OnLog += TransmitterOnOnLog;
            _transmitter.MessageReceived += TransmitterOnMessageReceived;
            _transmitter.ConnectionStateChanged += TransmitterOnConnectionStateChanged;

            IsWorking = true;
        }

        private void TransmitterOnConnectionStateChanged(object sender, bool newState)
        {
            ConnectionStateChanged?.Invoke(this, newState);
        }

        private void TransmitterOnMessageReceived(object sender, byte[] data)
        {
            var command = (Commands)data[0];
            var length = BitConverter.ToInt32(data, 1);
            switch (command)
            {
                case Commands.Message:
                {
                    var request = TransmissionPackage.Message.Parser.ParseFrom(data, 5, length);
                    if (request.SenderId == -1)
                    {
                        request.SenderId = ConnectedStationId;
                    }

                    OnSendMessageRequest?.Invoke(this, request.ConvertToDataTransferType());
                    break;
                }
                case Commands.Mode:
                {
                    var request = ModeRequest.Parser.ParseFrom(data, 5, length);
                    OnSendModeRequest?.Invoke(this, (byte)request.Mode);
                    break;
                }
                case Commands.AspTable :
                {
                    var request = TransmissionPackage.AspTable.Parser.ParseFrom(data, 5, length);
                    AspTableReceived?.Invoke(this, request.ConvertToDataTransferType());
                    break;
                }
                case Commands.ExecutiveDfRequest :
                {
                    var request = TransmissionPackage.ExecutiveDfRequestArgs.Parser.ParseFrom(data, 5, length);
                    OnSendExecutiveDfRequest?.Invoke(this, request.ConvertToDataTransferType());
                    break;
                }
                case Commands.ExecutiveDfResponse :
                {
                    var request = TransmissionPackage.ExecutiveDfResponseArgs.Parser.ParseFrom(data, 5, length);
                    OnSendExecutiveDfResponse?.Invoke(this, request.ConvertToDataTransferType());
                    break;
                }
                case Commands.TableFrsJam :
                {
                    var request = TableSuppressFWSTable.Parser.ParseFrom(data, 5, length);
                    TableFrsJamReceived?.Invoke(this, request.ConvertToDataTransferType());
                    break;
                }
                case Commands.TableFhssJam :
                {
                    var request = TableSuppressFHSSTable.Parser.ParseFrom(data, 5, length);
                    TableFhssJamReceived?.Invoke(this, request.ConvertToDataTransferType());
                    break;
                }
                case Commands.TableFreqImportant :
                {
                    var request = TableFreqImportantTable.Parser.ParseFrom(data, 5, length);    
                    TableFreqImportantReceived?.Invoke(this, request.ConvertToDataTransferType());
                    break;
                }
                case Commands.TableFreqForbidden :
                {
                    var request = TableFreqForbiddenTable.Parser.ParseFrom(data, 5, length);  
                    TableFreqForbiddenReceived?.Invoke(this, request.ConvertToDataTransferType());
                    break;
                }
                case Commands.TableFreqKnown :
                {
                    var request = TableFreqKnownTable.Parser.ParseFrom(data, 5, length);  
                    TableFreqKnownReceived?.Invoke(this, request.ConvertToDataTransferType());
                    break;
                }
                case Commands.TableSectorsRangesRecon :
                {
                    var request = TableSectorsRangesReconTable.Parser.ParseFrom(data, 5, length);  
                    TableSectorsRangesReconReceived?.Invoke(this, request.ConvertToDataTransferType());
                    break;
                }
                case Commands.TableSectorsRangesSuppr :
                {
                    var request = TableSectorsRangesSupprTable.Parser.ParseFrom(data, 5, length);  
                    TableSectorsRangesSupprReceived?.Invoke(this, request.ConvertToDataTransferType());
                    break;
                }
                case Commands.TableFrsRecon :
                {
                    var request = TableReconFWSTable.Parser.ParseFrom(data, 5, length);  
                    TableFrsReconReceived?.Invoke(this, request.ConvertToDataTransferType());
                    break;
                }
            }
            OnReceive?.Invoke(this, $"Received {command} command from {ConnectedStationId}");
        }

        private void TransmitterOnOnLog(object sender, string e)
        {
            OnSend?.Invoke(this, e);
        }
    }
}