﻿using System;
using ModelsTablesDBLib;
using DataTransferModel.Trasmission;
using DataTransferModel.DataTransfer;

namespace TransmissionLibrary
{
    /*
     * Used instead of null. When we try to create second COM transmitter for example
    */
    public class DummyTransmitter : ITransmitter
    {
        public bool IsMaster => false;
        public int OwnStationId => -1;

        public int ConnectedStationId => -1;

        public bool ShowRdfLog { get; set; }

        public bool IsConnected => false;

        public bool IsWorking => false;

        public string ServerAddress => "null";

        public int ServerPort => -1;

        public TransmissionType Type => TransmissionType.None;

        public string ClientAddress => ServerAddress;

        public int ClientPort => ServerPort;

        public event EventHandler<string> OnSend;
        public event EventHandler<string> OnReceive;
        public event EventHandler<Message> OnSendMessageRequest;
        public event EventHandler<ExecutiveDfRequestArgs> OnSendExecutiveDfRequest;
        public event EventHandler<ExecutiveDfResponseArgs> OnSendExecutiveDfResponse;
        public event EventHandler<byte> OnSendModeRequest;
        public event EventHandler<int> OnSetBandReceived;
        public event EventHandler<DateTime> OnLocalTimeReceived;
        public event EventHandler<RdfCycleResult> OnMasterRdfResultReceived;
        public event EventHandler<RdfCycleResult> OnSlaveRdfResultReceived;
        public event EventHandler<AspTable> AspTableReceived;
        public event EventHandler<TableEventArgs<TableSectorsRangesRecon>> TableSectorsRangesReconReceived;
        public event EventHandler<TableEventArgs<TableSectorsRangesSuppr>> TableSectorsRangesSupprReceived;
        public event EventHandler<TableEventArgs<TableReconFWS>> TableFrsReconReceived;
        public event EventHandler<TableEventArgs<TableReconFHSS>> TableFhssReconReceived;
        public event EventHandler<TableEventArgs<TableSuppressFWS>> TableFrsJamReceived;
        public event EventHandler<TableEventArgs<TableSuppressFHSS>> TableFhssJamReceived;
        public event EventHandler<TableEventArgs<TableFHSSExcludedFreq>> TableFhssExcludedFreqReceived;
        public event EventHandler<TableEventArgs<TableFreqImportant>> TableFreqImportantReceived;
        public event EventHandler<TableEventArgs<TableFreqForbidden>> TableFreqForbiddenReceived;
        public event EventHandler<TableEventArgs<TableFreqKnown>> TableFreqKnownReceived;
        public event EventHandler<PeerInfo> OnForceConnectionRequest;
        public event EventHandler<bool> ConnectionStateChanged;
        public event EventHandler<FhssCycleResult> OnFhssCycleResultReceived;

        public void AbortConnection(bool isMaster)
        {}

        public void ForceConnection(string host, int port)
        {}

        public void Initialize()
        {}

        public bool Ping(int ownStationId, int ownPort)
        {
            return true;
        }

        public bool SendAspTable(AspTable table)
        {
            return true;
        }

        public bool SendExecutiveDfRequest(float startFrequencyKhz, float endFrequencyKhz, int phaseAveragingCount, int directionAveragingCount)
        {
            return true;
        }

        public bool SendExecutiveDfResponse(float result, int stationId)
        {
            return true;
        }

        public bool SendFhssCycleResult(FhssCycleResult cycleResult)
        {
            return true;
        }

        public bool SendLocalTime(DateTime localTime)
        {
            return true;
        }

        public bool SendMasterRdfResult(RdfCycleResult result)
        {
            return true;
        }

        public bool SendMessage(Message message)
        {
            return true;
        }

        public bool SendMode(byte mode)
        {
            return true;
        }

        public bool SendSlaveRdfResult(RdfCycleResult result)
        {
            return true;
        }

        public bool SendTableFhssExcludedFreq(TableEventArgs<TableFHSSExcludedFreq> table)
        {
            return true;
        }

        public bool SendTableFhssJam(TableEventArgs<TableSuppressFHSS> table)
        {
            return true;
        }

        public bool SendTableFhssRecon(TableEventArgs<TableReconFHSS> table)
        {
            return true;
        }

        public bool SendTableFreqForbidden(TableEventArgs<TableFreqForbidden> table)
        {
            return true;
        }

        public bool SendTableFreqImportant(TableEventArgs<TableFreqImportant> table)
        {
            return true;
        }

        public bool SendTableFreqKnown(TableEventArgs<TableFreqKnown> table)
        {
            return true;
        }

        public bool SendTableFrsJam(TableEventArgs<TableSuppressFWS> table)
        {
            return true;
        }

        public bool SendTableFrsRecon(TableEventArgs<TableReconFWS> table)
        {
            return true;
        }

        public bool SendTableSectorsRangesRecon(TableEventArgs<TableSectorsRangesRecon> table)
        {
            return true;
        }

        public bool SendTableSectorsRangesSuppr(TableEventArgs<TableSectorsRangesSuppr> table)
        {
            return true;
        }

        public bool SetBandNumber(int bandNumber)
        {
            return true;
        }

        public void ShutDown()
        {}
    }
}
