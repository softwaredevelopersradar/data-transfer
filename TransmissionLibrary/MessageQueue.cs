﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace TransmissionLibrary
{
    internal class MessageQueue
    {
        private readonly Queue<Func<bool>> _commandQueue;
        private readonly object _lock;

        private bool _isCommandProcessing = false;

        internal event EventHandler<string> ExceptionLog;

        internal MessageQueue() 
        {
            _commandQueue = new Queue<Func<bool>>();
            _lock = new object();

            Task.Run(QueueTask);
        }

        internal bool InvokeWithoutQueueInterruption(Func<bool> command) 
        {
            lock (_lock) 
            {
                while (_isCommandProcessing)
                {
                    System.Threading.Thread.Sleep(1);
                }

                try 
                {
                    var result = command.Invoke();
                    return result;
                }
                catch
                {
                    ExceptionLog?.Invoke(this, $"Transmission exception occured");
                    return false;
                }
            }
        }

        internal void Enqueue(Func<bool> command) 
        {
            lock (_lock)
            {
                _commandQueue.Enqueue(command);
            }
        }

        private async Task QueueTask()
        {
            while (true)
            {
                try 
                {
                    if (_commandQueue.Count == 0)
                    {
                        await Task.Delay(10);
                    }

                    Func<bool> command;
                    lock (_lock)
                    {
                        command = _commandQueue.Peek();
                    }

                    _isCommandProcessing = true;
                    var result = command.Invoke();
                    _isCommandProcessing = false;

                    if (result)
                    {
                        lock (_lock)
                        {
                            _commandQueue.Dequeue(); // successfully processed command
                        }
                    }
                }
                catch(Exception e)
                {
                    _isCommandProcessing = false;
                    ExceptionLog?.Invoke(this, $"Transmission exception occured");
                }
            }
        }
    }
}
