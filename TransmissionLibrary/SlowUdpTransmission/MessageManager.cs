﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TransmissionLibrary.SlowUdpTransmission
{
    public class MessageManager
    {
        private readonly List<(Commands Message, byte[] ByteMessage)> _messageList =
            new List<(Commands, byte[])>(100);
        
        private bool _isMaster;

        public int Count => _messageList.Count;

        public MessageManager(bool isMaster)
        {
            _isMaster = isMaster;
        }

        public void Add(Commands message, byte[] byteMessage)
        {
            if (message != Commands.Message)
            {
                for (var i = 0; i < _messageList.Count; i++)
                {
                    if (_messageList[i].Message != message)
                    {
                        continue;
                    }

                    // just updating the message
                    _messageList[i] = new ValueTuple<Commands, byte[]>(message, byteMessage);
                    return;
                }
            }

            _messageList.Add((message, byteMessage));
            _messageList.Sort(CompareMessages);
        }

        public (Commands Message, byte[] ByteMessage) Get()
        {
            var value = _messageList.First();
            return value;
        }

        public void Remove()
        {
            _messageList.RemoveAt(0);
        }

        private static int CompareMessages((Commands Message, byte[] ByteMessage) a, (Commands Message, byte[] ByteMessage) b)
        {
            if (a.Message != b.Message)
            {
                return a.Message.CompareTo(b.Message);
            }

            const int idIndex = 1;
            return a.ByteMessage[idIndex].CompareTo(b.ByteMessage[idIndex]);
        }
    }
}