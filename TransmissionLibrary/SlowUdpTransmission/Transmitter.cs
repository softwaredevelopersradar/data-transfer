﻿using System;
using System.Threading.Tasks;
using System.Threading;

namespace TransmissionLibrary.SlowUdpTransmission
{
    public class Transmitter
    {
        private readonly Udp _udp;
        private bool _isMaster;

        private readonly MasterPingManager _pingManager;

        public event EventHandler<byte[]> MessageReceived;
        public event EventHandler<bool> ConnectionStateChanged;
        public event EventHandler<string> OnLog;

        private bool _waitingMessagesFromPair = false;
        private readonly string _remoteHost;

        public MessageManager Manager { get; private set; }

        public bool IsConnected => _udp.IsConnected;

        /// <summary>
        ///     Slave ctor
        /// </summary>
        public Transmitter(string local, int localPort, bool isMaster)
        {
            _udp = new Udp(local, localPort, isMaster);
            Initialize(_isMaster);
        }

        /// <summary>
        ///     Master ctor
        /// </summary>
        public Transmitter(string local, int localPort, string remote, int remotePort, bool isMaster)
        {
            _remoteHost = remote;
            _udp = new Udp(local, localPort, remote, remotePort, isMaster);
            Initialize(isMaster);

            if (_isMaster)
            {
                _pingManager = new MasterPingManager(Manager, local, localPort);
                _pingManager.Start();
            }
        }

        private void Initialize(bool isMaster)
        {
            Manager = new MessageManager(isMaster);
            _udp.SetManager(Manager);
            _isMaster = isMaster;

            _udp.MessageReceived += UdpOnMessageReceived;
            _udp.ConnectionStateChanged += UdpOnConnectionStateChanged;
            _udp.OnLog += UdpOnOnLog;
            _udp.StopMasterReceive += UdpOnStopMasterReceive;

            _waitingMessagesFromPair = !isMaster;
            this.forMessageTask = new CancellationTokenSource();
            Task.Run(()=>MessageTask(this.forMessageTask.Token));
        }

        private CancellationTokenSource forMessageTask;
        private void DeInitialize()
        {
            _udp.MessageReceived -= UdpOnMessageReceived;
            _udp.ConnectionStateChanged -= UdpOnConnectionStateChanged;
            _udp.OnLog -= UdpOnOnLog;
            _udp.StopMasterReceive -= UdpOnStopMasterReceive;
        }

        public void ShutDown()
        {
            forMessageTask.Cancel();
            StopReceiving();
            this._udp.ShutDown();
            DeInitialize();
        }

        private void UdpOnOnLog(object sender, string message)
        {
            OnLog?.Invoke(this, message);
        }

        private void UdpOnConnectionStateChanged(object sender, bool state)
        {
            ConnectionStateChanged?.Invoke(this, state);
        }

        private void UdpOnMessageReceived(object sender, byte[] data)
        {
            var command = (Commands)data[0];
            if (!_isMaster)
            {
                if (command == Commands.AllowSlaveToSendMessages)
                {
                    // master allowed to send data
                    StopReceiving();
                }
                if (command == Commands.PingCalibration)
                {
                    var timeoutMs = data[2] * 1000;
                    _udp.SetDelay(timeoutMs);
                }
            }

            MessageReceived?.Invoke(this, data);
        }

        private void UdpOnStopMasterReceive(object sender, EventArgs e)
        {
            StopReceiving();
        }

        private async Task MessageTask(CancellationToken token)
        {
            while (!token.IsCancellationRequested)
            //while (_udp.IsConnected)
            {
                try
                {
                    await Task.Delay(10);
                    if (Manager.Count == 0 || _waitingMessagesFromPair)
                    {
                        continue;
                    }

                    if (Manager.Count == 0 && !_isMaster)
                    {
                        // seems like unlikely situation, but can happen, so just stop sending
                        StartReceiving();
                    }

                    var (message, byteMessage) = Manager.Get();
                    if (message == Commands.PingCalibration)
                    {
                        var calibrationResult = PingCalibration();
                        if (calibrationResult)
                        {
                            Manager.Remove();
                        }

                        continue;
                    }

                    var result = _udp.Send(message, byteMessage);
                    if (!result)
                    {
                        continue;
                    }

                    Manager.Remove();
                    if (!_isMaster || message == Commands.AllowSlaveToSendMessages)
                    {
                        StartReceiving();
                    }
                }
                catch(Exception e)
                {
                    OnLog?.Invoke(this, $"Exception occured in sending task, please restart the app, {e.ToString()}\r\n" +
                                        $"Trace: {e.StackTrace}");
                    break;
                }
            }
        }

        private void StartReceiving()
        {
            _waitingMessagesFromPair = true;
            _udp.Start();
        }
        
        private void StopReceiving()
        {
            _udp.Stop();
            _waitingMessagesFromPair = false;
        }

        private bool PingCalibration()
        {
            var timeoutMs = SlowUdpTransmission.PingCalibration.Calibrate(_remoteHost);
            if (timeoutMs < 1000)
            {
                timeoutMs = 1000;
            }

            _udp.SetDelay(timeoutMs);
            _pingManager.SetPingDelay(timeoutMs);
            var timeoutSec = (byte)(Math.Ceiling(SlowUdpTransmission.PingCalibration.Calibrate(_remoteHost) * 1.0 / 1000));
            var data = new[] {(byte) Commands.PingCalibration, Encoder.GetMessageId(), timeoutSec};
            var result = _udp.Send(Commands.PingCalibration, data);
            return result;
        }

        public void SetDelay(int delayMs)
        {
            _udp.SetDelay(delayMs);
            _pingManager?.SetPingDelay(delayMs);
        }
    }
}