﻿using System;
using Google.Protobuf;

namespace TransmissionLibrary.SlowUdpTransmission
{
    public static class Encoder
    {
        private static byte _messageId = 0;
        public static void Send(this Transmitter transmitter, IMessage message, Commands command)
        {
            var bytes = message.Convert(command);
            transmitter.Manager.Add(command, bytes);
        }

        private static byte[] Convert(this IMessage message, Commands command)
        {
            var array = message.ToByteArray();
            var output = new byte[array.Length + 1 + 1 + 4]; // command/id/messagelength
            Array.Copy(array, 0, output, 6, array.Length);
            output[0] = (byte)command;
            output[1] = GetMessageId();
            var length = BitConverter.GetBytes(array.Length);
            Array.Copy(length, 0, output, 2, length.Length);
            return output;
        }

        public static byte GetMessageId()
        {
            var output = _messageId;
            _messageId++;
            if (_messageId == 255)
            {
                _messageId = 0;
            }

            return output;
        }
    }
}