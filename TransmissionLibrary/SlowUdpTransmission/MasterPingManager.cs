﻿using System.Threading.Tasks;

namespace TransmissionLibrary.SlowUdpTransmission
{
    public class MasterPingManager
    {
        private bool _isWorking;
        private int _pingDelayMs = 1_000;
        private readonly MessageManager _manager;
        private readonly byte[] _pingMessage;

        public MasterPingManager(MessageManager manager, string host, int port)
        {
            var pingMessage = new PingMessage() { IpAddress = host, Port = port };
            _pingMessage = pingMessage.GetBytes();
            _manager = manager;
        }

        public void Start()
        {
            _isWorking = true;
            Task.Run(PingTask);
        }

        public void Stop()
        {
            _isWorking = false;
        }

        public void SetPingDelay(int pingDelaySec)
        {
            _pingDelayMs = pingDelaySec;
        }

        private async Task PingTask()
        {
            while (_isWorking)
            {
                _manager.Add(Commands.Ping, _pingMessage);
                await Task.Delay(_pingDelayMs).ConfigureAwait(false);
            }
        }
    }
}