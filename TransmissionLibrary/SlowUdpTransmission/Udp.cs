﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace TransmissionLibrary.SlowUdpTransmission
{
    public class Udp
    {
        private readonly UdpClient _udp;
        private IPEndPoint _remote;

        public event EventHandler<byte[]> MessageReceived;
        public event EventHandler<bool> ConnectionStateChanged;
        public event EventHandler<string> OnLog;
        public event EventHandler StopMasterReceive;

        private bool _isWorking = false;
        private bool _connectionEstablished = false;
        private readonly bool _isMaster;

        private int _failedReceives = 0;
        private bool _isConnected;
        private MessageManager _manager;
        private byte _lastReceivedId = 255;

        public bool IsConnected
        {
            get => _isConnected;
            set
            {
                if (_isConnected == value)
                {
                    return;
                }

                _isConnected = value;
                ConnectionStateChanged?.Invoke(this, _isConnected);
            }
        }

        /// <summary>
        ///     Master ctor who knows where to send and from what port to listen
        /// </summary>
        public Udp(string local, int localPort, string remote, int remotePort, bool isMaster)
            : this(local, localPort)
        {
            _isMaster = isMaster;
            _remote = new IPEndPoint(IPAddress.Parse(remote), remotePort);
            _udp.Connect(_remote);
            _connectionEstablished = true; // master already knows the slave
        }

        /// <summary>
        ///     Slave ctor who does not know from what ip or port to listen
        /// </summary>
        public Udp(string local, int localPort, bool isMaster) : this(local, localPort)
        {
            _isMaster = isMaster;
            _remote = new IPEndPoint(IPAddress.Any, 0);
            Start();
        }

        private Udp(string local, int localPort)
        {
            _udp = new UdpClient(new IPEndPoint(IPAddress.Parse(local), localPort));
            _udp.Client.ReceiveTimeout = 1000;
            _udp.Client.SendTimeout = 1000;
        }

        public void SetDelay(int delayMs)
        {
            _udp.Client.ReceiveTimeout = delayMs;
            _udp.Client.SendTimeout = delayMs;
        }

        public void SetManager(MessageManager manager)
        {
            _manager = manager;
        }

        public bool Send(Commands message, byte[] byteMessage)
        {
            if (_remote.Port == 0)
            {
                return false;
            }

            if (byteMessage == null)
                return false;

            var result = _udp.SendReceive(byteMessage);
            OnLog?.Invoke(this, $"Sent {message} command to {_remote}, result : {result != null}");

            if (result != null && message == Commands.Ping)
            {
                var slavePendingCommands = result[1];
                if (slavePendingCommands != 0 && _isMaster)
                {
                    var data = new[] {(byte) Commands.AllowSlaveToSendMessages, Encoder.GetMessageId()};
                    _manager.Add(Commands.AllowSlaveToSendMessages, data);
                }
            }

            IsConnected = result != null;
            return IsConnected;
        }

        public void Start()
        {
            _isWorking = true;
            Task.Run(ListeningTask);
        }

        public void Stop() 
        {
            _isWorking = false;
            //if (_udp != null)
            //{
            //    _udp.Close();
            //    _udp.Dispose();
            //}
        }

        public void ShutDown()
        {
            _isWorking = false;
            if (_udp != null)
            {
                _udp.Close();
                _udp.Dispose();
            }
        }

        private async Task ListeningTask() 
        {
            while (_isWorking)
            {
                try
                {
                    OnLog?.Invoke(this, $"Waiting for requests");
                    var data = _udp.Receive(ref _remote);
                    _failedReceives = 0;
                    // slave waiting for ping request to establish connection
                    if (!_connectionEstablished && data[0] != (byte)Commands.Ping)
                    {
                        throw new Exception();
                    }

                    if (!_connectionEstablished)
                    {
                        _udp.Connect(_remote);
                        IsConnected = true;
                        _connectionEstablished = true;
                    }

                    var command = (Commands) data[0];
                    if (command == Commands.Ping)
                    {
                        SendConfirmation(data, false, LogMessage(false, command));
                        continue;
                    }

                    var commandId = data[1];
                    if(commandId == _lastReceivedId)
                    {
                        SendConfirmation(data, true, LogMessage(true, command));
                        continue;
                    }

                    _lastReceivedId = commandId;

                    SendConfirmation(data, false, LogMessage(false, command));
                }
                catch (Exception e)
                {
                    _failedReceives++;
                    if (_failedReceives == 3 && _isMaster)
                    {
                        // master doesn't wait for too long
                        StopMasterReceive?.Invoke(this, EventArgs.Empty);
                    }

                    if (_connectionEstablished && !_isMaster && _failedReceives == 10)
                    {
                        // slave losing connection
                        // todo
                        //_connectionEstablished = false;
                    }
                    
                    await Task.Delay(1);
                }
            }

            string LogMessage(bool isDuplicate, Commands command)
            {
                return !isDuplicate 
                    ? $"Received command : {command}" 
                    : $"Received command : {command} duplicate";
            }
        }

        private void SendConfirmation(byte[] data, bool isDuplicate, string logMessage)
        {
            OnLog?.Invoke(this, logMessage);
            var amount = (byte)_manager.Count;
            var byteMessage = _isMaster
                ? new byte[] { data[0] }
                : new byte[] { data[0], amount };
            _udp.Send(byteMessage, byteMessage.Length);

            if (!isDuplicate)
            {
                MessageReceived?.Invoke(this, data);
            }

            if (_isMaster)
            {
                // master receives only one message
                StopMasterReceive?.Invoke(this, EventArgs.Empty);
            }
        }
    }
}
