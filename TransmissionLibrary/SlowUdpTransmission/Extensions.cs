﻿using System;
using System.Net;
using System.Net.Sockets;

namespace TransmissionLibrary.SlowUdpTransmission
{
    public static class TransmissionChannelExtensions
    {
        /// <summary>
        ///     Send byte[] message to an already set up host and receive an answer to the message
        /// </summary>
        /// <returns> Returns null if failed to receive </returns>
        public static byte[] SendReceive(this UdpClient client, byte[] message)
        {
            lock (client)
            {
                var commandResendTries = 3;
                while (commandResendTries != 0)
                {
                    try
                    {
     
                        client.Send(message, message.Length);

                        var response = client.Receive();
                        // didn't receive response at all
                        if (response.Length == 0)
                            throw new SocketException();
                        // received another command response
                        //if (message[0] != response[0])
                        //    throw new SocketException();

                        return response;
                    }
                    catch (Exception e)
                    {
                        commandResendTries--;
                        // todo : log error
                    }
                }

                return null;
            }
        }
        
        /// <summary>
        ///     Receive udp package from already set up remote end point
        /// </summary>
        public static byte[] Receive(this UdpClient client)
        {
            try
            {
                var clientRemotePoint = client.Client.RemoteEndPoint.ToString().Split(new[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                var ipEndPoint = new IPEndPoint(IPAddress.Parse(clientRemotePoint[0]), Int32.Parse(clientRemotePoint[1]));
                return client.Receive(ref ipEndPoint);
            }
            catch
            {
                return new byte[] { };
            }
        }
    }
}
