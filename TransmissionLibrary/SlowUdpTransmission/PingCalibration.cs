﻿using System;
using System.Net.NetworkInformation;

namespace TransmissionLibrary.SlowUdpTransmission
{
    public static class PingCalibration
    {
        private static readonly Random God = new Random(42424242);
        public static bool Calibrated { get; private set; } = false;
        private static int _timeout = 0;
        
        public static int Calibrate(string host)
        {
            if (Calibrated)
            {
                return _timeout;
            }

            var ping = new Ping();
            const int numberOfPackets = 20;
            const int packetSize = 100;
            var timeoutMs = 1000;
            long overallTimeMs = 0;
            var index = 1;
            var bytes = new byte[packetSize];
            var maxFailedCount = 10;
            
            while (index != numberOfPackets)
            {
                try
                {
                    if (maxFailedCount == 0)
                    {
                        return 1000;
                        // rly bad connection
                    }

                    God.NextBytes(bytes);
                    var reply = ping.Send(host, timeoutMs, bytes);
                    overallTimeMs += reply.RoundtripTime;
                    timeoutMs = (int)overallTimeMs / index;
                    index++;
                }
                catch
                {
                    maxFailedCount--;
                    timeoutMs *= 2;
                    index = 0;
                }
            }

            const string file = "PingCalibration.txt";
            System.IO.File.AppendAllText(file, $"{DateTime.Now} : Calibration timeout {timeoutMs} ms\r\n");
            
            Calibrated = true;
            _timeout = timeoutMs;
            return timeoutMs;
        }
    }
}