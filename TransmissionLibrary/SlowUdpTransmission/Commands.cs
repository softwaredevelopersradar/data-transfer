﻿namespace TransmissionLibrary.SlowUdpTransmission
{
    public enum Commands : byte
    {
        Ping = 99,
        PingCalibration = 100,
        Message = 101,
        Mode = 102,
        ExecutiveDfRequest = 103,
        ExecutiveDfResponse = 104,
        AllowSlaveToSendMessages = 105,
        
        // anything below this is considered as low priority message
        // and will be send as soon as all high priority messages are sent
        
        AspTable = 106,
        TableSectorsRangesRecon = 107,
        TableSectorsRangesSuppr = 108,
        TableFrsRecon = 109,
        TableFrsJam = 110,
        TableFhssJam = 111,
        TableFreqImportant = 112,
        TableFreqKnown = 113,
        TableFreqForbidden = 114
    }
}