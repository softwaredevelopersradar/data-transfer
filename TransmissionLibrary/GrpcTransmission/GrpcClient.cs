﻿using System;
using Grpc.Core;
using DataTransferModel;
using DataTransferModel.Trasmission;
using DataTransferModel.DataTransfer;
using ModelsTablesDBLib;

namespace TransmissionLibrary.GrpcTransmission
{
    internal class GrpcClient : ITransmissionClient
    {
        private Channel _channel;
        private TransmissionPackage.Transmission.TransmissionClient _client;
        private bool _isInitialized;

        public event EventHandler<bool> ConnectionStateChanged;

        private DateTime Deadline => DateTime.UtcNow.AddSeconds(Constants.DeadlineSec);

        private bool _isConnected;
        public bool IsConnected 
        {
            get => _isConnected;
            private set 
            {
                if (_isConnected != value)
                    ConnectionStateChanged?.Invoke(this, value);
                _isConnected = value;
            } 
        }

        public string ClientAddress { get; private set; }

        public int ClientPort { get; private set; }

        internal GrpcClient() { }

        internal GrpcClient(string host, int port)
        {
            ForceConnection(host, port);
        }

        public bool Ping(int ownStationId, int ownPort)
        {
            if (_isInitialized == false)
                return false;
            try
            {
                var pingResult = _client.Ping(
                    request: new TransmissionPackage.PingRequest() { LocalPort = ownPort, StationId = ownStationId }, 
                    deadline: Deadline).IsSucceeded;
                IsConnected = pingResult;
                return pingResult;
            }
            catch (RpcException e )
            {
                IsConnected = false; 
            } //deadline exception, todo!
            return false;
        }

        /// <summary>
        /// Used for manual connection, to have flexability to connect via event and so one. 
        /// BUT not for dymanic reconnects to different hosts
        /// </summary>
        public void ForceConnection(string host, int port)
        {
            if (_isInitialized)
                return;

            ClientAddress = host;
            ClientPort = port;
            _channel = new Channel(ClientAddress, ClientPort, ChannelCredentials.Insecure);
            _client = new TransmissionPackage.Transmission.TransmissionClient(_channel);
            _isInitialized = true;
        }

        public void AbortConnection(bool isMaster) 
        {
            if (isMaster && _isInitialized)
                return;
            if (!_isInitialized)
                return;

            IsConnected = false;
            _isInitialized = false;
            _channel.ShutdownAsync().Wait();
            _client = null;
        }

        //We do not manually connect with grpc, that's why this method is empty
        public void Initialize()
        { }

        public void ShutDown()
        {
            if (!IsConnected)
                return;
            IsConnected = false;
            _isInitialized = false;
            _channel.ShutdownAsync().Wait();
        }

        public bool SendAspTable(AspTable table) => _client.SendAspTable(table.ConvertToProtoType()).IsSucceeded;

        public bool SendExecutiveDfRequest(float startFrequencyKhz, float endFrequencyKhz, int phaseAveragingCount, int directionAveragingCount)
            => _client.SendExecutiveDfRequest(
                request: new TransmissionPackage.ExecutiveDfRequestArgs() 
                {
                    StationId = -1, //todo : remove from proto?
                    StartFrequencyKhz = startFrequencyKhz,
                    EndFrequencyKhz = endFrequencyKhz,
                    PhaseAveragingCount = phaseAveragingCount,
                    DirectionAveragingCount = directionAveragingCount
                },
                deadline: Deadline).IsSucceeded;

        public bool SendExecutiveDfResponse(float result, int stationId) => 
            _client.SendExecutiveDfResponse(
                request: new TransmissionPackage.ExecutiveDfResponseArgs() { Direction = result, FrequencyKhz = -1, StationId = stationId }, //todo : remove from proto freq & id?
                deadline: Deadline).IsSucceeded;

        public bool SendLocalTime(DateTime localTime) { return true; }

        public bool SendMasterRdfResult(RdfCycleResult result) =>
            _client.SendMasterRdfResult(
                request: result.ConvertToProtoType(),
                deadline: Deadline).IsSucceeded;

        public bool SendMessage(Message message) =>
            _client.SendMessage(
                request: message.ConvertToProtoType(),
                deadline: Deadline).IsSucceeded;

        public bool SendMode(byte mode) =>
            _client.SendMode(
                request: new TransmissionPackage.ModeRequest() { Mode = mode},
                deadline: Deadline).IsSucceeded;

        public bool SendSlaveRdfResult(RdfCycleResult result) =>
            _client.SendSlaveRdfResult(
                request: result.ConvertToProtoType(),
                deadline: Deadline).IsSucceeded;

        public bool SetBandNumber(int bandNumber) =>
            _client.SetBandNumber(
                request: new TransmissionPackage.BandNumberRequest() { BandNumber = bandNumber},
                deadline: Deadline).IsSucceeded;

        public bool SendTableFhssExcludedFreq(TableEventArgs<TableFHSSExcludedFreq> args) =>
            _client.SendTableFhssExcludedFreq(
                request: args.ConvertToProtoType(),
                deadline: Deadline).IsSucceeded;

        public bool SendTableFhssJam(TableEventArgs<TableSuppressFHSS> args) =>
            _client.SendTableFhssJam(
                request: args.ConvertToProtoType(),
                deadline: Deadline).IsSucceeded;

        public bool SendTableFhssRecon(TableEventArgs<TableReconFHSS> args) =>
            _client.SendTableFhssRecon(
                request: args.ConvertToProtoType(),
                deadline: Deadline).IsSucceeded;

        public bool SendTableFreqForbidden(TableEventArgs<TableFreqForbidden> args) =>
            _client.SendTableFreqForbidden(
                request: args.ConvertToProtoType(),
                deadline: Deadline).IsSucceeded;

        public bool SendTableFreqImportant(TableEventArgs<TableFreqImportant> args) =>
            _client.SendTableFreqImportant(
                request: args.ConvertToProtoType(),
                deadline: Deadline).IsSucceeded;

        public bool SendTableFreqKnown(TableEventArgs<TableFreqKnown> args) =>
            _client.SendTableFreqKnown(
                request: args.ConvertToProtoType(),
                deadline: Deadline).IsSucceeded;

        public bool SendTableFrsJam(TableEventArgs<TableSuppressFWS> args) =>
            _client.SendTableFrsJam(
                request: args.ConvertToProtoType(),
                deadline: Deadline).IsSucceeded;

        public bool SendTableFrsRecon(TableEventArgs<TableReconFWS> args) =>
            _client.SendTableFrsRecon(
                request: args.ConvertToProtoType(),
                deadline: Deadline).IsSucceeded;

        public bool SendTableSectorsRangesRecon(TableEventArgs<TableSectorsRangesRecon> args) =>
            _client.SendTableSectorsRangesRecon(
                request: args.ConvertToProtoType(),
                deadline: Deadline).IsSucceeded;

        public bool SendTableSectorsRangesSuppr(TableEventArgs<TableSectorsRangesSuppr> args) => 
            _client.SendTableSectorsRangesSuppr(
                request: args.ConvertToProtoType(),
                deadline: Deadline).IsSucceeded;

        public bool SendFhssCycleResult(FhssCycleResult cycleResult) =>
            _client.SendFhssCycleResult(
                request: cycleResult.ConvertToProtoType(),
                deadline: Deadline).IsSucceeded;
    }
}
