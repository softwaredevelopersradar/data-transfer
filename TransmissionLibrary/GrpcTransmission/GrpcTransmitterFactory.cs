﻿using System;
using DataTransferModel.Trasmission;
using DataTransferModel;

namespace TransmissionLibrary.GrpcTransmission
{
    public class GrpcTransmitterFactory : ITransmitterFactory
    {
        /// <summary>
        /// We want to have a single server, for each connection type except RS-232.
        /// </summary>
        private ITransmissionServer _grpcServer;
        private string _serverHost = "127.0.0.1";
        private int _serverPort = 14888;

        private bool _isServerCreated = false;

        /// <summary>
        /// Flag to ensure that we passed our server only once
        /// </summary>
        private bool _isServerUsed = false;
        private readonly EthernetConnectionType _connectionType;

        public GrpcTransmitterFactory(string serverHost, int serverPort, EthernetConnectionType type = EthernetConnectionType.Fast) 
        {
            _connectionType = type;
            Initialize(serverHost, serverPort);
        }

        public string GetIp()
        {
            return _serverHost;
        }

        public int GetPort()
        {
            return _serverPort;
        }

        private void CreateServerIfNecessary() 
        {
            if (_isServerCreated)
                return;

            _grpcServer = new GrpcServer(_serverHost, _serverPort);
            _grpcServer.Initialize();
            _isServerCreated = true;
        }

        /// <summary>
        /// Left public t
        /// </summary>
        public void Initialize(string serverHost, int serverPort) 
        {
            _serverHost = serverHost;
            _serverPort = serverPort;
            
            // if server with another settings already running
            if (_isServerCreated)
            {
                _grpcServer.ShutDown();
                _isServerCreated = false;
                _isServerUsed = false;
            }
        }

        public void Initialize(string ipString)
        {
            var parts = ipString.Split(':');
            Initialize(parts[0], Int32.Parse(parts[1]));
        }

        /// <summary>
        /// Creates grpc transmitter which is not connected to any certain linked station
        /// </summary>
        public ITransmitter CreateTransmitter(int ownStationId, int connectedStationId, bool isMaster)
        {
            CreateServerIfNecessary();
            var grpcClient = new GrpcClient();
            return CreateTransmitter(ownStationId, connectedStationId, grpcClient, isMaster);
        }

        /// <summary>
        /// Creates grpc transmitter which is connected to a certain linked station
        /// </summary>
        public ITransmitter CreateTransmitter(int ownStationId, int connectedStationId, string linkedStationHost, int linkedStationPort, bool isMaster) 
        {
            CreateServerIfNecessary();
            var grpcClient = new GrpcClient(linkedStationHost, linkedStationPort);
            return CreateTransmitter(ownStationId, connectedStationId, grpcClient, isMaster);
        }

        private ITransmitter CreateTransmitter(int ownStationId, int linkedStationId, ITransmissionClient grpcClient, bool isMaster)
        {
            if (_isServerUsed)
            {
                return new GrpcTransmitter(ownStationId, linkedStationId, grpcClient, new DummyGrpcServer(), isMaster);
            }

            _isServerUsed = true;
            return new GrpcTransmitter(ownStationId, linkedStationId, grpcClient, _grpcServer, isMaster);
        }
    }
}
