﻿using DataTransferModel.DataTransfer;
using DataTransferModel.Trasmission;
using ModelsTablesDBLib;
using System;

namespace TransmissionLibrary.GrpcTransmission
{
    /// <summary>
    /// This is a dummy. Nothing NEVER invoked, 
    /// </summary>
    internal class DummyGrpcServer : ITransmissionServer
    {
        public bool IsWorking { get; private set; }

        public string ServerAddress { get; private set; }

        public int ServerPort { get; private set; }

        public event EventHandler<PeerInfo> OnForceConnectionRequest;

        public event EventHandler<Message> OnSendMessageRequest;
        public event EventHandler<ExecutiveDfRequestArgs> OnSendExecutiveDfRequest;
        public event EventHandler<ExecutiveDfResponseArgs> OnSendExecutiveDfResponse;
        public event EventHandler<byte> OnSendModeRequest;
        public event EventHandler<int> OnSetBandReceived;
        public event EventHandler<DateTime> OnLocalTimeReceived;
        public event EventHandler<RdfCycleResult> OnMasterRdfResultReceived;
        public event EventHandler<RdfCycleResult> OnSlaveRdfResultReceived;
        public event EventHandler<AspTable> AspTableReceived;
        public event EventHandler<TableEventArgs<TableSectorsRangesRecon>> TableSectorsRangesReconReceived;
        public event EventHandler<TableEventArgs<TableSectorsRangesSuppr>> TableSectorsRangesSupprReceived;
        public event EventHandler<TableEventArgs<TableReconFWS>> TableFrsReconReceived;
        public event EventHandler<TableEventArgs<TableReconFHSS>> TableFhssReconReceived;
        public event EventHandler<TableEventArgs<TableSuppressFWS>> TableFrsJamReceived;
        public event EventHandler<TableEventArgs<TableSuppressFHSS>> TableFhssJamReceived;
        public event EventHandler<TableEventArgs<TableFHSSExcludedFreq>> TableFhssExcludedFreqReceived;
        public event EventHandler<TableEventArgs<TableFreqImportant>> TableFreqImportantReceived;
        public event EventHandler<TableEventArgs<TableFreqForbidden>> TableFreqForbiddenReceived;
        public event EventHandler<TableEventArgs<TableFreqKnown>> TableFreqKnownReceived;
        public event EventHandler<FhssCycleResult> OnFhssCycleResultReceived;

        public void Initialize()
        {
            IsWorking = true;
        }

        public void ShutDown()
        {
            IsWorking = false;
        }
    }
}
