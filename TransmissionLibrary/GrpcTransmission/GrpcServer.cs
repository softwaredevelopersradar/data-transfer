﻿using System;
using DataTransferModel.DataTransfer;
using DataTransferModel.Trasmission;
using ModelsTablesDBLib;

namespace TransmissionLibrary.GrpcTransmission
{
    internal class GrpcServer : ITransmissionServer
    {
        private Grpc.Core.Server _server;
        private readonly ProtoServer _service;

        public bool IsWorking { get; private set; }

        public string ServerAddress { get; private set; }

        public int ServerPort { get; private set; }

        public event EventHandler<PeerInfo> OnForceConnectionRequest;

        public event EventHandler<Message> OnSendMessageRequest;
        public event EventHandler<ExecutiveDfRequestArgs> OnSendExecutiveDfRequest;
        public event EventHandler<ExecutiveDfResponseArgs> OnSendExecutiveDfResponse;
        public event EventHandler<byte> OnSendModeRequest;
        public event EventHandler<int> OnSetBandReceived;
        public event EventHandler<DateTime> OnLocalTimeReceived;
        public event EventHandler<RdfCycleResult> OnMasterRdfResultReceived;
        public event EventHandler<RdfCycleResult> OnSlaveRdfResultReceived;

        public event EventHandler<AspTable> AspTableReceived;
        public event EventHandler<TableEventArgs<TableSectorsRangesRecon>> TableSectorsRangesReconReceived;
        public event EventHandler<TableEventArgs<TableSectorsRangesSuppr>> TableSectorsRangesSupprReceived;
        public event EventHandler<TableEventArgs<TableReconFWS>> TableFrsReconReceived;
        public event EventHandler<TableEventArgs<TableReconFHSS>> TableFhssReconReceived;
        public event EventHandler<TableEventArgs<TableSuppressFWS>> TableFrsJamReceived;
        public event EventHandler<TableEventArgs<TableSuppressFHSS>> TableFhssJamReceived;
        public event EventHandler<TableEventArgs<TableFHSSExcludedFreq>> TableFhssExcludedFreqReceived;
        public event EventHandler<TableEventArgs<TableFreqImportant>> TableFreqImportantReceived;
        public event EventHandler<TableEventArgs<TableFreqForbidden>> TableFreqForbiddenReceived;
        public event EventHandler<TableEventArgs<TableFreqKnown>> TableFreqKnownReceived;
        public event EventHandler<FhssCycleResult> OnFhssCycleResultReceived;

        internal GrpcServer(string host, int port)
        {
            _service = new ProtoServer();
            ServerAddress = host;
            ServerPort = port;
        }
               
        public void Initialize()
        {
            if (IsWorking)
                return;
            _server = new Grpc.Core.Server
            {
                Services = { TransmissionPackage.Transmission.BindService(_service) },
                Ports = { new Grpc.Core.ServerPort(ServerAddress, ServerPort, Grpc.Core.ServerCredentials.Insecure) }
            };
            IsWorking = true;
            SubscribeToServiceEvents();
            _server.Start();
        }

        public void ShutDown()
        {
            if (!IsWorking)
                return;
            IsWorking = false;
            UnsubscribeFromServiceEvents();
            _server.ShutdownAsync().Wait();
        }

        private void SubscribeToServiceEvents()
        {
            _service.OnForceConnectionRequest += _service_OnForceConnectionRequest;

            _service.OnSendMessageRequest += _server_OnSendMessageRequest;
            _service.OnSendModeRequest += _server_OnSendModeRequest;
            _service.OnSendExecutiveDfRequest += _server_OnSendExecutiveDfRequest;
            _service.OnSendExecutiveDfResponse += _server_OnSendExecutiveDfResponse;
            _service.OnSetBandReceived += _server_OnSetBandReceived;
            _service.OnLocalTimeReceived += _server_OnLocalTimeReceived;

            _service.OnMasterRdfResultReceived += _server_OnMasterRdfResultReceived;
            _service.OnSlaveRdfResultReceived += _server_OnSlaveRdfResultReceived;
            _service.OnFhssCycleResultReceived += _service_OnFhssCycleResultReceived;

            _service.AspTableReceived += _server_AspTableReceived;
            _service.TableSectorsRangesReconReceived += _server_TableSectorsRangesReconReceived;
            _service.TableSectorsRangesSupprReceived += _server_TableSectorsRangesSupprReceived;
            _service.TableFrsReconReceived += _server_TableFrsReconReceived;
            _service.TableFhssReconReceived += _server_TableFhssReconReceived;
            _service.TableFrsJamReceived += _server_TableFrsJamReceived;
            _service.TableFhssJamReceived += _server_TableFhssJamReceived;
            _service.TableFhssExcludedFreqReceived += _server_TableFhssExcludedFreqReceived;
            _service.TableFreqImportantReceived += _server_TableFreqImportantReceived;
            _service.TableFreqKnownReceived += _server_TableFreqKnownReceived;
            _service.TableFreqForbiddenReceived += _server_TableFreqForbiddenReceived;
        }

        private void UnsubscribeFromServiceEvents()
        {
            _service.OnForceConnectionRequest -= _service_OnForceConnectionRequest;

            _service.OnSendMessageRequest -= _server_OnSendMessageRequest;
            _service.OnSendModeRequest -= _server_OnSendModeRequest;
            _service.OnSendExecutiveDfRequest -= _server_OnSendExecutiveDfRequest;
            _service.OnSendExecutiveDfResponse -= _server_OnSendExecutiveDfResponse;
            _service.OnSetBandReceived -= _server_OnSetBandReceived;
            _service.OnLocalTimeReceived -= _server_OnLocalTimeReceived;

            _service.OnMasterRdfResultReceived -= _server_OnMasterRdfResultReceived;
            _service.OnSlaveRdfResultReceived -= _server_OnSlaveRdfResultReceived; ;
            _service.OnFhssCycleResultReceived -= _service_OnFhssCycleResultReceived;

            _service.AspTableReceived -= _server_AspTableReceived;
            _service.TableSectorsRangesReconReceived -= _server_TableSectorsRangesReconReceived;
            _service.TableSectorsRangesSupprReceived -= _server_TableSectorsRangesSupprReceived;
            _service.TableFrsReconReceived -= _server_TableFrsReconReceived;
            _service.TableFhssReconReceived -= _server_TableFhssReconReceived;
            _service.TableFrsJamReceived -= _server_TableFrsJamReceived;
            _service.TableFhssJamReceived -= _server_TableFhssJamReceived;
            _service.TableFhssExcludedFreqReceived -= _server_TableFhssExcludedFreqReceived;
            _service.TableFreqImportantReceived -= _server_TableFreqImportantReceived;
            _service.TableFreqKnownReceived -= _server_TableFreqKnownReceived;
            _service.TableFreqForbiddenReceived -= _server_TableFreqForbiddenReceived;
        }

        private void _service_OnFhssCycleResultReceived(object sender, FhssCycleResult fhssResult) => OnFhssCycleResultReceived?.Invoke(sender, fhssResult);
        private void _service_OnForceConnectionRequest(object sender, PeerInfo info) => OnForceConnectionRequest?.Invoke(sender, info);
        private void _server_TableFreqForbiddenReceived(object sender, TableEventArgs<TableFreqForbidden> table) => TableFreqForbiddenReceived?.Invoke(sender, table);
        private void _server_TableFreqKnownReceived(object sender, TableEventArgs<TableFreqKnown> table) => TableFreqKnownReceived?.Invoke(sender, table);
        private void _server_TableFreqImportantReceived(object sender, TableEventArgs<TableFreqImportant> table) => TableFreqImportantReceived?.Invoke(sender, table);
        private void _server_TableFhssExcludedFreqReceived(object sender, TableEventArgs<TableFHSSExcludedFreq> table) => TableFhssExcludedFreqReceived?.Invoke(sender, table);
        private void _server_TableFhssJamReceived(object sender, TableEventArgs<TableSuppressFHSS> table) => TableFhssJamReceived?.Invoke(sender, table);
        private void _server_TableFrsJamReceived(object sender, TableEventArgs<TableSuppressFWS> table) => TableFrsJamReceived?.Invoke(sender, table);
        private void _server_TableFhssReconReceived(object sender, TableEventArgs<TableReconFHSS> table) => TableFhssReconReceived?.Invoke(sender, table);
        private void _server_TableFrsReconReceived(object sender, TableEventArgs<TableReconFWS> table) => TableFrsReconReceived?.Invoke(sender, table);
        private void _server_TableSectorsRangesSupprReceived(object sender, TableEventArgs<TableSectorsRangesSuppr> table) => TableSectorsRangesSupprReceived?.Invoke(sender, table);
        private void _server_TableSectorsRangesReconReceived(object sender, TableEventArgs<TableSectorsRangesRecon> table) => TableSectorsRangesReconReceived?.Invoke(sender, table);
        private void _server_AspTableReceived(object sender, AspTable table) => AspTableReceived?.Invoke(sender, table);
        private void _server_OnSlaveRdfResultReceived(object sender, RdfCycleResult rdfResult) => OnSlaveRdfResultReceived?.Invoke(sender, rdfResult);
        private void _server_OnMasterRdfResultReceived(object sender, RdfCycleResult rdfResult) => OnMasterRdfResultReceived?.Invoke(sender, rdfResult);
        private void _server_OnLocalTimeReceived(object sender, DateTime time) => OnLocalTimeReceived?.Invoke(sender, time);
        private void _server_OnSetBandReceived(object sender, int bandNumber) => OnSetBandReceived?.Invoke(sender, bandNumber);
        private void _server_OnSendExecutiveDfResponse(object sender, ExecutiveDfResponseArgs response) => OnSendExecutiveDfResponse?.Invoke(sender, response);
        private void _server_OnSendExecutiveDfRequest(object sender, ExecutiveDfRequestArgs request) => OnSendExecutiveDfRequest?.Invoke(sender, request);
        private void _server_OnSendModeRequest(object sender, byte mode) => OnSendModeRequest?.Invoke(sender, mode);
        private void _server_OnSendMessageRequest(object sender, Message message) => OnSendMessageRequest?.Invoke(sender, message);
    }
}
