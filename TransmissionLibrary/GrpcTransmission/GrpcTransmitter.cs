﻿using System;
using ModelsTablesDBLib;
using DataTransferModel.DataTransfer;
using DataTransferModel.Trasmission;

namespace TransmissionLibrary.GrpcTransmission
{
    internal class GrpcTransmitter : ITransmitter
    {
        private readonly ITransmissionServer _server;
        private readonly ITransmissionClient _client;

        public bool IsConnected => _client.IsConnected;
        
        public bool IsMaster { get; private set; }
        public int OwnStationId { get; private set; }

        public int ConnectedStationId { get; private set; }
        public bool ShowRdfLog { get; set; }

        public bool IsWorking => _server.IsWorking;

        public string ServerAddress => _server.ServerAddress;

        public int ServerPort => _server.ServerPort;

        public string ClientAddress => _client.ClientAddress;

        public int ClientPort => _client.ClientPort;

        public TransmissionType Type => TransmissionType.GrpcTransmitter;

        public event EventHandler<string> OnSend;
        public event EventHandler<string> OnReceive;
        public event EventHandler<PeerInfo> OnForceConnectionRequest; //not invoked here, lower request is proceeded here

        public event EventHandler<Message> OnSendMessageRequest;
        public event EventHandler<ExecutiveDfRequestArgs> OnSendExecutiveDfRequest;
        public event EventHandler<ExecutiveDfResponseArgs> OnSendExecutiveDfResponse;
        public event EventHandler<byte> OnSendModeRequest;
        public event EventHandler<int> OnSetBandReceived;
        public event EventHandler<DateTime> OnLocalTimeReceived;

        public event EventHandler<RdfCycleResult> OnMasterRdfResultReceived;
        public event EventHandler<RdfCycleResult> OnSlaveRdfResultReceived;

        public event EventHandler<AspTable> AspTableReceived;
        public event EventHandler<TableEventArgs<TableSectorsRangesRecon>> TableSectorsRangesReconReceived;
        public event EventHandler<TableEventArgs<TableSectorsRangesSuppr>> TableSectorsRangesSupprReceived;
        public event EventHandler<TableEventArgs<TableReconFWS>> TableFrsReconReceived;
        public event EventHandler<TableEventArgs<TableReconFHSS>> TableFhssReconReceived;
        public event EventHandler<TableEventArgs<TableSuppressFWS>> TableFrsJamReceived;
        public event EventHandler<TableEventArgs<TableSuppressFHSS>> TableFhssJamReceived;
        public event EventHandler<TableEventArgs<TableFHSSExcludedFreq>> TableFhssExcludedFreqReceived;
        public event EventHandler<TableEventArgs<TableFreqImportant>> TableFreqImportantReceived;
        public event EventHandler<TableEventArgs<TableFreqForbidden>> TableFreqForbiddenReceived;
        public event EventHandler<TableEventArgs<TableFreqKnown>> TableFreqKnownReceived;
        public event EventHandler<bool> ConnectionStateChanged;
        public event EventHandler<FhssCycleResult> OnFhssCycleResultReceived;

        internal GrpcTransmitter(int ownStationId, int connectedStationId, ITransmissionClient client, ITransmissionServer server, bool isMaster)
        {
            OwnStationId = ownStationId;
            ConnectedStationId = connectedStationId;
            _client = client;
            _server = server;
            IsMaster = isMaster;
            SubscribeToServerAndClientEvents();
        }

        public bool Ping(int ownStationId, int ownPort) 
        {
            OnSend?.Invoke(this, $"pinging station {ConnectedStationId}");
            var result = _client.Ping(ownStationId, ownPort);
            OnSend?.Invoke(this, $"Ping result : {result}");
            return result;
        }

        public void Initialize()
        {
            SubscribeToServerAndClientEvents();
            _server.Initialize();
        }

        public void ShutDown()
        {
            UnsubscribeFromServerEvents();
            _client.ShutDown();
            _server.ShutDown();
        }

        public void ForceConnection(string host, int port)
        {
            OnReceive?.Invoke(this, $"Received force connection request to {host}:{port}");
            _client.ForceConnection(host, port);
        }

        public void AbortConnection(bool isMaster)
        {
            _client.AbortConnection(isMaster);
        }

        private void SubscribeToServerAndClientEvents()
        {
            _client.ConnectionStateChanged += _client_ConnectionStateChanged;

            _server.OnForceConnectionRequest += _server_OnForceConnectionRequest;

            _server.OnSendMessageRequest += _server_OnSendMessageRequest;
            _server.OnSendModeRequest += _server_OnSendModeRequest;
            _server.OnSendExecutiveDfRequest += _server_OnSendExecutiveDfRequest;
            _server.OnSendExecutiveDfResponse += _server_OnSendExecutiveDfResponse;
            _server.OnSetBandReceived += _server_OnSetBandReceived;
            _server.OnLocalTimeReceived += _server_OnLocalTimeReceived;

            _server.OnMasterRdfResultReceived += _server_OnMasterRdfResultReceived;
            _server.OnSlaveRdfResultReceived += _server_OnSlaveRdfResultReceived;
            _server.OnFhssCycleResultReceived += _server_OnFhssCycleResultReceived;

            _server.AspTableReceived += _server_AspTableReceived;
            _server.TableSectorsRangesReconReceived += _server_TableSectorsRangesReconReceived;
            _server.TableSectorsRangesSupprReceived += _server_TableSectorsRangesSupprReceived;
            _server.TableFrsReconReceived += _server_TableFrsReconReceived;
            _server.TableFhssReconReceived += _server_TableFhssReconReceived;
            _server.TableFrsJamReceived += _server_TableFrsJamReceived;
            _server.TableFhssJamReceived += _server_TableFhssJamReceived;
            _server.TableFhssExcludedFreqReceived += _server_TableFhssExcludedFreqReceived;
            _server.TableFreqImportantReceived += _server_TableFreqImportantReceived;
            _server.TableFreqKnownReceived += _server_TableFreqKnownReceived;
            _server.TableFreqForbiddenReceived += _server_TableFreqForbiddenReceived;
        }

        private void UnsubscribeFromServerEvents()
        {
            _client.ConnectionStateChanged -= _client_ConnectionStateChanged;

            _server.OnForceConnectionRequest -= _server_OnForceConnectionRequest;

            _server.OnSendMessageRequest -= _server_OnSendMessageRequest;
            _server.OnSendModeRequest -= _server_OnSendModeRequest;
            _server.OnSendExecutiveDfRequest -= _server_OnSendExecutiveDfRequest;
            _server.OnSendExecutiveDfResponse -= _server_OnSendExecutiveDfResponse;
            _server.OnSetBandReceived -= _server_OnSetBandReceived;
            _server.OnLocalTimeReceived -= _server_OnLocalTimeReceived;

            _server.OnMasterRdfResultReceived -= _server_OnMasterRdfResultReceived;
            _server.OnSlaveRdfResultReceived -= _server_OnSlaveRdfResultReceived;
            _server.OnFhssCycleResultReceived -= _server_OnFhssCycleResultReceived;

            _server.AspTableReceived -= _server_AspTableReceived;
            _server.TableSectorsRangesReconReceived -= _server_TableSectorsRangesReconReceived;
            _server.TableSectorsRangesSupprReceived -= _server_TableSectorsRangesSupprReceived;
            _server.TableFrsReconReceived -= _server_TableFrsReconReceived;
            _server.TableFhssReconReceived -= _server_TableFhssReconReceived;
            _server.TableFrsJamReceived -= _server_TableFrsJamReceived;
            _server.TableFhssJamReceived -= _server_TableFhssJamReceived;
            _server.TableFhssExcludedFreqReceived -= _server_TableFhssExcludedFreqReceived;
            _server.TableFreqImportantReceived -= _server_TableFreqImportantReceived;
            _server.TableFreqKnownReceived -= _server_TableFreqKnownReceived;
            _server.TableFreqForbiddenReceived -= _server_TableFreqForbiddenReceived;
        }

        private void _client_ConnectionStateChanged(object sender, bool connectionState)
        {
            ConnectionStateChanged?.Invoke(this, connectionState);
        }

        private void _server_OnFhssCycleResultReceived(object sender, FhssCycleResult fhssResult)
        {
            OnReceive?.Invoke(this, $"Received fhss result from station {ConnectedStationId}");
            OnFhssCycleResultReceived?.Invoke(sender, fhssResult);
        }

        private void _server_OnForceConnectionRequest(object sender, PeerInfo peerInfo)
        {
            OnForceConnectionRequest?.Invoke(this, peerInfo);
        }

        #region Further throwed events
        private void _server_TableFreqForbiddenReceived(object sender, TableEventArgs<TableFreqForbidden> table)
        {
            OnReceive?.Invoke(this, $"Received forbidden from station {ConnectedStationId}");
            TableFreqForbiddenReceived?.Invoke(this, table);
        }
        private void _server_TableFreqKnownReceived(object sender, TableEventArgs<TableFreqKnown> table)
        {
            OnReceive?.Invoke(this, $"Received known from station {ConnectedStationId}");
            TableFreqKnownReceived?.Invoke(this, table);
        }
        private void _server_TableFreqImportantReceived(object sender, TableEventArgs<TableFreqImportant> table)
        {
            OnReceive?.Invoke(this, $"Received important from station {ConnectedStationId}");
            TableFreqImportantReceived?.Invoke(this, table);
        }
        private void _server_TableFhssExcludedFreqReceived(object sender, TableEventArgs<TableFHSSExcludedFreq> table)
        {
            OnReceive?.Invoke(this, $"Received sectors ranges recon from station {ConnectedStationId}");
            TableFhssExcludedFreqReceived?.Invoke(this, table);
        }
        private void _server_TableFhssJamReceived(object sender, TableEventArgs<TableSuppressFHSS> table)
        {
            OnReceive?.Invoke(this, $"Received fhss jam from station {ConnectedStationId}");
            TableFhssJamReceived?.Invoke(this, table);
        }
        private void _server_TableFrsJamReceived(object sender, TableEventArgs<TableSuppressFWS> table)
        {
            OnReceive?.Invoke(this, $"Received frs jam from station {ConnectedStationId}");
            TableFrsJamReceived?.Invoke(this, table);
        }
        private void _server_TableFhssReconReceived(object sender, TableEventArgs<TableReconFHSS> table)
        {
            OnReceive?.Invoke(this, $"Received fhss recon from station {ConnectedStationId}");
            TableFhssReconReceived?.Invoke(this, table);
        }
        private void _server_TableFrsReconReceived(object sender, TableEventArgs<TableReconFWS> table)
        {
            OnReceive?.Invoke(this, $"Received frs recon from station {ConnectedStationId}");
            TableFrsReconReceived?.Invoke(this, table);
        }
        private void _server_TableSectorsRangesSupprReceived(object sender, TableEventArgs<TableSectorsRangesSuppr> table)
        {
            OnReceive?.Invoke(this, $"Received sectors ranges suppress from station {ConnectedStationId}");
            TableSectorsRangesSupprReceived?.Invoke(this, table);
        }
        private void _server_TableSectorsRangesReconReceived(object sender, TableEventArgs<TableSectorsRangesRecon> table)
        {
            OnReceive?.Invoke(this, $"Received sectors ranges recon from station {ConnectedStationId}");
            TableSectorsRangesReconReceived?.Invoke(this, table);
        }
        private void _server_AspTableReceived(object sender, AspTable table)
        {
            OnReceive?.Invoke(this, $"Received asp table from {ConnectedStationId}");
            AspTableReceived?.Invoke(this, table);
        }
        private void _server_OnSlaveRdfResultReceived(object sender, RdfCycleResult rdfResult)
        {
            if(ShowRdfLog)
                OnReceive?.Invoke(this, $"Received slave rdf result from station {ConnectedStationId}");
            OnSlaveRdfResultReceived?.Invoke(this, rdfResult);
        }
        private void _server_OnMasterRdfResultReceived(object sender, RdfCycleResult rdfResult)
        {
            if (ShowRdfLog)
                OnReceive?.Invoke(this, $"Received master rdf result from station {ConnectedStationId}");
            OnMasterRdfResultReceived?.Invoke(this, rdfResult);
        }
        private void _server_OnLocalTimeReceived(object sender, DateTime dateTime)
        {
            OnReceive?.Invoke(this, $"Received date & time from station {ConnectedStationId} : {dateTime}");
            OnLocalTimeReceived?.Invoke(this, dateTime);
        }
        private void _server_OnSetBandReceived(object sender, int bandNumber)
        {
            OnReceive?.Invoke(this, $"Received set band request from station {ConnectedStationId} : {bandNumber}");
            OnSetBandReceived?.Invoke(this, bandNumber);
        }
        private void _server_OnSendExecutiveDfResponse(object sender, ExecutiveDfResponseArgs result)
        {
            OnReceive?.Invoke(this, $"Received executive df result from station {ConnectedStationId} : {result.Direction}");
            OnSendExecutiveDfResponse?.Invoke(this, result);
        }
        private void _server_OnSendExecutiveDfRequest(object sender, ExecutiveDfRequestArgs args)
        {
            OnReceive?.Invoke(this, $"Received executive df request from station {ConnectedStationId}");
            OnSendExecutiveDfRequest?.Invoke(this, args);
        }
        private void _server_OnSendModeRequest(object sender, byte mode)
        {
            OnReceive?.Invoke(this, $"Received mode from station {ConnectedStationId} : {mode}");
            OnSendModeRequest?.Invoke(this, mode);
        }
        private void _server_OnSendMessageRequest(object sender, Message message)
        {
            OnReceive?.Invoke(this, $"Received message from station {ConnectedStationId} : {message.Text}");
            OnSendMessageRequest?.Invoke(this, message);
        }
        #endregion

        #region Further throwed methods
        public bool SendExecutiveDfRequest(float startFrequencyKhz, float endFrequencyKhz, int phaseAveragingCount, int directionAveragingCount)
        {
            OnSend?.Invoke(this, $"Sending executive df request to station {ConnectedStationId}");
            return _client.SendExecutiveDfRequest(startFrequencyKhz, endFrequencyKhz, phaseAveragingCount, directionAveragingCount);
        }

        public bool SendExecutiveDfResponse(float result, int stationId)
        {
            OnSend?.Invoke(this, $"Sending executive df result to station {ConnectedStationId} : {result}");
            return _client.SendExecutiveDfResponse(result, stationId);
        }
        public bool SendLocalTime(DateTime localTime)
        {
            OnSend?.Invoke(this, $"Sending time to station {ConnectedStationId} : {localTime}");
            return _client.SendLocalTime(localTime);
        }
        public bool SendMasterRdfResult(RdfCycleResult result)
        {
            if (ShowRdfLog)
                OnSend?.Invoke(this, $"Sending master rdf result to station {ConnectedStationId}");
            return _client.SendMasterRdfResult(result);
        }
        public bool SendMessage(Message message)
        {
            OnSend?.Invoke(this, $"Sending message to station {ConnectedStationId} : {message.Text}");
            message.UpdateSenderId(OwnStationId);
            return _client.SendMessage(message);
        }
        public bool SendMode(byte mode)
        {
            OnSend?.Invoke(this, $"Sending mode to station {ConnectedStationId} : {mode}");
            return _client.SendMode(mode);
        }
        public bool SendSlaveRdfResult(RdfCycleResult result)
        {
            if (ShowRdfLog)
                OnSend?.Invoke(this, $"Sending slave rdf result to station {ConnectedStationId}");
            return _client.SendSlaveRdfResult(result);
        }
        public bool SetBandNumber(int bandNumber)
        {
            OnSend?.Invoke(this, $"Sending set band number request to station {ConnectedStationId} : {bandNumber}");
            return _client.SetBandNumber(bandNumber);
        }
        public bool SendAspTable(AspTable table)
        {
            OnSend?.Invoke(this, $"Sending asp table to station {ConnectedStationId}");
            try
            {
                return _client.SendAspTable(table);
            }
            catch (Grpc.Core.RpcException rpcException)
            {
                //OnError!
                OnSend?.Invoke(this, $"Rpc exception occured : {rpcException.StatusCode}");
                return false;
            }
        }

        public bool SendTableSectorsRangesRecon(TableEventArgs<TableSectorsRangesRecon> table)
        {
            OnSend?.Invoke(this, $"Sending sectors recon to station {ConnectedStationId}");
            return _client.SendTableSectorsRangesRecon(table);
        }

        public bool SendTableSectorsRangesSuppr(TableEventArgs<TableSectorsRangesSuppr> table)
        {
            OnSend?.Invoke(this, $"Sending sectors suppr to station {ConnectedStationId}");
            return _client.SendTableSectorsRangesSuppr(table);
        }

        public bool SendTableFrsRecon(TableEventArgs<TableReconFWS> table)
        {
            OnSend?.Invoke(this, $"Sending frs recon to station {ConnectedStationId}");
            return _client.SendTableFrsRecon(table);
        }

        public bool SendTableFhssRecon(TableEventArgs<TableReconFHSS> table)
        {
            OnSend?.Invoke(this, $"Sending fhss recon to station {ConnectedStationId}");
            return _client.SendTableFhssRecon(table);
        }

        public bool SendTableFrsJam(TableEventArgs<TableSuppressFWS> table)
        {
            OnSend?.Invoke(this, $"Sending frs jam to station {ConnectedStationId}");
            return _client.SendTableFrsJam(table);
        }

        public bool SendTableFhssJam(TableEventArgs<TableSuppressFHSS> table)
        {
            OnSend?.Invoke(this, $"Sending fhss jam to station {ConnectedStationId}");
            return _client.SendTableFhssJam(table);
        }

        public bool SendTableFhssExcludedFreq(TableEventArgs<TableFHSSExcludedFreq> table)
        {
            OnSend?.Invoke(this, $"Sending fhss excluded to station {ConnectedStationId}");
            return _client.SendTableFhssExcludedFreq(table);
        }

        public bool SendTableFreqImportant(TableEventArgs<TableFreqImportant> table)
        {
            OnSend?.Invoke(this, $"Sending important to station {ConnectedStationId}");
            return _client.SendTableFreqImportant(table);
        }

        public bool SendTableFreqKnown(TableEventArgs<TableFreqKnown> table)
        {
            OnSend?.Invoke(this, $"Sending known to station {ConnectedStationId}");
            return _client.SendTableFreqKnown(table);
        }

        public bool SendTableFreqForbidden(TableEventArgs<TableFreqForbidden> table)
        {
            OnSend?.Invoke(this, $"Sending forbidden to station {ConnectedStationId}");
            return _client.SendTableFreqForbidden(table);
        }

        public bool SendFhssCycleResult(FhssCycleResult cycleResult)
        {
            OnSend?.Invoke(this, $"Sending fhss cycle result to station {ConnectedStationId}");
            return _client.SendFhssCycleResult(cycleResult);
        }
        #endregion
    }
}
