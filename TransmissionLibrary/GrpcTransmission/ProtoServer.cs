﻿using System;
using System.Threading.Tasks;
using Grpc.Core;
using TransmissionPackage;

namespace TransmissionLibrary.GrpcTransmission
{
    /// <summary>
    /// Grpc server implementation.
    /// </summary>
    internal class ProtoServer : Transmission.TransmissionBase , DataTransferModel.Trasmission.IReceiver
    {
        public event EventHandler<DataTransferModel.Trasmission.PeerInfo> OnForceConnectionRequest;

        public event EventHandler<DataTransferModel.DataTransfer.Message> OnSendMessageRequest;
        public event EventHandler<DataTransferModel.DataTransfer.ExecutiveDfRequestArgs> OnSendExecutiveDfRequest;
        public event EventHandler<DataTransferModel.DataTransfer.ExecutiveDfResponseArgs> OnSendExecutiveDfResponse;
        public event EventHandler<byte> OnSendModeRequest;
        public event EventHandler<int> OnSetBandReceived;
        public event EventHandler<System.DateTime> OnLocalTimeReceived;
        public event EventHandler<DataTransferModel.DataTransfer.RdfCycleResult> OnMasterRdfResultReceived;
        public event EventHandler<DataTransferModel.DataTransfer.RdfCycleResult> OnSlaveRdfResultReceived;
        public event EventHandler<DataTransferModel.DataTransfer.FhssCycleResult> OnFhssCycleResultReceived;
        public event EventHandler<DataTransferModel.DataTransfer.AspTable> AspTableReceived;
        public event EventHandler<DataTransferModel.Trasmission.TableEventArgs<ModelsTablesDBLib.TableSectorsRangesRecon>> TableSectorsRangesReconReceived;
        public event EventHandler<DataTransferModel.Trasmission.TableEventArgs<ModelsTablesDBLib.TableSectorsRangesSuppr>> TableSectorsRangesSupprReceived;
        public event EventHandler<DataTransferModel.Trasmission.TableEventArgs<ModelsTablesDBLib.TableReconFWS>> TableFrsReconReceived;
        public event EventHandler<DataTransferModel.Trasmission.TableEventArgs<ModelsTablesDBLib.TableReconFHSS>> TableFhssReconReceived;
        public event EventHandler<DataTransferModel.Trasmission.TableEventArgs<ModelsTablesDBLib.TableSuppressFWS>> TableFrsJamReceived;
        public event EventHandler<DataTransferModel.Trasmission.TableEventArgs<ModelsTablesDBLib.TableSuppressFHSS>> TableFhssJamReceived;
        public event EventHandler<DataTransferModel.Trasmission.TableEventArgs<ModelsTablesDBLib.TableFHSSExcludedFreq>> TableFhssExcludedFreqReceived;
        public event EventHandler<DataTransferModel.Trasmission.TableEventArgs<ModelsTablesDBLib.TableFreqImportant>> TableFreqImportantReceived;
        public event EventHandler<DataTransferModel.Trasmission.TableEventArgs<ModelsTablesDBLib.TableFreqForbidden>> TableFreqForbiddenReceived;
        public event EventHandler<DataTransferModel.Trasmission.TableEventArgs<ModelsTablesDBLib.TableFreqKnown>> TableFreqKnownReceived;

        public override Task<DefaultResponse> SendMessage(Message request, ServerCallContext context)
        {
            OnSendMessageRequest?.Invoke(this, request.ConvertToDataTransferType());
            return Task.FromResult(new DefaultResponse() { IsSucceeded = true });
        }

        public override Task<DefaultResponse> SendMode(ModeRequest request, ServerCallContext context)
        {
            OnSendModeRequest?.Invoke(this, (byte)request.Mode);
            return Task.FromResult(new DefaultResponse() { IsSucceeded = true });
        }

        public override Task<DefaultResponse> Ping(PingRequest request, ServerCallContext context)
        {
            try
            {
                var peerParts = context.Peer.Split(':');
                OnForceConnectionRequest?.Invoke(this,
                    new DataTransferModel.Trasmission.PeerInfo(request.StationId, peerParts[1], request.LocalPort));
            }
            catch
            {
                //parse exception, ignore
            }
            return Task.FromResult(new DefaultResponse() { IsSucceeded = true });
        }

        public override Task<DefaultResponse> SendAspTable(AspTable request, ServerCallContext context)
        {
            AspTableReceived?.Invoke(this, request.ConvertToDataTransferType());
            return Task.FromResult(new DefaultResponse() { IsSucceeded = true });
        }

        public override Task<DefaultResponse> SendExecutiveDfRequest(ExecutiveDfRequestArgs request, ServerCallContext context)
        {
            OnSendExecutiveDfRequest?.Invoke(this, request.ConvertToDataTransferType());
            return Task.FromResult(new DefaultResponse() { IsSucceeded = true });
        }

        public override Task<DefaultResponse> SendExecutiveDfResponse(ExecutiveDfResponseArgs request, ServerCallContext context)
        {
            OnSendExecutiveDfResponse?.Invoke(this, new DataTransferModel.DataTransfer.ExecutiveDfResponseArgs(request.FrequencyKhz, request.Direction, request.StationId));
            return Task.FromResult(new DefaultResponse() { IsSucceeded = true });
        }

        public override Task<DefaultResponse> SendLocalTime(TransmissionPackage.DateTime request, ServerCallContext context)
        {
            OnLocalTimeReceived?.Invoke(this, request.ConvertToSystemType());
            return Task.FromResult(new DefaultResponse() { IsSucceeded = true });
        }

        public override Task<DefaultResponse> SendMasterRdfResult(RdfCycleResult request, ServerCallContext context)
        {
            OnMasterRdfResultReceived?.Invoke(this, request.ConvertToDataTransferType());
            return Task.FromResult(new DefaultResponse() { IsSucceeded = true });
        }

        public override Task<DefaultResponse> SendSlaveRdfResult(RdfCycleResult request, ServerCallContext context)
        {
            OnSlaveRdfResultReceived?.Invoke(this, request.ConvertToDataTransferType());
            return Task.FromResult(new DefaultResponse() { IsSucceeded = true });
        }

        public override Task<DefaultResponse> SendTableFhssExcludedFreq(TableFHSSExcludedFreqTable request, ServerCallContext context)
        {
            TableFhssExcludedFreqReceived?.Invoke(this, request.ConvertToDataTransferType());
            return Task.FromResult(new DefaultResponse() { IsSucceeded = true });
        }

        public override Task<DefaultResponse> SendTableFhssJam(TableSuppressFHSSTable request, ServerCallContext context)
        {
            TableFhssJamReceived?.Invoke(this, request.ConvertToDataTransferType());
            return Task.FromResult(new DefaultResponse() { IsSucceeded = true });
        }

        public override Task<DefaultResponse> SendTableFhssRecon(TableReconFHSSTable request, ServerCallContext context)
        {
            TableFhssReconReceived?.Invoke(this, request.ConvertToDataTransferType());
            return Task.FromResult(new DefaultResponse() { IsSucceeded = true });
        }

        public override Task<DefaultResponse> SendTableFreqForbidden(TableFreqForbiddenTable request, ServerCallContext context)
        {
            TableFreqForbiddenReceived?.Invoke(this, request.ConvertToDataTransferType());
            return Task.FromResult(new DefaultResponse() { IsSucceeded = true });
        }

        public override Task<DefaultResponse> SendTableFreqImportant(TableFreqImportantTable request, ServerCallContext context)
        {
            TableFreqImportantReceived?.Invoke(this, request.ConvertToDataTransferType());
            return Task.FromResult(new DefaultResponse() { IsSucceeded = true });
        }

        public override Task<DefaultResponse> SendTableFreqKnown(TableFreqKnownTable request, ServerCallContext context)
        {
            TableFreqKnownReceived?.Invoke(this, request.ConvertToDataTransferType());
            return Task.FromResult(new DefaultResponse() { IsSucceeded = true });
        }

        public override Task<DefaultResponse> SendTableFrsJam(TableSuppressFWSTable request, ServerCallContext context)
        {
            TableFrsJamReceived?.Invoke(this, request.ConvertToDataTransferType());
            return Task.FromResult(new DefaultResponse() { IsSucceeded = true });
        }

        public override Task<DefaultResponse> SendTableFrsRecon(TableReconFWSTable request, ServerCallContext context)
        {
            TableFrsReconReceived?.Invoke(this, request.ConvertToDataTransferType());
            return Task.FromResult(new DefaultResponse() { IsSucceeded = true });
        }

        public override Task<DefaultResponse> SendTableSectorsRangesRecon(TableSectorsRangesReconTable request, ServerCallContext context)
        {
            TableSectorsRangesReconReceived?.Invoke(this, request.ConvertToDataTransferType());
            return Task.FromResult(new DefaultResponse() { IsSucceeded = true });
        }

        public override Task<DefaultResponse> SendTableSectorsRangesSuppr(TableSectorsRangesSupprTable request, ServerCallContext context)
        {
            TableSectorsRangesSupprReceived?.Invoke(this, request.ConvertToDataTransferType());
            return Task.FromResult(new DefaultResponse() { IsSucceeded = true });
        }

        public override Task<DefaultResponse> SetBandNumber(BandNumberRequest request, ServerCallContext context)
        {
            OnSetBandReceived?.Invoke(this, request.BandNumber);
            return Task.FromResult(new DefaultResponse() { IsSucceeded = true });
        }

        public override Task<DefaultResponse> SendFhssCycleResult(FhssCycleResult request, ServerCallContext context)
        {
            OnFhssCycleResultReceived?.Invoke(this, request.ConvertToDataTransferType());
            return Task.FromResult(new DefaultResponse() { IsSucceeded = true });
        }
    }
}
