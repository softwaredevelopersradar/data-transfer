﻿using System.Linq;
using ModelsTablesDBLib;

namespace TransmissionLibrary
{
    /// <summary>
    /// Implements all conversions between modeltables or data transfer types and .proto types 
    /// </summary>
    internal static class Converter
    {
        internal static TransmissionPackage.InterferenceParam ConvertToProtoType(this ModelsTablesDBLib.InterferenceParam args)
        {
            return new TransmissionPackage.InterferenceParam()
            {
                Deviation = args.Deviation,
                Duration = args.Duration,
                Manipulation = args.Manipulation,
                Modulation = args.Modulation
            };
        }

        internal static ModelsTablesDBLib.InterferenceParam ConvertToDatabaseType(this TransmissionPackage.InterferenceParam args)
        {
            return new ModelsTablesDBLib.InterferenceParam()
            {
                Deviation = (byte)args.Deviation,
                Duration = (byte)args.Duration,
                Manipulation = (byte)args.Manipulation,
                Modulation = (byte)args.Modulation
            };
        }

        internal static TransmissionPackage.JamDirect ConvertToProtoType(this ModelsTablesDBLib.JamDirect args)
        {
            return new TransmissionPackage.JamDirect()
            {
                Bearing = args.Bearing,
                IsOwn = args.IsOwn,
                DistanceKM = args.DistanceKM,
                Level = args.Level,
                NumberASP = args.NumberASP,
                Std = args.Std
            };
        }

        internal static ModelsTablesDBLib.JamDirect ConvertToDatabaseType(this TransmissionPackage.JamDirect args)
        {
            return new ModelsTablesDBLib.JamDirect()
            {
                Bearing = args.Bearing,
                IsOwn = args.IsOwn,
                DistanceKM = args.DistanceKM,
                Level = (short)args.Level,
                NumberASP = args.NumberASP,
                Std = args.Std
            };
        }

        internal static TransmissionPackage.Message ConvertToProtoType(this DataTransferModel.DataTransfer.Message args)
        {
            return new TransmissionPackage.Message()
            {
                ReceiverId = args.ReceiverId,
                SenderId = args.SenderId,
                Text = args.Text ?? ""
            };
        }

        internal static DataTransferModel.DataTransfer.Message ConvertToDataTransferType(this TransmissionPackage.Message args)
        {
            return new DataTransferModel.DataTransfer.Message(
                senderId: args.SenderId,
                receiverId: args.ReceiverId,
                text: args.Text);
        }

        internal static TransmissionPackage.Coord ConvertToProtoType(this ModelsTablesDBLib.Coord args)
        {
            return new TransmissionPackage.Coord()
            {
                Altitude = args.Altitude,
                Latitude = args.Latitude,
                Longitude = args.Longitude
            };
        }

        internal static ModelsTablesDBLib.Coord ConvertToDatabaseType(this TransmissionPackage.Coord args)
        {
            return new ModelsTablesDBLib.Coord()
            {
                Altitude = args.Altitude,
                Latitude = args.Latitude,
                Longitude = args.Longitude
            };
        }

        internal static TransmissionPackage.AspTable ConvertToProtoType(this DataTransferModel.DataTransfer.AspTable args)
        {
            return new TransmissionPackage.AspTable()
            {
                Mode = args.Mode,
                StationId = args.StationId,
                Coordinates = args.Coordinates.ConvertToProtoType(),
                Compasses = new TransmissionPackage.CompassesValues() 
                {
                    Lpa13 = args.Compasses.Lpa13,
                    Lpa24 = args.Compasses.Lpa24,
                    Lpa510 = args.Compasses.Lpa510,
                    Rrs1 = args.Compasses.Rrs1,
                    Rrs2 = args.Compasses.Rrs2,
                    BPSS = args.Compasses.BPSS,
                    LPA57 = args.Compasses.LPA57,
                    LPA59 = args.Compasses.LPA59,
                    LPA10 = args.Compasses.LPA10
                }
            };
        }

        internal static DataTransferModel.DataTransfer.AspTable ConvertToDataTransferType(this TransmissionPackage.AspTable args)
        {
            var compassesValues = new DataTransferModel.DataTransfer.CompassesValues(
                lpa13: (short)args.Compasses.Lpa13,
                lpa24: (short)args.Compasses.Lpa24,
                lpa510: (short)args.Compasses.Lpa510,
                rrs1: (short)args.Compasses.Rrs1,
                rrs2: (short)args.Compasses.Rrs2,
                bpss: (short)args.Compasses.BPSS,
                lpa57: (short)args.Compasses.LPA57,
                lpa59: (short)args.Compasses.LPA59,
                lpa10: (short)args.Compasses.LPA10);
            return new DataTransferModel.DataTransfer.AspTable(
                mode: (byte)args.Mode,
                stationId: args.StationId,
                coordinates: args.Coordinates.ConvertToDatabaseType(),
                compasses: compassesValues);
        }

        internal static ModelsTablesDBLib.TableSectorsRangesRecon ConvertToDatabaseType(this TransmissionPackage.TableSectorsRangesRecon args)
        {
            return new ModelsTablesDBLib.TableSectorsRangesRecon()
            {
                NumberASP = args.NumberASP,
                FreqMinKHz = args.FreqMinKHz,
                FreqMaxKHz = args.FreqMaxKHz,
                AngleMax = (short)args.AngleMax,
                AngleMin = (short)args.AngleMin,
                Note = args.Note ?? "",
                IsCheck = args.IsCheck
            };
        }

        internal static TransmissionPackage.TableSectorsRangesRecon ConvertToProtoType(this ModelsTablesDBLib.TableSectorsRangesRecon args)
        {
            return new TransmissionPackage.TableSectorsRangesRecon()
            {
                NumberASP = args.NumberASP,
                FreqMinKHz = args.FreqMinKHz,
                FreqMaxKHz = args.FreqMaxKHz,
                AngleMax = args.AngleMax,
                AngleMin = args.AngleMin,
                Note = args.Note ?? "",
                IsCheck = args.IsCheck
            };
        }

        internal static ModelsTablesDBLib.TableSectorsRangesSuppr ConvertToDatabaseType(this TransmissionPackage.TableSectorsRangesSuppr args)
        {
            return new ModelsTablesDBLib.TableSectorsRangesSuppr()
            {
                NumberASP = args.NumberASP,
                FreqMinKHz = args.FreqMinKHz,
                FreqMaxKHz = args.FreqMaxKHz,
                AngleMax = (short)args.AngleMax,
                AngleMin = (short)args.AngleMin,
                Note = args.Note ?? "",
                IsCheck = args.IsCheck
            };
        }

        internal static TransmissionPackage.TableSectorsRangesSuppr ConvertToProtoType(this ModelsTablesDBLib.TableSectorsRangesSuppr args)
        {
            return new TransmissionPackage.TableSectorsRangesSuppr()
            {
                NumberASP = args.NumberASP,
                FreqMinKHz = args.FreqMinKHz,
                FreqMaxKHz = args.FreqMaxKHz,
                AngleMax = args.AngleMax,
                AngleMin = args.AngleMin,
                Note = args.Note ?? "",
                IsCheck = args.IsCheck
            };
        }

        internal static System.DateTime ConvertToSystemType(this TransmissionPackage.DateTime args)
        {
            System.DateTime date;
            try 
            {
                date = new System.DateTime(args.Year, args.Month, args.Day, args.Hour, args.Minute, args.Second, args.Millisecond);
            }
            catch (System.ArgumentOutOfRangeException e)
            {
                //if we have an incorrect date
                date = new System.DateTime();
            }
            return date;
        }
            

        internal static TransmissionPackage.DateTime ConvertToProtoType(this System.DateTime args)
        {
            return new TransmissionPackage.DateTime()
            {
                Year = args.Year,
                Month = args.Month,
                Day = args.Day,
                Hour = args.Hour,
                Minute = args.Minute,
                Second = args.Second,
                Millisecond = args.Millisecond
            };
        }

        internal static ModelsTablesDBLib.TableJamDirect ConvertToDatabaseType(this TransmissionPackage.TableJamDirect args) =>
            new ModelsTablesDBLib.TableJamDirect() 
            {
                JamDirect = args.JamDirect.ConvertToDatabaseType()
            };

        internal static TransmissionPackage.TableJamDirect ConvertToProtoType(this ModelsTablesDBLib.TableJamDirect args) =>
            new TransmissionPackage.TableJamDirect()
            {
                JamDirect = args.JamDirect.ConvertToProtoType()
            };

        internal static ModelsTablesDBLib.TableReconFWS ConvertToDatabaseType(this TransmissionPackage.TableReconFWS args)
        {
            var listJamDirect = new System.Collections.ObjectModel.ObservableCollection<ModelsTablesDBLib.TableJamDirect>();
            foreach (var record in args.ListJamDirect)
            {
                listJamDirect.Add(record.ConvertToDatabaseType());
            }
            return new ModelsTablesDBLib.TableReconFWS()
            {
                FreqKHz = args.FreqKHz,
                Deviation = args.Deviation,
                Coordinates = args.Coordinates.ConvertToDatabaseType(),
                Time = args.Time.ConvertToSystemType(),
                ASPSuppr = args.ASPSuppr,
                Type = (byte)args.Type,
                ListJamDirect = listJamDirect,
                Sender = (SignSender?)args.Sender,
                Modulation = (ModulationKondor)args.Modulation
            };
        }

        internal static TransmissionPackage.TableReconFWS ConvertToProtoType(this ModelsTablesDBLib.TableReconFWS args) 
        {
            var output = new TransmissionPackage.TableReconFWS()
            {
                FreqKHz = args.FreqKHz,
                Deviation = args.Deviation,
                Coordinates = args.Coordinates.ConvertToProtoType(),
                Time = args.Time.ConvertToProtoType(),
                ASPSuppr = args.ASPSuppr,
                Type = args.Type,
                Sender = (int?)args.Sender,
                Modulation = (TransmissionPackage.TableReconFWS.Types.ModulationKondor)args.Modulation
            };

            output.ListJamDirect.AddRange(args.ListJamDirect.Select(r => r.ConvertToProtoType()));
            return output;
        }

        internal static ModelsTablesDBLib.TableReconFHSS ConvertToDatabaseType(this TransmissionPackage.TableReconFHSS args) 
        {
            return new ModelsTablesDBLib.TableReconFHSS() 
            {
                FreqMinKHz = args.FreqMinKHz,
                FreqMaxKHz = args.FreqMaxKHz,
                Deviation = args.Deviation,
                Modulation = (byte)args.Modulation,
                Time = args.Time.ConvertToSystemType(),
                StepKHz = (short)args.StepKHz,
                ImpulseDuration = (ushort)args.ImpulseDuration,
                QuantitySignal = (short)args.QuantitySignal,
                IsSelected = args.IsSelected
            };
        }

        internal static TransmissionPackage.TableReconFHSS ConvertToProtoType(this ModelsTablesDBLib.TableReconFHSS args)
        {
            return new TransmissionPackage.TableReconFHSS()
            {
                FreqMinKHz = args.FreqMinKHz,
                FreqMaxKHz = args.FreqMaxKHz,
                Deviation = args.Deviation,
                Modulation = args.Modulation,
                Time = args.Time.ConvertToProtoType(),
                StepKHz = args.StepKHz,
                ImpulseDuration = args.ImpulseDuration,
                QuantitySignal = args.QuantitySignal,
                IsSelected = args.IsSelected ?? false
            };
        }

        internal static ModelsTablesDBLib.TableSuppressFWS ConvertToDatabaseType(this TransmissionPackage.TableSuppressFWS args) 
        {
            return new ModelsTablesDBLib.TableSuppressFWS() 
            {
                NumberASP = args.NumberASP,
                Coordinates = args.Coordinates.ConvertToDatabaseType(),
                Sender = (ModelsTablesDBLib.SignSender) args.Sender,
                FreqKHz = args.FreqKHz,
                Bearing = args.Bearing,
                Letter = (byte)args.Letter,
                Threshold = (short)args.Threshold,
                Priority = (byte)args.Priority,
                InterferenceParam = args.InterferenceParam.ConvertToDatabaseType()
            };
        }

        internal static TransmissionPackage.TableSuppressFWS ConvertToProtoType(this ModelsTablesDBLib.TableSuppressFWS args)
        {
            return new TransmissionPackage.TableSuppressFWS()
            {
                NumberASP = args.NumberASP,
                Coordinates = args.Coordinates.ConvertToProtoType(),
                Sender = (TransmissionPackage.SignSender)(args.Sender ?? ModelsTablesDBLib.SignSender.PC),
                FreqKHz = args.FreqKHz ?? -1,
                Bearing = args.Bearing ?? -1,
                Letter = args.Letter ?? -1,
                Threshold = args.Threshold ?? -1,
                Priority = args.Priority ?? -1,
                InterferenceParam = args.InterferenceParam.ConvertToProtoType()
            };
        }

        internal static ModelsTablesDBLib.TableSuppressFHSS ConvertToDatabaseType(this TransmissionPackage.TableSuppressFHSS args) 
        {
            return new ModelsTablesDBLib.TableSuppressFHSS() 
            {
                NumberASP = args.NumberASP,
                FreqMinKHz = args.FreqMinKHz,
                FreqMaxKHz = args.FreqMaxKHz,
                Threshold = args.Threshold,
                StepKHz = (short)args.StepKHz,
                InterferenceParam = args.InterferenceParam.ConvertToDatabaseType(),
                Letters = args.Letters.ToArray(),
                EPO = args.EPO.ToArray()
            };
        }

        internal static TransmissionPackage.TableSuppressFHSS ConvertToProtoType(this ModelsTablesDBLib.TableSuppressFHSS args)
        {
            return new TransmissionPackage.TableSuppressFHSS()
            {
                NumberASP = args.NumberASP,
                FreqMinKHz = args.FreqMinKHz,
                FreqMaxKHz = args.FreqMaxKHz,
                Threshold = args.Threshold,
                StepKHz = args.StepKHz,
                InterferenceParam = args.InterferenceParam.ConvertToProtoType(),
                Letters = Google.Protobuf.ByteString.CopyFrom(args.Letters),
                EPO = Google.Protobuf.ByteString.CopyFrom(args.EPO)
            };
        }

        internal static ModelsTablesDBLib.TableFHSSExcludedFreq ConvertToDatabaseType(this TransmissionPackage.TableFHSSExcludedFreq args) 
        {
            return new ModelsTablesDBLib.TableFHSSExcludedFreq() 
            {
                IdFHSS = args.IdFHSS,
                FreqKHz = args.FreqKHz,
                Deviation = args.Deviation
            };
        }

        internal static TransmissionPackage.TableFHSSExcludedFreq ConvertToProtoType(this ModelsTablesDBLib.TableFHSSExcludedFreq args)
        {
            return new TransmissionPackage.TableFHSSExcludedFreq()
            {
                IdFHSS = args.IdFHSS,
                FreqKHz = args.FreqKHz,
                Deviation = args.Deviation
            };
        }

        internal static ModelsTablesDBLib.TableFreqImportant ConvertToDatabaseType(this TransmissionPackage.TableFreqImportant args) =>
            new ModelsTablesDBLib.TableFreqImportant()
            {
                NumberASP = args.NumberASP,
                FreqMinKHz = args.FreqMinKHz,
                FreqMaxKHz = args.FreqMaxKHz,
                Note = args.Note ?? ""
            };

        internal static TransmissionPackage.TableFreqImportant ConvertToProtoType(this ModelsTablesDBLib.TableFreqImportant args) =>
            new TransmissionPackage.TableFreqImportant()
            {
                NumberASP = args.NumberASP,
                FreqMinKHz = args.FreqMinKHz,
                FreqMaxKHz = args.FreqMaxKHz,
                Note = args.Note ?? ""
            };

        internal static ModelsTablesDBLib.TableFreqKnown ConvertToDatabaseType(this TransmissionPackage.TableFreqKnown args) =>
            new ModelsTablesDBLib.TableFreqKnown()
            {
                NumberASP = args.NumberASP,
                FreqMinKHz = args.FreqMinKHz,
                FreqMaxKHz = args.FreqMaxKHz,
                Note = args.Note ?? ""
            };

        internal static TransmissionPackage.TableFreqKnown ConvertToProtoType(this ModelsTablesDBLib.TableFreqKnown args) =>
            new TransmissionPackage.TableFreqKnown()
            {
                NumberASP = args.NumberASP,
                FreqMinKHz = args.FreqMinKHz,
                FreqMaxKHz = args.FreqMaxKHz,
                Note = args.Note ?? ""
            };

        internal static ModelsTablesDBLib.TableFreqForbidden ConvertToDatabaseType(this TransmissionPackage.TableFreqForbidden args) =>
            new ModelsTablesDBLib.TableFreqForbidden()
            {
                NumberASP = args.NumberASP,
                FreqMinKHz = args.FreqMinKHz,
                FreqMaxKHz = args.FreqMaxKHz,
                Note = args.Note ?? ""
            };

        internal static TransmissionPackage.TableFreqForbidden ConvertToProtoType(this ModelsTablesDBLib.TableFreqForbidden args) =>
            new TransmissionPackage.TableFreqForbidden()
            {
                NumberASP = args.NumberASP,
                FreqMinKHz = args.FreqMinKHz,
                FreqMaxKHz = args.FreqMaxKHz,
                Note = args.Note ?? ""
            };

        internal static DataTransferModel.Trasmission.TableEventArgs<ModelsTablesDBLib.TableSectorsRangesRecon> ConvertToDataTransferType(
            this TransmissionPackage.TableSectorsRangesReconTable args) 
        {
            var table = args.Table.Select(r => r.ConvertToDatabaseType()).ToArray();
            var output = new DataTransferModel.Trasmission.TableEventArgs<ModelsTablesDBLib.TableSectorsRangesRecon>(args.SenderStationId, table);
            return output;
        }

        internal static TransmissionPackage.TableSectorsRangesReconTable ConvertToProtoType(
            this DataTransferModel.Trasmission.TableEventArgs<ModelsTablesDBLib.TableSectorsRangesRecon> args)
        {
            var output = new TransmissionPackage.TableSectorsRangesReconTable() {SenderStationId = args.SenderStationId};
            output.Table.AddRange(args.Table.Select(r => r.ConvertToProtoType()));
            return output;
        }

        internal static DataTransferModel.Trasmission.TableEventArgs<ModelsTablesDBLib.TableSectorsRangesSuppr> ConvertToDataTransferType(
            this TransmissionPackage.TableSectorsRangesSupprTable args)
        {
            var table = args.Table.Select(r => r.ConvertToDatabaseType()).ToArray();
            var output = new DataTransferModel.Trasmission.TableEventArgs<ModelsTablesDBLib.TableSectorsRangesSuppr>(args.SenderStationId, table);
            return output;
        }

        internal static TransmissionPackage.TableSectorsRangesSupprTable ConvertToProtoType(
            this DataTransferModel.Trasmission.TableEventArgs<ModelsTablesDBLib.TableSectorsRangesSuppr> args)
        {
            var output = new TransmissionPackage.TableSectorsRangesSupprTable() { SenderStationId = args.SenderStationId };
            output.Table.AddRange(args.Table.Select(r => r.ConvertToProtoType()));
            return output;
        }

        internal static DataTransferModel.Trasmission.TableEventArgs<ModelsTablesDBLib.TableReconFWS> ConvertToDataTransferType(
            this TransmissionPackage.TableReconFWSTable args)
        {
            var table = args.Table.Select(r => r.ConvertToDatabaseType()).ToArray();
            var output = new DataTransferModel.Trasmission.TableEventArgs<ModelsTablesDBLib.TableReconFWS>(args.SenderStationId, table);
            return output;
        }

        internal static TransmissionPackage.TableReconFWSTable ConvertToProtoType(
            this DataTransferModel.Trasmission.TableEventArgs<ModelsTablesDBLib.TableReconFWS> args)
        {
            var output = new TransmissionPackage.TableReconFWSTable() { SenderStationId = args.SenderStationId };
            output.Table.AddRange(args.Table.Select(r => r.ConvertToProtoType()));
            return output;
        }

        internal static DataTransferModel.Trasmission.TableEventArgs<ModelsTablesDBLib.TableReconFHSS> ConvertToDataTransferType(
            this TransmissionPackage.TableReconFHSSTable args)
        {
            var table = args.Table.Select(r => r.ConvertToDatabaseType()).ToArray();
            var output = new DataTransferModel.Trasmission.TableEventArgs<ModelsTablesDBLib.TableReconFHSS>(args.SenderStationId, table);
            return output;
        }

        internal static TransmissionPackage.TableReconFHSSTable ConvertToProtoType(
            this DataTransferModel.Trasmission.TableEventArgs<ModelsTablesDBLib.TableReconFHSS> args)
        {
            var output = new TransmissionPackage.TableReconFHSSTable() { SenderStationId = args.SenderStationId };
            output.Table.AddRange(args.Table.Select(r => r.ConvertToProtoType()));
            return output;
        }

        internal static DataTransferModel.Trasmission.TableEventArgs<ModelsTablesDBLib.TableSuppressFWS> ConvertToDataTransferType(
            this TransmissionPackage.TableSuppressFWSTable args)
        {
            var table = args.Table.Select(r => r.ConvertToDatabaseType()).ToArray();
            var output = new DataTransferModel.Trasmission.TableEventArgs<ModelsTablesDBLib.TableSuppressFWS>(args.SenderStationId, table);
            return output;
        }

        internal static TransmissionPackage.TableSuppressFWSTable ConvertToProtoType(
            this DataTransferModel.Trasmission.TableEventArgs<ModelsTablesDBLib.TableSuppressFWS> args)
        {
            var output = new TransmissionPackage.TableSuppressFWSTable() { SenderStationId = args.SenderStationId };
            output.Table.AddRange(args.Table.Select(r => r.ConvertToProtoType()));
            return output;
        }

        internal static DataTransferModel.Trasmission.TableEventArgs<ModelsTablesDBLib.TableSuppressFHSS> ConvertToDataTransferType(
            this TransmissionPackage.TableSuppressFHSSTable args)
        {
            var table = args.Table.Select(r => r.ConvertToDatabaseType()).ToArray();
            var output = new DataTransferModel.Trasmission.TableEventArgs<ModelsTablesDBLib.TableSuppressFHSS>(args.SenderStationId, table);
            return output;
        }

        internal static TransmissionPackage.TableSuppressFHSSTable ConvertToProtoType(
            this DataTransferModel.Trasmission.TableEventArgs<ModelsTablesDBLib.TableSuppressFHSS> args)
        {
            var output = new TransmissionPackage.TableSuppressFHSSTable() { SenderStationId = args.SenderStationId };
            output.Table.AddRange(args.Table.Select(r => r.ConvertToProtoType()));
            return output;
        }

        internal static DataTransferModel.Trasmission.TableEventArgs<ModelsTablesDBLib.TableFHSSExcludedFreq> ConvertToDataTransferType(
            this TransmissionPackage.TableFHSSExcludedFreqTable args)
        {
            var table = args.Table.Select(r => r.ConvertToDatabaseType()).ToArray();
            var output = new DataTransferModel.Trasmission.TableEventArgs<ModelsTablesDBLib.TableFHSSExcludedFreq>(args.SenderStationId, table);
            return output;
        }

        internal static TransmissionPackage.TableFHSSExcludedFreqTable ConvertToProtoType(
            this DataTransferModel.Trasmission.TableEventArgs<ModelsTablesDBLib.TableFHSSExcludedFreq> args)
        {
            var output = new TransmissionPackage.TableFHSSExcludedFreqTable() { SenderStationId = args.SenderStationId };
            output.Table.AddRange(args.Table.Select(r => r.ConvertToProtoType()));
            return output;
        }

        internal static DataTransferModel.Trasmission.TableEventArgs<ModelsTablesDBLib.TableFreqImportant> ConvertToDataTransferType(
            this TransmissionPackage.TableFreqImportantTable args)
        {
            var table = args.Table.Select(r => r.ConvertToDatabaseType()).ToArray();
            var output = new DataTransferModel.Trasmission.TableEventArgs<ModelsTablesDBLib.TableFreqImportant>(args.SenderStationId, table);
            return output;
        }

        internal static TransmissionPackage.TableFreqImportantTable ConvertToProtoType(
            this DataTransferModel.Trasmission.TableEventArgs<ModelsTablesDBLib.TableFreqImportant> args)
        {
            var output = new TransmissionPackage.TableFreqImportantTable() { SenderStationId = args.SenderStationId };
            output.Table.AddRange(args.Table.Select(r => r.ConvertToProtoType()));
            return output;
        }

        internal static DataTransferModel.Trasmission.TableEventArgs<ModelsTablesDBLib.TableFreqForbidden> ConvertToDataTransferType(
            this TransmissionPackage.TableFreqForbiddenTable args)
        {
            var table = args.Table.Select(r => r.ConvertToDatabaseType()).ToArray();
            var output = new DataTransferModel.Trasmission.TableEventArgs<ModelsTablesDBLib.TableFreqForbidden>(args.SenderStationId, table);
            return output;
        }

        internal static TransmissionPackage.TableFreqForbiddenTable ConvertToProtoType(
            this DataTransferModel.Trasmission.TableEventArgs<ModelsTablesDBLib.TableFreqForbidden> args)
        {
            var output = new TransmissionPackage.TableFreqForbiddenTable() { SenderStationId = args.SenderStationId };
            output.Table.AddRange(args.Table.Select(r => r.ConvertToProtoType()));
            return output;
        }

        internal static DataTransferModel.Trasmission.TableEventArgs<ModelsTablesDBLib.TableFreqKnown> ConvertToDataTransferType(
            this TransmissionPackage.TableFreqKnownTable args)
        {
            var table = args.Table.Select(r => r.ConvertToDatabaseType()).ToArray();
            var output = new DataTransferModel.Trasmission.TableEventArgs<ModelsTablesDBLib.TableFreqKnown>(args.SenderStationId, table);
            return output;
        }

        internal static TransmissionPackage.TableFreqKnownTable ConvertToProtoType(
            this DataTransferModel.Trasmission.TableEventArgs<ModelsTablesDBLib.TableFreqKnown> args)
        {
            var output = new TransmissionPackage.TableFreqKnownTable() { SenderStationId = args.SenderStationId };
            output.Table.AddRange(args.Table.Select(r => r.ConvertToProtoType()));
            return output;
        }

        internal static DataTransferModel.DataTransfer.ExecutiveDfRequestArgs ConvertToDataTransferType(this TransmissionPackage.ExecutiveDfRequestArgs args) =>
            new DataTransferModel.DataTransfer.ExecutiveDfRequestArgs(
                stationId: args.StationId, 
                startFrequencyKhz: args.StartFrequencyKhz, 
                endFrequencyKhz: args.EndFrequencyKhz, 
                phaseAveragingCount: args.PhaseAveragingCount, 
                directionAveragingCount: args.DirectionAveragingCount);

        internal static TransmissionPackage.ExecutiveDfRequestArgs ConvertToProtoType(this DataTransferModel.DataTransfer.ExecutiveDfRequestArgs args) =>
            new TransmissionPackage.ExecutiveDfRequestArgs()
            {
                StartFrequencyKhz = args.StartFrequencyKhz,
                StationId = args.StationId,
                EndFrequencyKhz = args.EndFrequencyKhz,
                PhaseAveragingCount = args.PhaseAveragingCount,
                DirectionAveragingCount = args.DirectionAveragingCount
            };

        internal static DataTransferModel.DataTransfer.ExecutiveDfResponseArgs ConvertToDataTransferType(this TransmissionPackage.ExecutiveDfResponseArgs args) =>
            new DataTransferModel.DataTransfer.ExecutiveDfResponseArgs(
                frequencyKhz: args.FrequencyKhz,
                direction: args.Direction,
                stationId: args.StationId);

        internal static TransmissionPackage.ExecutiveDfResponseArgs ConvertToProtoType(this DataTransferModel.DataTransfer.ExecutiveDfResponseArgs args) =>
            new TransmissionPackage.ExecutiveDfResponseArgs()
            {
                Direction = args.Direction,
                StationId = args.StationId,
                FrequencyKhz = args.FrequencyKhz
            };

        internal static DataTransferModel.DataTransfer.Signal ConvertToDataTransferType(this TransmissionPackage.Signal args) 
        {
            var time = args.BroadcastTimeSpan.ConvertToSystemType();
            return new DataTransferModel.DataTransfer.Signal() 
            {
                FrequencyKhz = args.FrequencyKhz,
                CentralFrequencyKhz = args.CentralFrequencyKhz,
                BandwidthKhz = args.BandwidthKhz,
                Direction = args.Direction,
                Amplitude = args.Amplitude,
                StandardDeviation = args.StandardDeviation,
                Altitude = args.Altitude,
                Latitude = args.Latitude,
                Longitude = args.Longitude,
                DiscardedDirectionsPart = args.DiscardedDirectionsPart,
                PhaseDeviation = args.PhaseDeviation,
                RelativeSubScanCount = args.RelativeSubScanCount,
                BroadcastTimeSpan = new System.TimeSpan(time.Day, time.Hour, time.Minute, time.Second, time.Millisecond)
            };
        }

        internal static TransmissionPackage.Signal ConvertToProtoType(this DataTransferModel.DataTransfer.Signal args)
        {
            System.DateTime time;
            try
            {
                time = new System.DateTime(
                System.DateTime.UtcNow.Year,
                args.BroadcastTimeSpan.Days,
                args.BroadcastTimeSpan.Hours,
                args.BroadcastTimeSpan.Minutes,
                args.BroadcastTimeSpan.Seconds,
                args.BroadcastTimeSpan.Milliseconds);
            }
            catch (System.ArgumentOutOfRangeException e)
            {
                //if we have an incorrect date
                time = new System.DateTime();
            }
            return new TransmissionPackage.Signal()
            {
                FrequencyKhz = args.FrequencyKhz,
                CentralFrequencyKhz = args.CentralFrequencyKhz,
                BandwidthKhz = args.BandwidthKhz,
                Direction = args.Direction,
                Amplitude = args.Amplitude,
                StandardDeviation = args.StandardDeviation,
                Altitude = args.Altitude,
                Latitude = args.Latitude,
                Longitude = args.Longitude,
                DiscardedDirectionsPart = args.DiscardedDirectionsPart,
                PhaseDeviation = args.PhaseDeviation,
                RelativeSubScanCount = args.RelativeSubScanCount,
                BroadcastTimeSpan = time.ConvertToProtoType()
            };
        }

        internal static DataTransferModel.DataTransfer.RdfCycleResult ConvertToDataTransferType(this TransmissionPackage.RdfCycleResult args) 
        {
            return new DataTransferModel.DataTransfer.RdfCycleResult() 
            {
                StationId = args.StationId,
                BandNumber = args.BandNumber,
                Signals = args.Signals.Select(r => r.ConvertToDataTransferType()).ToArray()
            };
        }
        internal static TransmissionPackage.RdfCycleResult ConvertToProtoType(this DataTransferModel.DataTransfer.RdfCycleResult args)
        {
            var output = new TransmissionPackage.RdfCycleResult()
            {
                StationId = args.StationId,
                BandNumber = args.BandNumber,
            };
            output.Signals.AddRange(args.Signals.Select(r => r.ConvertToProtoType()));
            return output;
        }

        internal static DataTransferModel.DataTransfer.FhssCycleResult ConvertToDataTransferType(this TransmissionPackage.FhssCycleResult args) => 
            new DataTransferModel.DataTransfer.FhssCycleResult(args.StationId, args.Networks.Select(ConvertToDataTransferType));

        internal static TransmissionPackage.FhssCycleResult ConvertToProtoType(this DataTransferModel.DataTransfer.FhssCycleResult args) 
        {
            var output = new TransmissionPackage.FhssCycleResult()
            {
                StationId = args.StationId
            };
            output.Networks.AddRange(args.Networks.Select(ConvertToProtoType));
            return output;
        }

        internal static DataTransferModel.DataTransfer.FhssNetwork ConvertToDataTransferType(this TransmissionPackage.FhssNetwork args) 
        {
            return new DataTransferModel.DataTransfer.FhssNetwork(
                frequencyMihKhz: args.FrequencyMinKhz,
                frequencyMaxKhz: args.FrequencyMaxKhz,
                userInfo: args.UserInfo.Select(i => i.ConvertToDataTransferType()),
                altitude: args.Altitude,
                latitude: args.Latitude,
                longitude: args.Longitude);
        }

        internal static TransmissionPackage.FhssNetwork ConvertToProtoType(this DataTransferModel.DataTransfer.FhssNetwork args) 
        {
            var output = new TransmissionPackage.FhssNetwork()
            {
                FrequencyMinKhz = args.FrequencyMinKhz,
                FrequencyMaxKhz = args.FrequencyMaxKhz,
                Altitude = args.Altitude,
                Latitude = args.Latitude,
                Longitude = args.Longitude,
            };
            output.UserInfo.AddRange(args.UserInfo.Select(i => i.ConvertToProtoType()));
            return output;
        }

        internal static DataTransferModel.DataTransfer.FhssUserInfo ConvertToDataTransferType(this TransmissionPackage.FhssUserInfo args) =>
            new DataTransferModel.DataTransfer.FhssUserInfo(args.Amplitude, args.Direction, args.StandardDeviation);

        internal static TransmissionPackage.FhssUserInfo ConvertToProtoType(this DataTransferModel.DataTransfer.FhssUserInfo args) =>
            new TransmissionPackage.FhssUserInfo() 
            {
                StandardDeviation = args.StandardDeviation,
                Direction = args.Direction,
                Amplitude = args.Amplitude
            };
    }
}
