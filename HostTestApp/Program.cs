﻿using System;
using System.ServiceModel;
using DataTransferControllerLibrary;

namespace HostTestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var d = new DataTransferController();
            using (var host = new ServiceHost(d))
            {
                host.Open();
                d.OnLog += (sender, s) => Console.WriteLine(s);
                d.OnMessageReceived += (sender, s) => d.MessageReceived(s.SenderId, s.ReceiverId, s.Text);

                Console.WriteLine("hi");
                Console.ReadLine();
            }
        }
    }
}
