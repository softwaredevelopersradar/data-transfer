﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataTransferModel;
using DataTransferModel.Database;
using DataTransferModel.DataTransfer;
using Errors;
using ClientDataBase;
using ModelsTablesDBLib;
using InheritorsEventArgs;

namespace DatabaseControllerLibrary
{
    public partial class DatabaseController : IDatabaseController
    {
        public bool IsConnected { get; private set; }

        public IReadOnlyList<Station> LinkedStations => _linkedStations;

        public Station OwnStation { get; private set; } = new Station(-1, true, "", 0);
        public StationRole Role { get; private set; }

        private readonly ClientDB _clientDb;
        private readonly List<Station> _linkedStations;

        private readonly IClassTables LinkedStationsTable;

        public event EventHandler<string> OnDatabaseLog;
        public event EventHandler<string> OnDatabaseError;
        public event EventHandler<bool> OnDatabaseConnectionStateChanged;
        public event EventHandler<IReadOnlyList<int>> OnLinkedStationsConnectionSettingsChanged;
        public event EventHandler<AspTable> AspTableReceived;
        public event EventHandler<TableSectorsRangesRecon[]> TableSectorsRangesReconReceived;
        public event EventHandler<TableSectorsRangesSuppr[]> TableSectorsRangesSupprReceived;
        public event EventHandler<TableReconFWS[]> TableFrsReconReceived;
        public event EventHandler<TableReconFHSS[]> TableFhssReconReceived;
        public event EventHandler<TableSuppressFWS[]> TableFrsJamReceived;
        public event EventHandler<TableSuppressFHSS[]> TableFhssJamReceived;
        public event EventHandler<TableFHSSExcludedFreq[]> TableFhssExcludedFreqReceived;
        public event EventHandler<TableFreqImportant[]> TableFreqImportantReceived;
        public event EventHandler<TableFreqForbidden[]> TableFreqForbiddenReceived;
        public event EventHandler<TableFreqKnown[]> TableFreqKnownReceived;
        public event EventHandler<bool> OnOwnStationValidData;

        public DatabaseController(string databaseHost, int databasePort)
        {
            _clientDb = new ClientDB("Data Transfer", $"{databaseHost}:{databasePort}");

            _linkedStations = new List<Station>();
            LinkedStationsTable = _clientDb.Tables[NameTable.TableASP];
            PeriodicTask.Run(ClearTickets, TimeSpan.FromSeconds(10));
        }

        public void Connect()
        {
            try
            {
                if (!IsConnected)
                {
                    SubscribeToDatabaseEvents();
                    _clientDb.Connect();
                }
            }
            catch (Exception e)
            {
                OnDatabaseError?.Invoke(this, $"Database connection error {e}");
            }
        }

        public void Disconnect()
        {
            try
            {
                if (IsConnected)
                {
                    UnsubscribeFromDatabaseEvents();
                    _clientDb.Disconnect();
                    OnDisconnect(this, new ClientEventArgs(EnumClientError.None, "manual")); //hack for no reconnect when manually disconnected
                }
            }
            catch (Exception e)
            {
                OnDatabaseError?.Invoke(this, $"Error while trying to disconnect from database {e}");
            }
        }

        private void LoadAllTables()
        {
            var aspTable = LinkedStationsTable.Load<TableASP>().ToList();
            OnLinkedStationReceived(this, new TableEventArs<TableASP>(aspTable));
            foreach (var table in aspTable) 
            {
                if (table.ISOwn)
                    continue;

                //clear previous database state of linked stations
                UpdateAspConnectionState(table.Id, false);
                UpdateAspTable(new AspTable(0, table.Id, table.Coordinates, new CompassesValues(-1,-1,-1,-1,-1, -1, -1, -1, -1)));
            }
        }

        public void LoadTablesForClient() 
        {
            var ownStation = LinkedStationsTable.Load<TableASP>().FirstOrDefault(s => s.ISOwn);
            if (ownStation == null)
            {
                OnDatabaseError?.Invoke(this, "Couldn't load asp table to send to client, cause there are no OWN station!");
                return;
            }
            var compassesValues = new CompassesValues(ownStation.LPA13, ownStation.LPA24, ownStation.LPA510, ownStation.RRS1, ownStation.RRS2, ownStation.BPSS, ownStation.LPA57, ownStation.LPA59, ownStation.LPA10);
            AspTableReceived?.Invoke(this, new AspTable(ownStation.Mode, ownStation.Id, ownStation.Coordinates, compassesValues));
            if (Role != StationRole.Master)
            {
                TableReconFWSUpdated(this, new TableEventArs<TableReconFWS>(
                        _clientDb.Tables[NameTable.TableReconFWS]
                        .Load<TableReconFWS>()));
                return;
            }

            TableSectorsRangesReconUpdated(this,
                new TableEventArs<TableSectorsRangesRecon>(
                    _clientDb.Tables[NameTable.TableSectorsRangesRecon]
                    .Load<TableSectorsRangesRecon>()));

            TableSectorsRangesSupprUpdated(this,
                new TableEventArs<TableSectorsRangesSuppr>(
                    _clientDb.Tables[NameTable.TableSectorsRangesSuppr]
                    .Load<TableSectorsRangesSuppr>()));

            TableSuppressFWSUpdated(this, new TableEventArs<TableSuppressFWS>(
                    _clientDb.Tables[NameTable.TableSuppressFWS]
                    .Load<TableSuppressFWS>()));

            TableSuppressFHSSUpdated(this, new TableEventArs<TableSuppressFHSS>(
                    _clientDb.Tables[NameTable.TableSuppressFHSS]
                    .Load<TableSuppressFHSS>()));

            TableFHSSExcludedFreqUpdated(this, new TableEventArs<TableFHSSExcludedFreq>(
                    _clientDb.Tables[NameTable.TableFHSSExcludedFreq]
                    .Load<TableFHSSExcludedFreq>()));

            TableFreqKnownUpdated(this, new TableEventArs<TableFreqKnown>(
                    _clientDb.Tables[NameTable.TableFreqKnown]
                    .Load<TableFreqKnown>()));

            TableFreqImportantUpdated(this, new TableEventArs<TableFreqImportant>(
                    _clientDb.Tables[NameTable.TableFreqImportant]
                    .Load<TableFreqImportant>()));

            TableFreqForbiddenUpdated(this, new TableEventArs<TableFreqForbidden>(
                    _clientDb.Tables[NameTable.TableFreqForbidden]
                    .Load<TableFreqForbidden>()));
        }

        private void SubscribeToDatabaseEvents()
        {
            _clientDb.OnConnect += OnConnect;
            _clientDb.OnDisconnect += OnDisconnect;
            _clientDb.OnErrorDataBase += OnError;

            (LinkedStationsTable as ITableUpdate<TableASP>).OnUpTable += OnLinkedStationReceived;
            (_clientDb.Tables[NameTable.TableSectorsRangesRecon] as ITableUpdate<TableSectorsRangesRecon>).OnUpTable += TableSectorsRangesReconUpdated;
            (_clientDb.Tables[NameTable.TableSectorsRangesSuppr] as ITableUpdate<TableSectorsRangesSuppr>).OnUpTable += TableSectorsRangesSupprUpdated;
            (_clientDb.Tables[NameTable.TableReconFWS] as ITableUpdate<TableReconFWS>).OnUpTable += TableReconFWSUpdated;
            //(_clientDb.Tables[NameTable.TableReconFHSS] as ITableUpdate<TableReconFHSS>).OnUpTable += TableReconFHSSUpdated;
            (_clientDb.Tables[NameTable.TableSuppressFWS] as ITableUpdate<TableSuppressFWS>).OnUpTable += TableSuppressFWSUpdated;
            (_clientDb.Tables[NameTable.TableSuppressFHSS] as ITableUpdate<TableSuppressFHSS>).OnUpTable += TableSuppressFHSSUpdated;
            (_clientDb.Tables[NameTable.TableFHSSExcludedFreq] as ITableUpdate<TableFHSSExcludedFreq>).OnUpTable += TableFHSSExcludedFreqUpdated;
            (_clientDb.Tables[NameTable.TableFreqImportant] as ITableUpdate<TableFreqImportant>).OnUpTable += TableFreqImportantUpdated;
            (_clientDb.Tables[NameTable.TableFreqForbidden] as ITableUpdate<TableFreqForbidden>).OnUpTable += TableFreqForbiddenUpdated;
            (_clientDb.Tables[NameTable.TableFreqKnown] as ITableUpdate<TableFreqKnown>).OnUpTable += TableFreqKnownUpdated;
        }

        private void UnsubscribeFromDatabaseEvents()
        {
            _clientDb.OnConnect -= OnConnect;
            _clientDb.OnDisconnect -= OnDisconnect;
            _clientDb.OnErrorDataBase -= OnError;

            (LinkedStationsTable as ITableUpdate<TableASP>).OnUpTable -= OnLinkedStationReceived;
            (_clientDb.Tables[NameTable.TableSectorsRangesRecon] as ITableUpdate<TableSectorsRangesRecon>).OnUpTable -= TableSectorsRangesReconUpdated;
            (_clientDb.Tables[NameTable.TableSectorsRangesSuppr] as ITableUpdate<TableSectorsRangesSuppr>).OnUpTable -= TableSectorsRangesSupprUpdated;
            (_clientDb.Tables[NameTable.TableReconFWS] as ITableUpdate<TableReconFWS>).OnUpTable -= TableReconFWSUpdated;
            //(_clientDb.Tables[NameTable.TableReconFHSS] as ITableUpdate<TableReconFHSS>).OnUpTable -= TableReconFHSSUpdated;
            (_clientDb.Tables[NameTable.TableSuppressFWS] as ITableUpdate<TableSuppressFWS>).OnUpTable -= TableSuppressFWSUpdated;
            (_clientDb.Tables[NameTable.TableSuppressFHSS] as ITableUpdate<TableSuppressFHSS>).OnUpTable -= TableSuppressFHSSUpdated;
            (_clientDb.Tables[NameTable.TableFHSSExcludedFreq] as ITableUpdate<TableFHSSExcludedFreq>).OnUpTable -= TableFHSSExcludedFreqUpdated;
            (_clientDb.Tables[NameTable.TableFreqImportant] as ITableUpdate<TableFreqImportant>).OnUpTable -= TableFreqImportantUpdated;
            (_clientDb.Tables[NameTable.TableFreqForbidden] as ITableUpdate<TableFreqForbidden>).OnUpTable -= TableFreqForbiddenUpdated;
            (_clientDb.Tables[NameTable.TableFreqKnown] as ITableUpdate<TableFreqKnown>).OnUpTable -= TableFreqKnownUpdated;
        }
        
        /// <summary>
        /// number of tickets equals to number of operations with database, usually two (clear and write)
        /// </summary>
        private readonly List<Ticket> _tickets = new List<Ticket>(40);


        public void UpdateFrsJamTable(TableSuppressFWS[] table)
        {
            RewriteTable(NameTable.TableSuppressFWS, table.ToList(), 2, "updating frs jam");
        }

        public void ActuallyUpdateFrsJamTable(int stationNumber, TableSuppressFWS[] table)
        {
            var currentTable = _clientDb.Tables[NameTable.TableSuppressFWS]
                .Load<TableSuppressFWS>()
                .Where(r => r.NumberASP != stationNumber)
                .ToList();
            currentTable.AddRange(table);
            RewriteTable(NameTable.TableSuppressFWS, currentTable, 2, "updating master frs jam");
        }

        public void UpdateFhssJamTable(TableSuppressFHSS[] table)
        {
            RewriteTable(NameTable.TableSuppressFHSS, table.ToList(), 2, "updating fhss jam");
        }

        public void ActuallyUpdateFhssJamTable(int stationNumber, TableSuppressFHSS[] table)
        {
            var currentTable = _clientDb.Tables[NameTable.TableSuppressFHSS]
                .Load<TableSuppressFHSS>()
                .Where(r => r.NumberASP != stationNumber)
                .ToList();
            currentTable.AddRange(table);
            RewriteTable(NameTable.TableSuppressFHSS, currentTable, 2, "updating master fhss jam");
        }


        public void UpdateSectorsRangesRecon(TableSectorsRangesRecon[] table)
        {
            RewriteTable(NameTable.TableSectorsRangesRecon, table.ToList(), 2, "updating sectors ranges recon");
        }

        public void UpdateSectorsRangesSuppr(TableSectorsRangesSuppr[] table)
        {
            RewriteTable(NameTable.TableSectorsRangesSuppr, table.ToList(), 2, "updating sectors ranges suppr");
        }
        

        public void UpdateFrsReconTable(TableReconFWS[] table)
        {
            RewriteTable(NameTable.TableReconFWS, table.ToList(), 2, "updating frs recon");
        }

        public void ActuallyUpdateFrsReconTable(int stationNumber, TableReconFWS[] table)
        {
            var currentTable = _clientDb.Tables[NameTable.TableReconFWS].Load<TableReconFWS>().ToList();
            foreach (var record in table)
            {
                if (currentTable.Any(
                    r => record.FreqKHz == r.FreqKHz 
                    && record.Time == r.Time))
                    continue;
                currentTable.Add(record);
            }
            RewriteTable(NameTable.TableReconFWS, currentTable, 2, "updating master frs recon");
        }

        public void UpdateFhssReconTable(TableReconFHSS[] table)
        {
            RewriteTable(NameTable.TableReconFHSS, table.ToList(), 2, "updating fhss recon");
        }

        public void ActuallyUpdateFhssReconTable(int stationNumber, TableReconFHSS[] table)
        {
            var currentTable = _clientDb.Tables[NameTable.TableReconFHSS]
                .Load<TableReconFHSS>()
                .Where(r => r.Id != stationNumber) //todo : here
                .ToList();
            currentTable.AddRange(table);

            RewriteTable(NameTable.TableReconFHSS, currentTable, 2, "updating master fhss recon");
        }

        public void UpdateForbiddenFrequenciesTable(TableFreqForbidden[] table)
        {
            RewriteTable(NameTable.TableFreqForbidden, table.ToList(), 2, "updating forbidden frequencies");
        }

        public void UpdateImportantFrequenciesTable(TableFreqImportant[] table)
        {
            RewriteTable(NameTable.TableFreqImportant, table.ToList(), 2, "updating important frequencies");
        }

        public void UpdateKnownFrequenciesTable(TableFreqKnown[] table)
        {
            RewriteTable(NameTable.TableFreqKnown, table.ToList(), 2, "updating known frequencies");
        }

        public void UpdateAspTable(AspTable table)
        {
            //other stations can't change our station info
            if (table.StationId == OwnStation.Id)
                return;

            var aspData = _clientDb.Tables[NameTable.TableASP]
                .Load<TableASP>()
                .FirstOrDefault(t => t.Id == table.StationId);

            if (aspData != null)
            {
                GenerateTickets(NameTable.TableASP, 1, $"updating asp table, mode : {table.Mode}");
                //updating values
                aspData.Coordinates = table.Coordinates;
                aspData.Mode = table.Mode;
                aspData.LPA13 = table.Compasses.Lpa13;
                aspData.LPA24 = table.Compasses.Lpa24;
                aspData.LPA510 = table.Compasses.Lpa510;
                aspData.RRS1 = table.Compasses.Rrs1;
                aspData.RRS2 = table.Compasses.Rrs2;
                aspData.BPSS = table.Compasses.BPSS;
                aspData.LPA57 = table.Compasses.LPA57;
                aspData.LPA59 = table.Compasses.LPA59;
                aspData.LPA10 = table.Compasses.LPA10;

                _clientDb.Tables[NameTable.TableASP].Change(aspData);
            }
        }

        public void UpdateAspConnectionState(int stationId, bool isConnected)
        {
            var aspData = _clientDb.Tables[NameTable.TableASP]
                .Load<TableASP>()
                .FirstOrDefault(t => t.Id == stationId);

            if (aspData != null)
            {
                GenerateTickets(NameTable.TableASP, 1, $"updating asp table connection state : { isConnected}");
                //updating values
                aspData.IsConnect = isConnected ? Led.Green : Led.Empty;
                _clientDb.Tables[NameTable.TableASP].Change(aspData);
            }
        }

        /// <summary>
        /// Clears expired tickets in a periodic task
        /// </summary>
        private void ClearTickets()
        {
            if (_tickets.Count == 0)
                return;

            _tickets.RemoveAll(t => t.IsExpired());
            OnDatabaseLog?.Invoke(this, "cleared tickets");
        }

        private void GenerateTickets(NameTable table, int count, string message)
        {
            _tickets.AddRange(
                Enumerable.Repeat(
                    element: new Ticket(table),
                    count: count));
            OnDatabaseLog?.Invoke(this, message);
        }

        private void RewriteTable(NameTable table, object updateTable)
        {
            _clientDb.Tables[table].Clear();
            _clientDb.Tables[table].AddRange(updateTable);
        }

        /// <summary>
        /// Rewrites table with new values and generates tickets for operation
        /// </summary>
        private void RewriteTable(NameTable table, object updatedTable, int ticketCount, string logMessage)
        {
            GenerateTickets(table, ticketCount, logMessage);
            RewriteTable(table, updatedTable);
        }
    }
}
