using System.Collections.Generic;
using InheritorsEventArgs;
using ModelsTablesDBLib;
using System.Linq;
using DataTransferModel.Database;
using DataTransferModel.DataTransfer;
using System.Threading.Tasks;

namespace DatabaseControllerLibrary
{
    public partial class DatabaseController
    {
        private int _reconnectionAttemptsCount = 5; //todo : to constants
        private readonly object _reconnectLockObject = new object();

        private void OnConnect(object sender, ClientEventArgs args)
        {
            IsConnected = true;
            _reconnectionAttemptsCount = 5;
            OnDatabaseLog?.Invoke(this, $"Connected to database {_clientDb.NameEndPoint}");
            OnDatabaseConnectionStateChanged?.Invoke(this, IsConnected);
            Task.Run(() => LoadAllTables());
        }

        private void OnDisconnect(object sender, ClientEventArgs args)
        {
            IsConnected = false;
            UnsubscribeFromDatabaseEvents();
            if(args.GetMessage.Split(' ').Last().Equals("manual") == false)//hack
            {
                Task.Run(Reconnect);
                OnDatabaseLog?.Invoke(this, $"Disconnected from database, trying to reconnect");
            }
            else
            {
                OnDatabaseLog?.Invoke(this, $"Disconnected from database");
            }
            OnDatabaseConnectionStateChanged?.Invoke(this, IsConnected);
        }

        private async Task Reconnect()
        {
            lock(_reconnectLockObject)
            {
                while (_reconnectionAttemptsCount > 0 && !IsConnected)
                {
                    _reconnectionAttemptsCount--;
                    Connect();
                };

                if (!IsConnected)
                    OnDatabaseError?.Invoke(this, "Couldn't reconnect to database");
            }
        }

        private void OnError(object sender, OperationTableEventArgs args)
        {
            OnDatabaseError?.Invoke(this, $"Database error : {args.TypeError}, " +
                                          $"table name : {args.TableName}, " +
                                          $"operation : {args.Operation}, " +
                                          $"message : {args.GetMessage}" 
                                          );
        }

        private AspTable _lastStationUpdate = new AspTable();
        private bool _isOwnStationValidData;

        private void OnLinkedStationReceived(object sender, TableEventArs<TableASP> args)
        {
            var asps = new List<TableASP>(args.Table);
            if (CheckTicket(NameTable.TableASP))
                return;

            if (asps.Count != 0)
            {
                var own = asps.FirstOrDefault(station => station.ISOwn == true);
                
                if (own == null)
                {
                    _isOwnStationValidData = false;
                    OnOwnStationValidData?.Invoke(this, _isOwnStationValidData);
                    return;
                }
               

                var compassesValues = new CompassesValues(own.LPA13, own.LPA24, own.LPA510, own.RRS1, own.RRS2, own.BPSS, own.LPA57, own.LPA59, own.LPA10);
                var updatedTable = new AspTable(
                    mode: own.Mode,
                    stationId: own.Id,
                    coordinates: own.Coordinates,
                    compasses: compassesValues);

                //update values only when they've changed
                if (!_lastStationUpdate.Equals(updatedTable))
                {
                    OwnStation = new Station(own.Id, own.ISOwn, own.AddressIP, own.AddressPort, ConnectionType.None);
                    Role = (StationRole)own.Role;
                    _lastStationUpdate = updatedTable;
                    AspTableReceived?.Invoke(this, updatedTable);
                } 
                
               

                asps.Remove(own);

                var preChangesLinkedStations = _linkedStations.ToList();
                _linkedStations.Clear();

                _linkedStations.AddRange(
                    asps.Select(
                        station => new Station(
                            id: station.Id, 
                            isOwn: station.ISOwn, 
                            ipAddress: (station.AddressIP ?? "").Replace(',','.'), 
                            port: station.AddressPort, 
                            connectionType: station.TypeConnection.ParseToDataTransferType()
                            )));
                if (!_isOwnStationValidData)
                {
                    _isOwnStationValidData = true;
                    OnOwnStationValidData?.Invoke(this, _isOwnStationValidData);
                }
                //if (_linkedStations.Count != preChangesLinkedStations.Count)
                //    OnLinkedStationsConnectionSettingsChanged?.Invoke(this, _linkedStations.Select(s => s.Id).ToList()); //todo : refactor

                //OnStartReconnection?.Invoke(this, null);
            }
            else
            {
                OwnStation = new Station(-1, true, "", 0);
                _linkedStations.Clear();
            }
        }

        private bool AreLinkedStationsChanged(List<TableASP> table)
        {
            if (Role != StationRole.Master)
                return false;

            if (table.Count != _linkedStations.Count)
                return true;

            for (int i = 0; i < _linkedStations.Count; i++)
            {
                if (_linkedStations[i].Id != table[i].Id ||
                    _linkedStations[i].IpAddress.Equals(table[i].AddressIP) ||
                    _linkedStations[i].Port != table[i].AddressPort)
                    return false;
            }

            return true;
        }

        private void TableFreqForbiddenUpdated(object sender, TableEventArs<TableFreqForbidden> e)
        {
            if (CheckTicket(NameTable.TableFreqForbidden))
                return;
            TableFreqForbiddenReceived?.Invoke(this, e.Table.ToArray());
        }

        private void TableFreqImportantUpdated(object sender, TableEventArs<TableFreqImportant> e)
        {
            if (CheckTicket(NameTable.TableFreqImportant))
                return;
            TableFreqImportantReceived?.Invoke(this, e.Table.ToArray());
        }

        private void TableFreqKnownUpdated(object sender, TableEventArs<TableFreqKnown> e)
        {
            if (CheckTicket(NameTable.TableFreqKnown))
                return;
            TableFreqKnownReceived?.Invoke(this, e.Table.ToArray());
        }


        private void TableFHSSExcludedFreqUpdated(object sender, TableEventArs<TableFHSSExcludedFreq> e) =>
            TableFhssExcludedFreqReceived?.Invoke(this, e.Table.ToArray());

        private void TableSuppressFWSUpdated(object sender, TableEventArs<TableSuppressFWS> e)
        {
            if (CheckTicket(NameTable.TableSuppressFWS))
                return;
            TableFrsJamReceived?.Invoke(this, e.Table.ToArray());
        }

        private void TableSuppressFHSSUpdated(object sender, TableEventArs<TableSuppressFHSS> e)
        {
            if (CheckTicket(NameTable.TableSuppressFHSS))
                return;
            TableFhssJamReceived?.Invoke(this, e.Table.ToArray());
        }

        private void TableReconFWSUpdated(object sender, TableEventArs<TableReconFWS> e)
        {
            if (CheckTicket(NameTable.TableReconFWS))
                return;
            TableFrsReconReceived?.Invoke(this, e.Table.ToArray());
        }

        private void TableReconFHSSUpdated(object sender, TableEventArs<TableReconFHSS> e)
        {
            return;
            if (CheckTicket(NameTable.TableReconFHSS))
                return;
            TableFhssReconReceived?.Invoke(this, e.Table.ToArray());
        }

        private void TableSectorsRangesReconUpdated(object sender, TableEventArs<TableSectorsRangesRecon> e)
        {
            if (CheckTicket(NameTable.TableSectorsRangesRecon))
                return;
            TableSectorsRangesReconReceived?.Invoke(this, e.Table.Where(r => r.IsCheck).ToArray());
        }

        private void TableSectorsRangesSupprUpdated(object sender, TableEventArs<TableSectorsRangesSuppr> e)
        {
            if (CheckTicket(NameTable.TableSectorsRangesSuppr))
                return;

            TableSectorsRangesSupprReceived?.Invoke(this, e.Table.Where(r => r.IsCheck).ToArray());
        }

        private bool CheckTicket(NameTable tableToCheck)
        {
            var ticket = _tickets.FirstOrDefault(t => t.IsExpired() == false && t.Table == tableToCheck);
            if (ticket.IsDefault() == false)
            {
                _tickets.Remove(ticket);
                return true;
            }
            return false;
        }
    }
}
