﻿using DataTransferClientLibrary.DataTransferService;
using DataTransferModel.DataTransfer;
using System;
using System.ServiceModel;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace DataTransferClientLibrary
{
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Reentrant)]
    public class DataTransferClient : IDataTransferClient, IDataTransferServiceCallback
    {
        public int Id { get; private set; }

        public bool IsConnected { get; private set; }

        public string EndpointAddress => _endpointAddress;
        private CancellationTokenSource tokenSource = new CancellationTokenSource();

        private DataTransferServiceClient _client;
        private readonly string _endpointAddress;
        private string lastConnectedPeer;

        public event EventHandler OnConnect;
        public event EventHandler<DisconnectedState> OnDisconnect;
        public event EventHandler<ErrorArgs> OnError;

        public event EventHandler<Message> OnMessageReceived;
        public event EventHandler<byte> OnModeReceived;
        public event EventHandler<int> OnSetBandReceived;

        public event EventHandler<ExecutiveDfRequestArgs> OnExecutiveDfRequestReceived;
        public event EventHandler<ExecutiveDfResponseArgs> OnExecutiveDfResponseReceived;
        public event EventHandler<RdfCycleResult> OnSlaveRdfResultReceived;
        public event EventHandler<RdfCycleResult> OnMasterRdfResultReceived;
        public event EventHandler<DateTime> OnLocalTimeReceived;
        public event EventHandler<FhssCycleResult> OnFhssCycleResultReceived;

        public DataTransferClient(string ipAddress, int port)
        {
            var endPointString = $"{ipAddress}:{port}";
            if (ValidEndPoint(endPointString))
                _endpointAddress = $"net.tcp://{endPointString}/";
            else
                OnError?.Invoke(this, new ErrorArgs(ErrorType.InvalidEndPointError, $"This is not a valid endpoint string - {endPointString} !"));
        }

        public void Connect(string name)
        {
            try
            {
                lastConnectedPeer = name;
                if (IsConnected)
                    return;
                _client = new DataTransferServiceClient(new InstanceContext(this), "NetTcpBinding_IDataTransferService");
                _client.Endpoint.Binding = GetBinding();
                _client.Endpoint.Address = new EndpointAddress(_endpointAddress);
                Id = _client.Connect(name);
                IsConnected = true;
                _client.ChannelFactory.Faulted += (o, args) => { NoticeAboutFaulted(); };
                OnConnect?.Invoke(this, EventArgs.Empty);
                if (IsConnected)
                {
                    try
                    {
                        Task.Run(CheckConnection);
                    }
                    catch
                    {
                        // ignored
                    }
                }
            }
            catch (Exception e)
            {
                OnError?.Invoke(this, new ErrorArgs(ErrorType.ConnectionError, e));
            }
        }

        private void NoticeAboutFaulted()
        {
            OnError.Invoke(this, new ErrorArgs(ErrorType.InvalidEndPointError, new Exception("Faulted state")));
        }

        private void CheckConnection()
        {
            CancellationToken token = tokenSource.Token;
            while (!token.IsCancellationRequested)
            {
                if (_client.State == CommunicationState.Faulted)
                {
                    OnError?.Invoke(this, new ErrorArgs(ErrorType.ConnectionError, new Exception("Faulted state")));
                    //Abort();
                    //Connect(lastConnectedPeer);
                }
                //Task.Delay(TimeSpan.FromSeconds(5));
                Thread.Sleep(5000);

            }
        }

        public void Disconnect()
        {
            try
            {
                if (!IsConnected)
                    return;
                
                IsConnected = false;

                if (_client != null)
                {
                    _client.Disconnect(Id);
                    Abort();
                }
            }
            catch (Exception e)
            {
                OnError?.Invoke(this, new ErrorArgs(ErrorType.DisconnectionError, e));
            }
        }

        public CommunicationState GetClientState => _client.State;
        public void SendMessage(int senderId, int receiverId, string message)
        {
            try
            {
                if (!IsConnected)
                    return;
                _client.SendMessage(senderId, receiverId, message);
            }
            catch (Exception e)
            {
                OnError?.Invoke(this, new ErrorArgs(ErrorType.SendMessageError, e));
            }
        }
        
        public void SendMessage(int receiverId, string message)
        {
            try
            {
                if (!IsConnected)
                    return;
                _client.SendMessage(-1, receiverId, message);
            }
            catch (Exception e)
            {
                OnError?.Invoke(this, new ErrorArgs(ErrorType.SendMessageError, e));
            }
        }

        public void SendMode(byte mode)
        {
            try
            {
                if (!IsConnected)
                    return;
                _client.SendMode(mode);
            }
            catch (Exception e)
            {
                OnError?.Invoke(this, new ErrorArgs(ErrorType.SendMessageError, e));
            }
        }

        public void SendExecutiveDfRequest(int stationId, float startFrequencyKhz, float endFrequencyKhz, int phaseAveragingCount, int directionAveragingCount)
        {
            try
            {
                if (!IsConnected)
                    return;
                _client.SendExecutiveDfRequest(stationId, startFrequencyKhz, endFrequencyKhz, phaseAveragingCount, directionAveragingCount);
            }
            catch (Exception e)
            {
                OnError?.Invoke(this, new ErrorArgs(ErrorType.SendMessageError, e));
            }
        }

        public void SendExecutiveDfResponse(int stationId, float result)
        {
            try
            {
                if (!IsConnected)
                    return;

                _client.SendExecutiveDfResponse(stationId, result);
            }
            catch (Exception e)
            {
                OnError?.Invoke(this, new ErrorArgs(ErrorType.SendMessageError, e));
            }
        }

        public void MessageCallback(int senderId, int receiverId, string message)
        {
            OnMessageReceived?.Invoke(this, new Message(senderId, receiverId, message));
        }

        public void ModeCallback(byte mode)
        {
            OnModeReceived?.Invoke(this, mode);
        }

        public void ExecutiveDfRequestCallback(int stationId, float startFrequencyKhz, float endFrequencyKhz, int phaseAveragingCount, int directionAveragingCount)
        {
           OnExecutiveDfRequestReceived?.Invoke(this, new ExecutiveDfRequestArgs(stationId, startFrequencyKhz, endFrequencyKhz, phaseAveragingCount, directionAveragingCount));
        }

        public void ExecutiveDfResponseCallback(int stationId, float result)
        {
            OnExecutiveDfResponseReceived?.Invoke(this, new ExecutiveDfResponseArgs(0, result, stationId));
        }

        private NetTcpBinding GetBinding()
        {
            var binding = new NetTcpBinding
            {
                Name = "NetTcpBinding_IDataTransferService"
            };
            
            binding.ReliableSession.Enabled = true;
            binding.ReliableSession.InactivityTimeout = TimeSpan.MaxValue;
            binding.ReceiveTimeout = TimeSpan.MaxValue;
            binding.CloseTimeout = TimeSpan.MaxValue;
            binding.SendTimeout = new TimeSpan(0, 0, 5);
            binding.OpenTimeout = new TimeSpan(0, 0, 5);
            binding.Security.Mode = SecurityMode.None;

            return binding;
        }

        private bool ValidEndPoint(string endpointAddress)
        {
            string ValidEndpointRegex = @"\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b";
            var r = new Regex(ValidEndpointRegex, RegexOptions.IgnoreCase | RegexOptions.Singleline);
            var m = r.Match(endpointAddress);
            return m.Success;
        }

        public void Abort()
        {
            AbortConnection();
            if (IsConnected)
            {
                IsConnected = false;
                OnDisconnect?.Invoke(this, DisconnectedState.Removed);
            }
            else
                OnDisconnect?.Invoke(this, DisconnectedState.Disconnected);
        }

        private void AbortConnection()
        {
            tokenSource.Cancel();
            _client.Abort();
            ((IDisposable)_client).Dispose();
            _client = null;
        }

        public void SendMasterRdfResult(RdfCycleResult result)
        {
            if (!IsConnected)
                return;

            _client.SendMasterRdfResult(result);
        }

        public void SendBandNumber(int bandNumber)
        {
            if (!IsConnected)
                return;

            _client.SetBandNumber(bandNumber);
        }

        public void SendSlaveRdfResult(RdfCycleResult result)
        {
            if (!IsConnected)
                return;

            _client.SendSlaveRdfResult(result);
        }

        public void SetBandCallback(int bandNumber)
        {
            OnSetBandReceived?.Invoke(this, bandNumber);
        }

        public void MasterRdfResultCallback(RdfCycleResult result)
        {
            OnMasterRdfResultReceived?.Invoke(this, result);
        }

        public void SlaveRdfResultCallback(RdfCycleResult result)
        {
            OnSlaveRdfResultReceived?.Invoke(this, result);
        }

        public void SendLocalTime(DateTime localTime)
        {
            try
            {
                if (!IsConnected)
                    return;
                _client.SendLocalTime(localTime);
            }
            catch (Exception e)
            {
                OnError?.Invoke(this, new ErrorArgs(ErrorType.SendMessageError, e));
            }
        }

        public void LocalTimeCallback(DateTime localTime)
        {
            OnLocalTimeReceived?.Invoke(this, localTime);
        }

        public void SendFhssCycleResult(FhssCycleResult result)
        {
            if (!IsConnected)
                return;

            _client.SendFhssCycleResult(result);
        }

        public void FhssCycleResultCallback(FhssCycleResult result) =>
            OnFhssCycleResultReceived?.Invoke(this, result);
    }
}
