﻿using DataTransferModel.DataTransfer;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Threading;
using System.Threading.Tasks;

namespace DataTransferControllerLibrary
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Reentrant)]
    public class DataTransferController : IDataTransferService, IDataTransferController
    {
        private readonly Dictionary<int, ServiceUser> _serviceUsers;
        private int _idCounter = 0;

        public bool ShowRdfLog { get; set; }

        public event EventHandler<string> OnLog;
        public event EventHandler<string> OnError;

        public event EventHandler<Message> OnMessageReceived;
        public event EventHandler<ExecutiveDfRequestArgs> OnExecutiveDfRequestReceived;
        public event EventHandler<ExecutiveDfResponseArgs> OnExecutiveDfResponseReceived;
        public event EventHandler<byte> OnModeReceived;
        public event EventHandler<DateTime> OnLocalTimeReceived;
        public event EventHandler<int> OnSetBandReceived;
        public event EventHandler<RdfCycleResult> OnSlaveRdfResultReceived;
        public event EventHandler<RdfCycleResult> OnMasterRdfResultReceived;
        public event EventHandler<FhssCycleResult> OnFhssCycleResultReceived;

        public DataTransferController()
        {
            _serviceUsers = new Dictionary<int, ServiceUser>();
        }

        public void Abort()
        {
            try
            {
                if (_serviceUsers.Count == 0)
                    return;
                foreach (var user in _serviceUsers)
                {
                    user.Value.Context.GetCallbackChannel<IDataTransferCallback>().Abort();
                }
            }
            catch (Exception e)
            {
                OnError?.Invoke(this, e.ToString());
            }
        }

        public int Connect(string name)
        {
            var serviceUser = new ServiceUser(name, OperationContext.Current);
            var clientId = _idCounter;
            _serviceUsers.Add(clientId, serviceUser);
            OnLog?.Invoke(this, $"{name} connected");
            Interlocked.Increment(ref _idCounter);
            return clientId;
        }

        public void Disconnect(int clientId)
        {
            _serviceUsers.TryGetValue(clientId, out var serviceUser);
            if (serviceUser != null)
            {
                _serviceUsers.Remove(clientId);
                OnLog?.Invoke(this, $"{serviceUser.Name} disconnected");
            }
            else
                OnLog?.Invoke(this, $"There is no user with such id : {clientId}");
        }

        public void SendMessage(int senderId, int receiverId, string message)
        {
            OnMessageReceived?.Invoke(this, new Message(senderId, receiverId, message));
            OnLog?.Invoke(this, $"Message to station {receiverId} : {message}");
        }

        public void MessageReceived(int senderId, int receiverId, string message)
        {
            foreach (var serviceUser in _serviceUsers)
            {
                MessageCallback(serviceUser.Value, senderId, receiverId, message);
            }

            OnLog?.Invoke(this, $"Message received from {senderId} : {message}");
        }

        private async Task MessageCallback(ServiceUser user, int senderId, int receiverId, string message)
        {
            try
            {
                user.Context.GetCallbackChannel<IDataTransferCallback>().MessageCallback(senderId, receiverId, message);
            }
            catch (Exception e)
            {
                OnError?.Invoke(this, $"Error during message receive for client \"{user.Name}\": {e.StackTrace}");
            }
        }

        public void SendLocalTime(DateTime localTime)
        {
            OnLocalTimeReceived?.Invoke(this, localTime);
            OnLog?.Invoke(this, $"received local time {localTime.ToLongTimeString()}");
        }

        public void LocalTimeReceived(DateTime localTime)
        {
            foreach (var serviceUser in _serviceUsers)
            {
                Task.Run(() =>
                {
                    try
                    {
                        serviceUser.Value.Context.GetCallbackChannel<IDataTransferCallback>().LocalTimeCallback(localTime);
                    }
                    catch (Exception e)
                    {
                        OnError?.Invoke(this, $"Error during local time receive : {e.StackTrace}");
                    }
                });
            }

            OnLog?.Invoke(this, $"Local time received : {localTime.ToLongTimeString()}");
        }

        public void SendExecutiveDfRequest(int stationId, float startFrequencyKhz, float endFrequencyKhz,
            int phaseAveragingCount, int directionAveragingCount)
        {
            OnExecutiveDfRequestReceived?.Invoke(this,
                new ExecutiveDfRequestArgs(stationId, startFrequencyKhz, endFrequencyKhz, phaseAveragingCount,
                    directionAveragingCount));
            OnLog?.Invoke(this, $"Executive df request is sended");
        }

        public void SendExecutiveDfResponse(int stationId, float result)
        {
            OnExecutiveDfResponseReceived?.Invoke(this, new ExecutiveDfResponseArgs(-1, result, stationId));
            OnLog?.Invoke(this, $"Executive df response is sended");
        }

        public void ExecutiveDfRequestReceived(ExecutiveDfRequestArgs requestArgs)
        {
            foreach (var serviceUser in _serviceUsers)
            {
                Task.Run(() =>
                {
                    try
                    {
                        serviceUser.Value.Context.GetCallbackChannel<IDataTransferCallback>()
                            .ExecutiveDfRequestCallback(requestArgs.StationId, requestArgs.StartFrequencyKhz,
                                requestArgs.EndFrequencyKhz, requestArgs.PhaseAveragingCount,
                                requestArgs.DirectionAveragingCount);
                    }
                    catch (Exception e)
                    {
                        OnError?.Invoke(this,
                            $"Error during executive df request receive for client \"{serviceUser.Value.Name}\": {e.StackTrace}");
                    }
                });
            }

            OnLog?.Invoke(this, $"Executive df request received");
        }

        public void ExecutiveDfResponseReceived(int stationId, float result)
        {
            foreach (var serviceUser in _serviceUsers)
            {
                Task.Run(() =>
                {
                    try
                    {
                        serviceUser.Value.Context.GetCallbackChannel<IDataTransferCallback>()
                            .ExecutiveDfResponseCallback(stationId, result);
                    }
                    catch (Exception e)
                    {
                        OnError?.Invoke(this,
                            $"Error during executive df response receive for client \"{serviceUser.Value.Name}\": {e.StackTrace}");
                    }
                });
            }

            OnLog?.Invoke(this, $"Executive df response received");
        }

        public void SendMode(byte mode)
        {
            OnModeReceived?.Invoke(this, mode);
            OnLog?.Invoke(this, $"Send mode command received : {mode}");
        }

        public void ModeReceived(byte mode)
        {
            foreach (var serviceUser in _serviceUsers)
            {
                Task.Run(() =>
                {
                    try
                    {
                        serviceUser.Value.Context.GetCallbackChannel<IDataTransferCallback>().ModeCallback(mode);
                    }
                    catch (Exception e)
                    {
                        OnError?.Invoke(this, $"Error during mode receive : {e.StackTrace}");
                    }
                });
            }

            OnLog?.Invoke(this, $"Mode received : {mode}");
        }

        public void SetBandNumber(int bandNumber)
        {
            OnSetBandReceived?.Invoke(this, bandNumber);
            OnLog?.Invoke(this, $"Set band number command received {bandNumber}");
        }

        public void BandReceived(int bandNumber)
        {
            foreach (var serviceUser in _serviceUsers)
            {
                Task.Run(() =>
                {
                    try
                    {
                        serviceUser.Value.Context.GetCallbackChannel<IDataTransferCallback>()
                            .SetBandCallback(bandNumber);
                    }
                    catch (Exception e)
                    {
                        OnError?.Invoke(this, $"Error during band receive : {e.StackTrace}");
                    }
                });
            }

            OnLog?.Invoke(this, $"Sending band to clients");
        }

        public void SendSlaveRdfResult(RdfCycleResult result)
        {
            if(ShowRdfLog)
                OnLog?.Invoke(this, "Slave rdf results received from client");
            OnSlaveRdfResultReceived?.Invoke(this, result);
        }

        public void SendMasterRdfResult(RdfCycleResult result)
        {
            if (ShowRdfLog)
                OnLog?.Invoke(this, "Master rdf results received from client");
            OnMasterRdfResultReceived?.Invoke(this, result);
        }

        public void SlaveRdfResultReceived(RdfCycleResult result)
        {
            foreach (var serviceUser in _serviceUsers)
            {
                Task.Run(() =>
                {
                    try
                    {
                        serviceUser.Value.Context.GetCallbackChannel<IDataTransferCallback>().SlaveRdfResultCallback(result);
                    }
                    catch (Exception e)
                    {
                        OnError?.Invoke(this, $"Error during signals receive : {e.StackTrace}");
                    }
                });
            }

            if (ShowRdfLog)
                OnLog?.Invoke(this, $"Sending slave rdf result to clients");
        }

        public void SlaveToMasterRdfResultReceived(RdfCycleResult signals)
        {
            foreach (var serviceUser in _serviceUsers)
            {
                Task.Run(() =>
                {
                    try
                    {
                        serviceUser.Value.Context.GetCallbackChannel<IDataTransferCallback>().SlaveRdfResultCallback(signals);
                    }
                    catch (Exception e)
                    {
                        OnError?.Invoke(this, $"Error during signals receive : {e.StackTrace}");
                    }
                });
            }

            if (ShowRdfLog)
                OnLog?.Invoke(this, $"Sending master rdf result to slave clients");
        }

        public void MasterRdfResultReceived(RdfCycleResult result)
        {
            foreach (var serviceUser in _serviceUsers)
            {
                Task.Run(() =>
                {
                    try
                    {
                        serviceUser.Value.Context.GetCallbackChannel<IDataTransferCallback>()
                            .MasterRdfResultCallback(result);
                    }
                    catch (Exception e)
                    {
                        OnError?.Invoke(this, $"Error during signals result receive : {e.StackTrace}");
                    }
                });
            }

            if (ShowRdfLog)
                OnLog?.Invoke(this, $"Sending master rdf result to clients");
        }

        public void FhssCycleResultReceived(FhssCycleResult result)
        {
            foreach (var serviceUser in _serviceUsers)
            {
                Task.Run(() =>
                {
                    try
                    {
                        serviceUser.Value.Context.GetCallbackChannel<IDataTransferCallback>()
                            .FhssCycleResultCallback(result);
                    }
                    catch (Exception e)
                    {
                        OnError?.Invoke(this, $"Error during signals result receive : {e.StackTrace}");
                    }
                });
            }
        }

        public void SendFhssCycleResult(FhssCycleResult result)
        {
            OnFhssCycleResultReceived?.Invoke(this, result);
        }
    }
}
