﻿using System;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using DataTransferModel;
using DataTransferModel.Database;
using DataTransferModel.Trasmission;
using DataTransferModel.DataTransfer;
using DataTransferControllerLibrary;

namespace ServerControllerLibrary
{
    internal abstract class BaseServerConductor : IServerConductor
    {
        protected readonly IDatabaseController _databaseController;
        protected readonly DataTransferController _dataTransferController;
        protected readonly IReadOnlyList<ITransmitter> _linkedStationsTransmitters;

        public virtual event EventHandler<string> OnTransmissionError;

        protected BaseServerConductor(IDatabaseController databaseController, DataTransferController dataTransferController, IReadOnlyList<ITransmitter> transmitters) 
        {
            _databaseController = databaseController;
            _dataTransferController = dataTransferController;
            _linkedStationsTransmitters = transmitters;

            Initialize();
        }

        public void Initialize()
        {
            SubscribeToDatabaseEvents();
            SubscribeToDataTransferEvents();
            SubscribeToTransmittersEvents();
        }

        public void ShutDown()
        {
            UnsubscribeFromDatabaseEvents();
            UnsubscribeFromDataTransferEvents();
            UnsubscribeFromTransmittersEvents();
        }

        protected abstract void SubscribeToDatabaseEvents();
        protected abstract void UnsubscribeFromDatabaseEvents();
        protected abstract void SubscribeToDataTransferEvents();
        protected abstract void UnsubscribeFromDataTransferEvents();
        protected abstract void SubscribeToTransmittersEvents();
        protected abstract void UnsubscribeFromTransmittersEvents();

        #region Database
        protected void _databaseController_TableFhssExcludedFreqReceived(object sender, ModelsTablesDBLib.TableFHSSExcludedFreq[] table)
        {
            //todo : me
        }

        protected void _databaseController_TableFhssReconReceived(object sender, ModelsTablesDBLib.TableReconFHSS[] table)
        {
            //todo : me
        }

        protected async void _databaseController_AspTableReceived(object sender, AspTable table)
        {
            try
            {
                await Task.Delay(1);
                foreach (var transmitter in _linkedStationsTransmitters)
                {
                    transmitter.SendAspTable(table);
                }
            }
            catch (Exception e)
            {
                OnTransmissionError?.Invoke(this, $"Transmission exception occured\r\n{e}");
            }
        }
        #endregion

        #region Data Transfer


        protected async void _dataTransferController_OnFhssCycleResultReceived(object sender, FhssCycleResult fhssResult)
        {
            try
            {
                await Task.Delay(1);
                foreach (var transmitter in _linkedStationsTransmitters)
                {
                    transmitter.SendFhssCycleResult(fhssResult);
                }
            }
            catch (Exception e)
            {
                OnTransmissionError?.Invoke(this, $"Transmission exception occured\r\n{e}");
            }
        }
        protected async void _dataTransferController_OnSendMessageRequest(object sender, Message message)
        {
            try
            {
                await Task.Delay(1);
                foreach (var transmitter in _linkedStationsTransmitters)
                {
                    transmitter.SendMessage(message);
                }
            }
            catch (Exception e) 
            {
                OnTransmissionError?.Invoke(this, $"Transmission exception occured\r\n{e}");
            }
        }

        protected async void _dataTransferController_OnSendExecutiveDfRequest(object sender, ExecutiveDfRequestArgs request)
        {
            try
            {
                await Task.Delay(1);
                var transmitter = _linkedStationsTransmitters.FirstOrDefault(t => t.ConnectedStationId == request.StationId);
                transmitter?.SendExecutiveDfRequest(request.StartFrequencyKhz, request.EndFrequencyKhz, request.PhaseAveragingCount, request.DirectionAveragingCount);
            }
            catch (Exception e)
            {
                OnTransmissionError?.Invoke(this, $"Transmission exception occured\r\n{e}");
            }
        }

        protected async void _dataTransferController_OnSendExecutiveDfResponse(object sender, ExecutiveDfResponseArgs result)
        {
            try
            {
                await Task.Delay(1);
                foreach (var transmitter in _linkedStationsTransmitters)
                {
                    transmitter.SendExecutiveDfResponse(result.Direction, result.StationId);
                }
            }
            catch (Exception e)
            {
                OnTransmissionError?.Invoke(this, $"Transmission exception occured\r\n{e}");
            }            
        }

        #endregion

        #region Transmitters

        protected void Transmitter_OnSendMessageRequest(object sender, Message message)
        {
            _dataTransferController.MessageReceived(message.SenderId, message.ReceiverId, message.Text);
        }

        protected void Transmitter_TableFhssExcludedFreqReceived(object sender, TableEventArgs<ModelsTablesDBLib.TableFHSSExcludedFreq> args)
        {
            //todo : me
        }

        protected void Transmitter_TableFhssReconReceived(object sender, TableEventArgs<ModelsTablesDBLib.TableReconFHSS> args)
        {
            _databaseController.ActuallyUpdateFhssReconTable(args.SenderStationId, args.Table);
        }

        protected void Transmitter_AspTableReceived(object sender, AspTable table)
        {
            _databaseController.UpdateAspTable(table);
        }

        protected void Transmitter_OnSendExecutiveDfResponse(object sender, ExecutiveDfResponseArgs result)
        {
            _dataTransferController.ExecutiveDfResponseReceived(result.StationId, result.Direction);
        }

        protected void Transmitter_OnSendExecutiveDfRequest(object sender, ExecutiveDfRequestArgs request)
        {
            _dataTransferController.ExecutiveDfRequestReceived(request);
        }

        protected void Transmitter_OnFhssCycleResultReceived(object sender, FhssCycleResult fhssResult)
        {
            _dataTransferController.FhssCycleResultReceived(fhssResult);
        }

        #endregion
    }
}
