﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using DatabaseControllerLibrary;
using DataTransferControllerLibrary;
using DataTransferModel;
using DataTransferModel.Database;
using DataTransferModel.Trasmission;
using TransmissionLibrary.GrpcTransmission;
using TransmissionLibrary.SlowUdpTransmission;

namespace ServerControllerLibrary
{
    public class ServerManager : IServerManager
    {
        private IServerConductor _serverConductor;
        private readonly IDatabaseController _databaseController;
        private readonly DataTransferController _dataTransferController;
        private readonly List<ITransmitter> _linkedStationsTransmitters;

        private readonly ITransmitterFactory _ethernetTransmitterFactory;
        private readonly ITransmitterFactory _mobile3gTransmitterFactory;
        private readonly ITransmitterFactory _comTransmitterFactory;
        private readonly EthernetConnectionType _connectionType;

        public event EventHandler<string> OnLocalEvent;
        public event EventHandler<string> OnGlobalEvent;
        public event EventHandler<string> OnError;
        /// <summary>
        /// For indication of database connection state
        /// </summary>
        public event EventHandler<bool> OnDatabaseConnectionStateChangedEvent;

        public ServerManager(DataTransferController dataTransferController, string databaseHost, int databasePort,
            ITransmitterFactory ethernetTransmitterFactory, ITransmitterFactory mobile3gTransmitterFactory, 
            ITransmitterFactory comTransmitterFactory, EthernetConnectionType connectionType)
        {
            _dataTransferController = dataTransferController;
            _databaseController = new DatabaseController(databaseHost, databasePort);
            _databaseController.OnDatabaseError += (sender, s) => OnLocalEvent?.Invoke(this, s);
            _databaseController.OnDatabaseLog += (sender, s) => OnLocalEvent?.Invoke(this, s);
            _linkedStationsTransmitters = new List<ITransmitter>(5);

            _ethernetTransmitterFactory = ethernetTransmitterFactory;
            _mobile3gTransmitterFactory = mobile3gTransmitterFactory;
            _comTransmitterFactory = comTransmitterFactory;
            _connectionType = connectionType;

            SubscribeToDatabaseAndDataTransferEvents();
            _serverConductor = CreateWaitingConductor();
        }

        private void SubscribeToDatabaseAndDataTransferEvents()
        {
            _dataTransferController.OnLog += LocalEventInvoked;

            _databaseController.OnDatabaseConnectionStateChanged += OnDatabaseConnectionStateChanged;
            _databaseController.OnLinkedStationsConnectionSettingsChanged += OnLinkedStationsConnectionSettingsChanged;
            _databaseController.OnOwnStationValidData += OnOwnStationValidData;
        }
        private void UnsubscribeFromDatabaseAndDataTransferEvents()
        {
            _dataTransferController.OnLog -= LocalEventInvoked;

            _databaseController.OnDatabaseConnectionStateChanged -= OnDatabaseConnectionStateChanged;
            _databaseController.OnLinkedStationsConnectionSettingsChanged -= OnLinkedStationsConnectionSettingsChanged;
            _databaseController.OnOwnStationValidData -= OnOwnStationValidData;
        }

        private void OnOwnStationValidData(object sender, bool isValid)
        {
            ShutDownTransmitters();
            UnsubFromServerConductorEvent();
            _serverConductor?.ShutDown();
            if (isValid)
            {
                InitializeTransmitters();
                _serverConductor = CreateServerConductor();
                RunPingTask();
            }
            else
            {
                _serverConductor = CreateWaitingConductor();
            }
        }

        //todo : refactor
        private void OnLinkedStationsConnectionSettingsChanged(object sender, IReadOnlyList<int> stationIds)
        {
            ShutDownTransmitters();
            InitializeTransmitters();
            RunPingTask();
        }

        private void OnDatabaseConnectionStateChanged(object sender, bool connectionState)
        {
            UnsubFromServerConductorEvent();
            _serverConductor?.ShutDown();
            _serverConductor = connectionState ? CreateServerConductor() : CreateWaitingConductor();
            OnDatabaseConnectionStateChangedEvent?.Invoke(this, connectionState);
        }

        private void _serverConductor_OnTransmissionError(object sender, string e)
        {
            OnError?.Invoke(sender, e);
        }

        private IServerConductor CreateWaitingConductor()
        {
            return new WaitingServerConductor(_databaseController, tryToConnectToDatabase: true);
        }

        private IServerConductor CreateServerConductor()
        {
            IServerConductor conductor;
            switch (_databaseController.Role)
            {
                //todo : on role changes, add event to db, add implementation
                case StationRole.Slave: 
                    conductor = new SlaveServerConductor(_databaseController, _dataTransferController, _linkedStationsTransmitters);
                    break;
                case StationRole.Master: 
                    conductor = new MasterServerConductor(_databaseController, _dataTransferController, _linkedStationsTransmitters);
                    break;
                default: 
                    conductor = new WaitingServerConductor(_databaseController);
                    break;
            }
            conductor.OnTransmissionError += _serverConductor_OnTransmissionError;
            return conductor;
        }

        private void UnsubFromServerConductorEvent()
        {
            if (_serverConductor != null)
                _serverConductor.OnTransmissionError -= _serverConductor_OnTransmissionError;
        }

        public void ShutDown()
        {
            UnsubscribeFromDatabaseAndDataTransferEvents();
            ShutDownTransmitters();
            foreach (var station in _databaseController.LinkedStations)
            {
                _databaseController.UpdateAspConnectionState(station.Id, false);
            }
            Thread.Sleep(1000);
            //_databaseController.Disconnect(); // it does not work as expected, just rely on the system
            _dataTransferController.Abort();
        }

        private void InitializeTransmitters() 
        {
            foreach (var linkedStation in _databaseController.LinkedStations)
            {
                if (linkedStation.ConnectionType == ConnectionType.None)
                    continue;

                ITransmitter transmitter = CreateTransmitter(linkedStation);
                _linkedStationsTransmitters.Add(transmitter);
                SubscribeToTransmitterEvents(transmitter);
            }
        }

        private ITransmitter CreateTransmitter(Station linkedStation)
        {
            var ownStationId = _databaseController.OwnStation.Id;
            var isMaster = _databaseController.Role == StationRole.Master;
            if (_connectionType == EthernetConnectionType.Slow 
                && (linkedStation.ConnectionType == ConnectionType.Ethernet || linkedStation.ConnectionType == ConnectionType.Mobile3G))
            {
                var factory = linkedStation.ConnectionType == ConnectionType.Ethernet
                    ? _ethernetTransmitterFactory as GrpcTransmitterFactory
                    : _mobile3gTransmitterFactory as GrpcTransmitterFactory;
                
                var slowFactory = new SlowTcpTransmitterFactory(factory.GetIp(), factory.GetPort());
                return slowFactory.CreateTransmitter(isMaster, ownStationId, linkedStation);
            }

            switch (linkedStation.ConnectionType) 
            {
                case ConnectionType.Ethernet: 
                    return isMaster
                    ? _ethernetTransmitterFactory.CreateTransmitter(ownStationId, linkedStation.Id, linkedStation.IpAddress, linkedStation.Port, isMaster)
                    : _ethernetTransmitterFactory.CreateTransmitter(ownStationId, linkedStation.Id, isMaster);

                case ConnectionType.Mobile3G: 
                    return isMaster
                    ? _mobile3gTransmitterFactory.CreateTransmitter(ownStationId, linkedStation.Id, linkedStation.IpAddress, linkedStation.Port, isMaster)
                    : _mobile3gTransmitterFactory.CreateTransmitter(ownStationId, linkedStation.Id, isMaster);

                case ConnectionType.ComPort:
                    return _comTransmitterFactory.CreateTransmitter(ownStationId, linkedStation.Id, isMaster);

                default: return null;
            }
        }

        private void SubscribeToTransmitterEvents(ITransmitter transmitter)
        {
            transmitter.OnReceive += GlobalEventInvoked;
            transmitter.OnSend += LocalEventInvoked;
            transmitter.OnForceConnectionRequest += OnForceConnectionRequest;
            transmitter.ConnectionStateChanged += Transmitter_ConnectionStateChanged;
        }

        private void UnsubscribeFromTransmitterEvents(ITransmitter transmitter)
        {
            transmitter.OnReceive -= GlobalEventInvoked;
            transmitter.OnSend -= LocalEventInvoked;
            transmitter.OnForceConnectionRequest -= OnForceConnectionRequest;
            transmitter.ConnectionStateChanged -= Transmitter_ConnectionStateChanged;
        }

        private void Transmitter_ConnectionStateChanged(object sender, bool currentConnectionState)
        {
            _databaseController.UpdateAspConnectionState((sender as ITransmitter).ConnectedStationId, currentConnectionState);

            if (currentConnectionState)
                _databaseController.LoadTablesForClient();
        }

        private void OnForceConnectionRequest(object sender, PeerInfo peerInfo)
        {
            var transmitter = _linkedStationsTransmitters.FirstOrDefault(t => t.ConnectedStationId == peerInfo.StationId);
            if (transmitter == null)
                return;
            if (transmitter.Type == TransmissionType.ComTransmitter)
                return;
            if (transmitter.IsConnected)
                return;
            transmitter.ForceConnection(peerInfo.IpAddress, peerInfo.Port);
        }

        private void LocalEventInvoked(object sender, string e)
        {
            OnLocalEvent?.Invoke(this, e);
        }

        private void GlobalEventInvoked(object sender, string e)
        {
            OnGlobalEvent?.Invoke(this, e);
        }

        private void ShutDownTransmitters()
        {
            _pingTaskCancellationTokenSource?.Cancel();
            foreach (var transmitter in _linkedStationsTransmitters)
            {
                UnsubscribeFromTransmitterEvents(transmitter);
                transmitter.ShutDown();
            }
            _linkedStationsTransmitters.Clear();
        }

        public void SetRdfLogVisibility(bool flag)
        {
            _dataTransferController.ShowRdfLog = flag;
            foreach (var transmitter in _linkedStationsTransmitters)
            {
                transmitter.ShowRdfLog = flag;
            }
        }

        private void RunPingTask() 
        {
            _pingTaskCancellationTokenSource = new CancellationTokenSource();
            Task.Run(PingTask);
        }

        private CancellationTokenSource _pingTaskCancellationTokenSource;

        /// <summary>
        /// Pings transmitters to identify, whether we are connected or not
        /// </summary>
        private async Task PingTask()
        {
            while (_pingTaskCancellationTokenSource.Token.IsCancellationRequested == false)
            {
                foreach (var transmitter in _linkedStationsTransmitters) 
                {
                    var pingResult = transmitter.Ping(transmitter.OwnStationId, transmitter.ServerPort);
                    if (pingResult)
                        await Task.Delay(Constants.SucceededPingDelayMs);
                    else
                    {
                        if (transmitter.Type == TransmissionType.GrpcTransmitter)
                            transmitter.AbortConnection(_databaseController.Role == StationRole.Master);//to clear previously established connection
                        await Task.Delay(Constants.FailedPingDelayMs);
                    }
                }
            }
        }
    }
}
