﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using DataTransferControllerLibrary;
using DataTransferModel.Database;
using DataTransferModel.Trasmission;
using DataTransferModel.DataTransfer;

namespace ServerControllerLibrary
{
    internal class MasterServerConductor : BaseServerConductor
    {
        internal MasterServerConductor(IDatabaseController databaseController, DataTransferController dataTransferController, IReadOnlyList<ITransmitter> transmitters)
            : base(databaseController, dataTransferController, transmitters)
        { }

        public override event EventHandler<string> OnTransmissionError;

        #region Database
        protected override void SubscribeToDatabaseEvents() 
        {
            _databaseController.AspTableReceived += _databaseController_AspTableReceived;
            _databaseController.TableSectorsRangesReconReceived += _databaseController_TableSectorsRangesReconReceived;
            _databaseController.TableSectorsRangesSupprReceived += _databaseController_TableSectorsRangesSupprReceived;
            _databaseController.TableFrsJamReceived += _databaseController_TableFrsJamReceived;
            _databaseController.TableFreqKnownReceived += _databaseController_TableFreqKnownReceived;
            _databaseController.TableFreqImportantReceived += _databaseController_TableFreqImportantReceived;
            _databaseController.TableFreqForbiddenReceived += _databaseController_TableFreqForbiddenReceived;
            _databaseController.TableFhssReconReceived += _databaseController_TableFhssReconReceived;
            _databaseController.TableFhssJamReceived += _databaseController_TableFhssJamReceived;
            _databaseController.TableFhssExcludedFreqReceived += _databaseController_TableFhssExcludedFreqReceived;
        }

        protected override void UnsubscribeFromDatabaseEvents() 
        {
            _databaseController.AspTableReceived -= _databaseController_AspTableReceived;
            _databaseController.TableSectorsRangesReconReceived -= _databaseController_TableSectorsRangesReconReceived;
            _databaseController.TableSectorsRangesSupprReceived -= _databaseController_TableSectorsRangesSupprReceived;
            _databaseController.TableFrsJamReceived -= _databaseController_TableFrsJamReceived;
            _databaseController.TableFreqKnownReceived -= _databaseController_TableFreqKnownReceived;
            _databaseController.TableFreqImportantReceived -= _databaseController_TableFreqImportantReceived;
            _databaseController.TableFreqForbiddenReceived -= _databaseController_TableFreqForbiddenReceived;
            _databaseController.TableFhssReconReceived -= _databaseController_TableFhssReconReceived;
            _databaseController.TableFhssJamReceived -= _databaseController_TableFhssJamReceived;
            _databaseController.TableFhssExcludedFreqReceived -= _databaseController_TableFhssExcludedFreqReceived;
        }

        private async void _databaseController_TableFhssJamReceived(object sender, ModelsTablesDBLib.TableSuppressFHSS[] table)
        {
            try
            {
                await Task.Delay(1);
                foreach (var transmitter in _linkedStationsTransmitters)
                {
                    var stationTable = table.Where(record => record.NumberASP == transmitter.ConnectedStationId).ToArray();
                    transmitter.SendTableFhssJam(new TableEventArgs<ModelsTablesDBLib.TableSuppressFHSS>(transmitter.ConnectedStationId, stationTable));
                }
            }
            catch (Exception e)
            {
                OnTransmissionError?.Invoke(this, $"Transmission exception occured\r\n{e}");
            }
        }

        private async void _databaseController_TableFrsJamReceived(object sender, ModelsTablesDBLib.TableSuppressFWS[] table)
        {
            try
            {
                await Task.Delay(1);
                foreach (var transmitter in _linkedStationsTransmitters)
                {
                    var stationTable = table.Where(record => record.NumberASP == transmitter.ConnectedStationId).ToArray();
                    transmitter.SendTableFrsJam(new TableEventArgs<ModelsTablesDBLib.TableSuppressFWS>(transmitter.ConnectedStationId, stationTable));
                }
            }
            catch (Exception e)
            {
                OnTransmissionError?.Invoke(this, $"Transmission exception occured\r\n{e}");
            }
        }

        private async void _databaseController_TableFreqForbiddenReceived(object sender, ModelsTablesDBLib.TableFreqForbidden[] table)
        {
            try
            {
                await Task.Delay(1);
                foreach (var transmitter in _linkedStationsTransmitters)
                {
                    var stationTable = table.Where(record => record.NumberASP == transmitter.ConnectedStationId).ToArray();
                    transmitter.SendTableFreqForbidden(new TableEventArgs<ModelsTablesDBLib.TableFreqForbidden>(transmitter.ConnectedStationId, stationTable));
                }
            }
            catch (Exception e)
            {
                OnTransmissionError?.Invoke(this, $"Transmission exception occured\r\n{e}");
            }
        }

        private async void _databaseController_TableFreqImportantReceived(object sender, ModelsTablesDBLib.TableFreqImportant[] table)
        {            
            try
            {
                await Task.Delay(1);
                foreach (var transmitter in _linkedStationsTransmitters)
                {
                    var stationTable = table.Where(record => record.NumberASP == transmitter.ConnectedStationId).ToArray();
                    transmitter.SendTableFreqImportant(new TableEventArgs<ModelsTablesDBLib.TableFreqImportant>(transmitter.ConnectedStationId, stationTable));
                }
            }
            catch (Exception e)
            {
                OnTransmissionError?.Invoke(this, $"Transmission exception occured\r\n{e}");
            }
        }

        private async void _databaseController_TableFreqKnownReceived(object sender, ModelsTablesDBLib.TableFreqKnown[] table)
        {
            try
            {
                await Task.Delay(1);
                foreach (var transmitter in _linkedStationsTransmitters)
                {
                    var stationTable = table.Where(record => record.NumberASP == transmitter.ConnectedStationId).ToArray();
                    transmitter.SendTableFreqKnown(new TableEventArgs<ModelsTablesDBLib.TableFreqKnown>(transmitter.ConnectedStationId, stationTable));
                }
            }
            catch (Exception e)
            {
                OnTransmissionError?.Invoke(this, $"Transmission exception occured\r\n{e}");
            }
        }

        private async void _databaseController_TableSectorsRangesSupprReceived(object sender, ModelsTablesDBLib.TableSectorsRangesSuppr[] table)
        {
            try
            {
                await Task.Delay(1);
                foreach (var transmitter in _linkedStationsTransmitters)
                {
                    var stationTable = table.Where(record => record.NumberASP == transmitter.ConnectedStationId).ToArray();
                    transmitter.SendTableSectorsRangesSuppr(new TableEventArgs<ModelsTablesDBLib.TableSectorsRangesSuppr>(transmitter.ConnectedStationId, stationTable));
                }
            }
            catch (Exception e)
            {
                OnTransmissionError?.Invoke(this, $"Transmission exception occured\r\n{e}");
            }
        }

        private async void _databaseController_TableSectorsRangesReconReceived(object sender, ModelsTablesDBLib.TableSectorsRangesRecon[] table)
        {
            try
            {
                await Task.Delay(1);
                foreach (var transmitter in _linkedStationsTransmitters)
                {
                    var stationTable = table.Where(record => record.NumberASP == transmitter.ConnectedStationId).ToArray();
                    transmitter.SendTableSectorsRangesRecon(new TableEventArgs<ModelsTablesDBLib.TableSectorsRangesRecon>(transmitter.ConnectedStationId, stationTable));
                }
            }
            catch (Exception e)
            {
                OnTransmissionError?.Invoke(this, $"Transmission exception occured\r\n{e}");
            }
        }
        #endregion

        #region Data Transfer
        protected override void SubscribeToDataTransferEvents() 
        {            
            _dataTransferController.OnMessageReceived += _dataTransferController_OnSendMessageRequest;
            _dataTransferController.OnModeReceived += _dataTransferController_OnSendModeRequest;
            _dataTransferController.OnExecutiveDfRequestReceived += _dataTransferController_OnSendExecutiveDfRequest;
            _dataTransferController.OnExecutiveDfResponseReceived += _dataTransferController_OnSendExecutiveDfResponse;
            _dataTransferController.OnMasterRdfResultReceived += _dataTransferController_OnMasterRdfResultReceived;
            _dataTransferController.OnFhssCycleResultReceived += _dataTransferController_OnFhssCycleResultReceived;
        }

        protected override void UnsubscribeFromDataTransferEvents()
        {
            _dataTransferController.OnMessageReceived -= _dataTransferController_OnSendMessageRequest;
            _dataTransferController.OnModeReceived -= _dataTransferController_OnSendModeRequest;
            _dataTransferController.OnExecutiveDfRequestReceived -= _dataTransferController_OnSendExecutiveDfRequest;
            _dataTransferController.OnExecutiveDfResponseReceived -= _dataTransferController_OnSendExecutiveDfResponse;
            _dataTransferController.OnMasterRdfResultReceived -= _dataTransferController_OnMasterRdfResultReceived;
            _dataTransferController.OnFhssCycleResultReceived -= _dataTransferController_OnFhssCycleResultReceived;
        }

        private async void _dataTransferController_OnMasterRdfResultReceived(object sender, RdfCycleResult rdfResult)
        {
            try
            {
                await Task.Delay(1);
                foreach (var transmitter in _linkedStationsTransmitters)
                {                
                    transmitter.SendMasterRdfResult(rdfResult);
                }
            }
            catch (Exception e)
            {
                OnTransmissionError?.Invoke(this, $"Transmission exception occured\r\n{e}");
            }
        }

        private async void _dataTransferController_OnSendModeRequest(object sender, byte mode)
        {
            try
            {
                await Task.Delay(1);
                foreach (var transmitter in _linkedStationsTransmitters) 
                {
                    transmitter.SendMode(mode);
                }
            }
            catch (Exception e)
            {
                OnTransmissionError?.Invoke(this, $"Transmission exception occured\r\n{e}");
            }            
        }

        #endregion

        #region Transmitters

        protected override void SubscribeToTransmittersEvents()
        {
            foreach (var transmitter in _linkedStationsTransmitters)
            {
                transmitter.AspTableReceived += Transmitter_AspTableReceived;
                transmitter.TableFrsReconReceived += Transmitter_TableFrsReconReceived;
                transmitter.TableFrsJamReceived += Transmitter_TableFrsJamReceived;
                transmitter.TableFhssReconReceived += Transmitter_TableFhssReconReceived;
                transmitter.TableFhssJamReceived += Transmitter_TableFhssJamReceived;
                transmitter.TableFhssExcludedFreqReceived += Transmitter_TableFhssExcludedFreqReceived;

                transmitter.OnSendMessageRequest += Transmitter_OnSendMessageRequest;
                transmitter.OnSendExecutiveDfResponse += Transmitter_OnSendExecutiveDfResponse;
                transmitter.OnSendExecutiveDfRequest += Transmitter_OnSendExecutiveDfRequest;
                transmitter.OnSlaveRdfResultReceived += Transmitter_OnSlaveRdfResultReceived;
                transmitter.OnFhssCycleResultReceived += Transmitter_OnFhssCycleResultReceived;
            }
        }

        protected override void UnsubscribeFromTransmittersEvents() 
        {
            foreach (var transmitter in _linkedStationsTransmitters)
            {
                transmitter.AspTableReceived -= Transmitter_AspTableReceived;
                transmitter.TableFrsReconReceived -= Transmitter_TableFrsReconReceived;
                transmitter.TableFrsJamReceived -= Transmitter_TableFrsJamReceived;
                transmitter.TableFhssReconReceived -= Transmitter_TableFhssReconReceived;
                transmitter.TableFhssJamReceived -= Transmitter_TableFhssJamReceived;
                transmitter.TableFhssExcludedFreqReceived -= Transmitter_TableFhssExcludedFreqReceived;

                transmitter.OnSendMessageRequest -= Transmitter_OnSendMessageRequest;
                transmitter.OnSendExecutiveDfResponse -= Transmitter_OnSendExecutiveDfResponse;
                transmitter.OnSendExecutiveDfRequest -= Transmitter_OnSendExecutiveDfRequest;
                transmitter.OnSlaveRdfResultReceived -= Transmitter_OnSlaveRdfResultReceived;
                transmitter.OnFhssCycleResultReceived -= Transmitter_OnFhssCycleResultReceived;
            }
        }

        protected void Transmitter_TableFrsReconReceived(object sender, TableEventArgs<ModelsTablesDBLib.TableReconFWS> args)
        {
            _databaseController.ActuallyUpdateFrsReconTable(args.SenderStationId, args.Table);
        }

        private void Transmitter_TableFrsJamReceived(object sender, TableEventArgs<ModelsTablesDBLib.TableSuppressFWS> args)
        {
            _databaseController.ActuallyUpdateFrsJamTable(args.SenderStationId, args.Table);
        }

        private void Transmitter_TableFhssJamReceived(object sender, TableEventArgs<ModelsTablesDBLib.TableSuppressFHSS> args)
        {
            _databaseController.ActuallyUpdateFhssJamTable(args.SenderStationId, args.Table);
        }

        private void Transmitter_OnSlaveRdfResultReceived(object sender, RdfCycleResult rdfResult)
        {
            _dataTransferController.SlaveRdfResultReceived(rdfResult);
        }

        #endregion
    }
}
