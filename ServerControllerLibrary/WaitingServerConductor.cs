﻿using System;
using DataTransferModel;
using DataTransferModel.Database;

namespace ServerControllerLibrary
{
    /// <summary>
    /// Dummy conductor. Doesn't do anything, can make one attempt to connect to database
    /// </summary>
    internal class WaitingServerConductor : IServerConductor
    {
        public event EventHandler<string> OnTransmissionError;

        /*
         * This conductor is used, when there are no database connection and we need to establish it first.
         * Because OnDisconnect event is occured at bad connection attempt, there is no need to loop it in this class.
         * Aslo used, when database is wrongly filled so no operations should be done with bad data.
        */
        internal WaitingServerConductor(IDatabaseController databaseController, bool tryToConnectToDatabase = false)
        {
            if (tryToConnectToDatabase)
            {
                System.Threading.Tasks.Task.Run(() =>
                {
                    databaseController.Connect();
                });
            }
        }

        public void Initialize()
        {}

        public void ShutDown()
        {}
    }
}
