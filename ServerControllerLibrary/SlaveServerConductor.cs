﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using DataTransferControllerLibrary;
using DataTransferModel.Database;
using DataTransferModel.Trasmission;
using DataTransferModel.DataTransfer;

namespace ServerControllerLibrary
{
    internal class SlaveServerConductor : BaseServerConductor
    {
        internal SlaveServerConductor(IDatabaseController databaseController, DataTransferController dataTransferController, IReadOnlyList<ITransmitter> transmitters) 
            : base(databaseController, dataTransferController, transmitters)
        {}

        public override event EventHandler<string> OnTransmissionError;

        #region Database
        protected override void SubscribeToDatabaseEvents()
        {
            _databaseController.AspTableReceived += _databaseController_AspTableReceived;
            _databaseController.TableFrsReconReceived += _databaseController_TableFrsReconReceived;
            _databaseController.TableFrsJamReceived += _databaseController_TableFrsJamReceived;
            _databaseController.TableFhssReconReceived += _databaseController_TableFhssReconReceived;
            _databaseController.TableFhssJamReceived += _databaseController_TableFhssJamReceived;
            _databaseController.TableFhssExcludedFreqReceived += _databaseController_TableFhssExcludedFreqReceived;
        }

        protected override void UnsubscribeFromDatabaseEvents()
        {
            _databaseController.AspTableReceived -= _databaseController_AspTableReceived;
            _databaseController.TableFrsReconReceived -= _databaseController_TableFrsReconReceived;
            _databaseController.TableFrsJamReceived -= _databaseController_TableFrsJamReceived;
            _databaseController.TableFhssReconReceived -= _databaseController_TableFhssReconReceived;
            _databaseController.TableFhssJamReceived -= _databaseController_TableFhssJamReceived;
            _databaseController.TableFhssExcludedFreqReceived -= _databaseController_TableFhssExcludedFreqReceived;
        }

        protected async void _databaseController_TableFhssJamReceived(object sender, ModelsTablesDBLib.TableSuppressFHSS[] table)
        {
            try
            {
                await Task.Delay(1);
                foreach (var transmitter in _linkedStationsTransmitters)
                {
                    transmitter.SendTableFhssJam(new TableEventArgs<ModelsTablesDBLib.TableSuppressFHSS>(transmitter.OwnStationId, table));
                }
            }
            catch (Exception e)
            {
                OnTransmissionError?.Invoke(this, $"Transmission exception occured\r\n{e}");
            }
        }

        protected async void _databaseController_TableFrsJamReceived(object sender, ModelsTablesDBLib.TableSuppressFWS[] table)
        {
            try
            {
                await Task.Delay(1);
                foreach (var transmitter in _linkedStationsTransmitters)
                {
                    transmitter.SendTableFrsJam(new TableEventArgs<ModelsTablesDBLib.TableSuppressFWS>(transmitter.OwnStationId, table));
                }
            }
            catch (Exception e)
            {
                OnTransmissionError?.Invoke(this, $"Transmission exception occured\r\n{e}");
            }
        }

        protected async void _databaseController_TableFrsReconReceived(object sender, ModelsTablesDBLib.TableReconFWS[] table)
        {
            try
            {
                await Task.Delay(1);
                foreach (var transmitter in _linkedStationsTransmitters)
                {
                    transmitter.SendTableFrsRecon(new TableEventArgs<ModelsTablesDBLib.TableReconFWS>(transmitter.OwnStationId, table));
                }
            }
            catch (Exception e)
            {
                OnTransmissionError?.Invoke(this, $"Transmission exception occured\r\n{e}");
            }
        }
        #endregion

        #region Data Transfer

        protected override void SubscribeToDataTransferEvents()
        {
            _dataTransferController.OnMessageReceived += _dataTransferController_OnSendMessageRequest;
            _dataTransferController.OnExecutiveDfRequestReceived += _dataTransferController_OnSendExecutiveDfRequest;
            _dataTransferController.OnExecutiveDfResponseReceived += _dataTransferController_OnSendExecutiveDfResponse;
            _dataTransferController.OnSlaveRdfResultReceived += _dataTransferController_OnSlaveRdfResultReceived;
            _dataTransferController.OnFhssCycleResultReceived += _dataTransferController_OnFhssCycleResultReceived;
        }

        protected override void UnsubscribeFromDataTransferEvents()
        {
            _dataTransferController.OnMessageReceived -= _dataTransferController_OnSendMessageRequest;
            _dataTransferController.OnExecutiveDfRequestReceived -= _dataTransferController_OnSendExecutiveDfRequest;
            _dataTransferController.OnExecutiveDfResponseReceived -= _dataTransferController_OnSendExecutiveDfResponse;
            _dataTransferController.OnSlaveRdfResultReceived -= _dataTransferController_OnSlaveRdfResultReceived;
            _dataTransferController.OnFhssCycleResultReceived -= _dataTransferController_OnFhssCycleResultReceived;
        }

        private async void _dataTransferController_OnSlaveRdfResultReceived(object sender, RdfCycleResult result)
        {
            try
            {
                await Task.Delay(1);
                foreach (var transmitter in _linkedStationsTransmitters)
                {
                    transmitter.SendSlaveRdfResult(result);
                }
            }
            catch (Exception e)
            {
                OnTransmissionError?.Invoke(this, $"Transmission exception occured\r\n{e}");
            }
        }
        #endregion

        #region Transmitters

        protected override void SubscribeToTransmittersEvents()
        {
            foreach (var transmitter in _linkedStationsTransmitters)
            {
                transmitter.AspTableReceived += Transmitter_AspTableReceived;
                transmitter.TableFrsJamReceived += Transmitter_TableFrsJamReceived;
                transmitter.TableFhssReconReceived += Transmitter_TableFhssReconReceived;
                transmitter.TableFhssJamReceived += Transmitter_TableFhssJamReceived;
                transmitter.TableFhssExcludedFreqReceived += Transmitter_TableFhssExcludedFreqReceived;
                transmitter.TableSectorsRangesReconReceived += Transmitter_TableSectorsRangesReconReceived;
                transmitter.TableSectorsRangesSupprReceived += Transmitter_TableSectorsRangesSupprReceived;
                transmitter.TableFreqForbiddenReceived += Transmitter_TableFreqForbiddenReceived;
                transmitter.TableFreqImportantReceived += Transmitter_TableFreqImportantReceived;
                transmitter.TableFreqKnownReceived += Transmitter_TableFreqKnownReceived;
                transmitter.OnLocalTimeReceived += Transmitter_OnLocalTimeReceived;
                transmitter.OnSetBandReceived += Transmitter_OnSetBandReceived;

                transmitter.OnSendMessageRequest += Transmitter_OnSendMessageRequest;
                transmitter.OnSendExecutiveDfResponse += Transmitter_OnSendExecutiveDfResponse;
                transmitter.OnSendExecutiveDfRequest += Transmitter_OnSendExecutiveDfRequest;
                transmitter.OnSendModeRequest += Transmitter_OnSendModeRequest;
                transmitter.OnMasterRdfResultReceived += Transmitter_OnMasterRdfResultReceived;
                transmitter.OnFhssCycleResultReceived += Transmitter_OnFhssCycleResultReceived;
            }
        }

        protected override void UnsubscribeFromTransmittersEvents()
        {
            foreach (var transmitter in _linkedStationsTransmitters)
            {
                transmitter.AspTableReceived -= Transmitter_AspTableReceived;
                transmitter.TableFrsJamReceived -= Transmitter_TableFrsJamReceived;
                transmitter.TableFhssReconReceived -= Transmitter_TableFhssReconReceived;
                transmitter.TableFhssJamReceived -= Transmitter_TableFhssJamReceived;
                transmitter.TableFhssExcludedFreqReceived -= Transmitter_TableFhssExcludedFreqReceived;
                transmitter.TableSectorsRangesReconReceived -= Transmitter_TableSectorsRangesReconReceived;
                transmitter.TableSectorsRangesSupprReceived -= Transmitter_TableSectorsRangesSupprReceived;
                transmitter.TableFreqForbiddenReceived -= Transmitter_TableFreqForbiddenReceived;
                transmitter.TableFreqImportantReceived -= Transmitter_TableFreqImportantReceived;
                transmitter.TableFreqKnownReceived -= Transmitter_TableFreqKnownReceived;
                transmitter.OnLocalTimeReceived -= Transmitter_OnLocalTimeReceived;
                transmitter.OnSetBandReceived -= Transmitter_OnSetBandReceived;

                transmitter.OnSendMessageRequest -= Transmitter_OnSendMessageRequest;
                transmitter.OnSendExecutiveDfResponse -= Transmitter_OnSendExecutiveDfResponse;
                transmitter.OnSendExecutiveDfRequest -= Transmitter_OnSendExecutiveDfRequest;
                transmitter.OnSendModeRequest -= Transmitter_OnSendModeRequest;
                transmitter.OnMasterRdfResultReceived -= Transmitter_OnMasterRdfResultReceived;
                transmitter.OnFhssCycleResultReceived -= Transmitter_OnFhssCycleResultReceived;
            }
        }

        private void Transmitter_OnSetBandReceived(object sender, int bandNumber)
        {
            _dataTransferController.BandReceived(bandNumber);
        }

        private void Transmitter_OnLocalTimeReceived(object sender, System.DateTime e)
        {
            throw new System.NotImplementedException(); //todo !_!
        }

        private void Transmitter_TableFrsJamReceived(object sender, TableEventArgs<ModelsTablesDBLib.TableSuppressFWS> args)
        {
            _databaseController.UpdateFrsJamTable(args.Table);
        }

        private void Transmitter_TableFhssJamReceived(object sender, TableEventArgs<ModelsTablesDBLib.TableSuppressFHSS> args)
        {
            _databaseController.UpdateFhssJamTable(args.Table);
        }

        private void Transmitter_TableFreqKnownReceived(object sender, TableEventArgs<ModelsTablesDBLib.TableFreqKnown> args)
        {
            _databaseController.UpdateKnownFrequenciesTable(args.Table);
        }

        private void Transmitter_TableFreqImportantReceived(object sender, TableEventArgs<ModelsTablesDBLib.TableFreqImportant> args)
        {
            _databaseController.UpdateImportantFrequenciesTable(args.Table);
        }

        private void Transmitter_TableFreqForbiddenReceived(object sender, TableEventArgs<ModelsTablesDBLib.TableFreqForbidden> args)
        {
            _databaseController.UpdateForbiddenFrequenciesTable(args.Table);
        }

        private void Transmitter_TableSectorsRangesSupprReceived(object sender, TableEventArgs<ModelsTablesDBLib.TableSectorsRangesSuppr> args)
        {
            _databaseController.UpdateSectorsRangesSuppr(args.Table);
        }

        private void Transmitter_TableSectorsRangesReconReceived(object sender, TableEventArgs<ModelsTablesDBLib.TableSectorsRangesRecon> args)
        {
            _databaseController.UpdateSectorsRangesRecon(args.Table);
        }

        private void Transmitter_OnMasterRdfResultReceived(object sender, RdfCycleResult rdfResult)
        {
            _dataTransferController.MasterRdfResultReceived(rdfResult);
        }

        private void Transmitter_OnSendModeRequest(object sender, byte mode)
        {
            _dataTransferController.ModeReceived(mode);
        }

        #endregion
    }
}
