﻿using System;
using System.Threading.Tasks;
using NUnit.Framework;
using TransmissionLibrary.SlowUdpTransmission;

namespace TestProject1
{
    [TestFixture]
    public class UdpTransmissionTests
    {
        private Transmitter _master;
        private Transmitter _slave;

        private int _slaveCounter;
        private int _masterCounter;

        [OneTimeSetUp]
        public void Setup()
        {
            const string localHost = "127.0.0.1";
            _master = new Transmitter(localHost, 10000, localHost, 10001, true);
            _slave = new Transmitter(localHost, 10001, false);
        }

        [SetUp]
        public void Reset()
        {
            _masterCounter = 0;
            _slaveCounter = 0;
        }

        [Test]
        [Parallelizable(ParallelScope.None)]
        public async Task ConnectionEstablished()
        {
            await Task.Delay(500);
            
            Assert.That(_master.IsConnected.Equals(true));
            Assert.That(_slave.IsConnected.Equals(true));
        }

        [Test]
        [Parallelizable(ParallelScope.None)]
        public async Task ReceiveMasterMessage()
        {
            _slave.MessageReceived += Handler;
            
            _master.Manager.Add(Commands.Message, new byte[]{(byte)Commands.Message,250,3,4,5,6,7,8,9});
            await Task.Delay(500);
            Assert.That(_slaveCounter.Equals(1));
            _slave.MessageReceived -= Handler;

            void Handler(object sender, byte[] bytes)
            {
                if (bytes[0] == (byte) Commands.Message)
                {
                    _slaveCounter++;
                }
            }
        }

        [Test]
        [Parallelizable(ParallelScope.None)]
        public async Task ReceiveSlaveMessage()
        {
            _master.MessageReceived += Handler;
            
            _slave.Manager.Add(Commands.Message, new byte[]{(byte)Commands.Message,200,3,4,5,6,7,8,9});
            await Task.Delay(2000);
            Assert.That(_masterCounter.Equals(1));
            
            _master.MessageReceived -= Handler;

            void Handler(object sender, byte[] bytes)
            {
                if (bytes[0] == (byte) Commands.Message)
                {
                    _masterCounter++;
                }
            }
        }

        [Test]
        [Parallelizable(ParallelScope.None)]
        public async Task ReceiveNumberOfMasterMessages()
        {
            byte lastMessage = 255;
            _slave.MessageReceived += Handler;

            const int NumberOfMessages = 10;
            for (int i = 0; i < NumberOfMessages; i++)
            {
                _master.Manager.Add(Commands.Message, new byte[]{(byte)Commands.Message,(byte)i,3,4,5,6,7,8,9});
            }
            await Task.Delay(1000);
            Assert.That(_slaveCounter.Equals(NumberOfMessages));
            
            _slave.MessageReceived -= Handler;
            
            void Handler(object sender, byte[] bytes)
            {
                if (bytes[0] == (byte) Commands.Message)
                {
                    lastMessage++;
                    if (lastMessage != bytes[1])
                    {
                        throw new Exception();
                    }
                    
                    _slaveCounter++;
                }
            }
        }

        [TestCase(10)]
        [TestCase(60_000)]
        [Parallelizable(ParallelScope.None)]
        public async Task MultipleMessageTransactions(int delayMs)
        {
            await Task.Delay(delayMs);
            byte lastMasterMessage = 255;
            byte lastSlaveMessage = 255;
            _master.MessageReceived += MasterHandler;
            _slave.MessageReceived += SlaveHandler;

            const int numberOfMessages = 10;
            for (var i = 0; i < numberOfMessages; i++)
            {
                _master.Manager.Add(Commands.Message, new byte[]{(byte)Commands.Message,(byte)(i + 100),3,4,5,6,7,8,9});
                _slave.Manager.Add(Commands.Message, new byte[]{(byte)Commands.Message,(byte)(i + 150),3,4,5,6,7,8,9});
            }
            await Task.Delay(10000);
            Assert.That(_masterCounter.Equals(numberOfMessages));
            Assert.That(_slaveCounter.Equals(numberOfMessages));
            
            _master.MessageReceived -= MasterHandler;
            _slave.MessageReceived -= SlaveHandler;

            void MasterHandler(object sender, byte[] bytes)
            {
                if (bytes[0] == (byte) Commands.Message)
                {
                    lastMasterMessage++;
                    if (lastMasterMessage + 150 != bytes[1])
                    {
                        throw new Exception();
                    }
                    
                    Console.WriteLine($"Master received {bytes[1]} message");
                    _masterCounter++;
                }
            }
            
            void SlaveHandler(object sender, byte[] bytes)
            {
                if (bytes[0] == (byte) Commands.Message)
                {
                    lastSlaveMessage++;
                    if (lastSlaveMessage + 100 != bytes[1])
                    {
                        throw new Exception();
                    }
                    
                    Console.WriteLine($"Slave received {bytes[1]} message");
                    _slaveCounter++;
                }
            }
        }
    }
}